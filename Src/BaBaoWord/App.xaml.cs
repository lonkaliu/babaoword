﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.IO;
using System.Threading;
using System.Windows.Controls;
using BaBaoWord.View;
using System.Collections;
using BaBaoWord.Model;
using BaBaoWord.ViewModel;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using BaBaoWord.Category;

namespace BaBaoWord
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {
        //Real-Time Change Language
        //http://www.wpfsharp.com/2012/01/27/how-to-change-language-at-run-time-in-wpf-with-loadable-resource-dictionaries-and-dynamicresource-binding/

        public static App Instance;
        public static string Directory;
        public event EventHandler LanguageChangedEvent;
        private ContentControl _content;
        private List<RootModel> _Root;
        private string _currentCultrue;
        #region Initail
        //Mutex myMutex;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //bool aIsNewInstance = false;
            //myMutex = new Mutex(true, "BaBaoWord", out aIsNewInstance);
            //if (!aIsNewInstance)
            //{
            //    switch (CultureInfo.CurrentCulture.Name)
            //    {
            //        case "zh-TW":
            //            MessageBox.Show("八寶單字已經在執行中", "警告", MessageBoxButton.OK, MessageBoxImage.Warning);
            //            break;
            //        default:
            //            MessageBox.Show("Ba Bao Word already is running.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //            break;
            //    }
            //    App.Current.Shutdown();
            //}
            Directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            var result = WebServiceDao.GetBaBaoWordVersion();
            if (result.Result)
            {
                string remoteVersion = result.Values["Version"].ToString().Trim();
                string selfVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion.ToString().Trim();
                if (!remoteVersion.Equals(selfVersion))
                {
                    try
                    {
                        System.Diagnostics.Process.Start(Path.Combine(Directory, "AutoUpdate.exe"));
                        App.Current.Shutdown();
                    }
                    catch (Exception ex)
                    {
                        Log.WriteLog(LogLevel.Error, ex);
                    }
                }
            }

            StartupUri = new Uri("/MasterWindow.xaml", UriKind.Relative);
            Instance = this;

            string path = Path.Combine(Directory, "Sound");
            if (!System.IO.Directory.Exists(path))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                catch
                {
                    MessageBox.Show("Please create the 「Sound」 folder first.");
                    App.Current.Shutdown();
                }
            }
            Parameter.GetInstance().Mp3Directory = path;

            string tempPath = Path.Combine(Path.GetTempPath(), "WordMp3Temp");
            if (!System.IO.Directory.Exists(tempPath))
            {
                System.IO.Directory.CreateDirectory(tempPath);
            }
            Parameter.GetInstance().Mp3TempDirectory = tempPath;

            _Root = new List<RootModel>();

            SetLanguageResourceDictionary(GetLocXamlFilePath(CultureInfo.CurrentCulture.Name));
            //SetLanguageResourceDictionary(GetLocXamlFilePath("en-US"));
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            if (_content == null)
            {
                _content = Application.Current.MainWindow.FindName("content_Detail") as ContentControl;
                ChangeContent(Parameter.HomeRootKey);
                List<SettingModel> settings;
                if (new BaBaoWord.DataAccess.SettingRespository().GetSetting("System", "TempAccount", out settings))
                {
                    if (settings.Count > 0)
                    {
                        ChangeContent(Parameter.LoginRootKey);
                    }
                }
                if (new BaBaoWord.DataAccess.SettingRespository().GetSetting("WordBook", "SortBy", out settings))
                {
                    if (settings.Count > 0)
                    {
                        Parameter.GetInstance().SortBy = (WordSortType)Enum.Parse(typeof(WordSortType), settings[0].Value);
                    }
                }
                if (new BaBaoWord.DataAccess.SettingRespository().GetSetting("WordBook", "WordFilters", out settings))
                {
                    if (settings.Count > 0)
                    {
                        System.Collections.ObjectModel.ObservableCollection<WordFilterModel> temp = new System.Collections.ObjectModel.ObservableCollection<Model.WordFilterModel>();
                        Parameter.GetInstance().WordFilters = temp.Deserialize<WordFilterModel>(settings[0].Value);
                    }
                }

            }
        }

        #endregion

        private void ChangeContent(RootModel root)
        {

            if (root != null && root.Content != null && root.Content != _content.Content)
            {
                PubSub<object>.RaiseEvent(Parameter.BannerControlEventKey, this, new PubSubEventArgs<object>(
                    new BannerModel()
                    {
                        IsVisibility = _Root.Count <= 1 ? false : true,
                        CurrentLocationValue = root.Name
                    })
                    );
                _content.Content = root.Content;
            }
        }

        public void PreviousContent(int previousCount)
        {
            if (previousCount < _Root.Count)
            {
                int index = _Root.Count - 1 - previousCount;
                RemoveRoot(index);
                ChangeContent(_Root[index]);
            }
        }
        public void ChangeContent(string path, object param = null)
        {
            RootModel root = null;
            if (_Root.Count == 0 || path != _Root[_Root.Count - 1].Name)
            {
                if (!FindRoot(path, out root))
                {
                    UserControl uc = null;
                    switch (path)
                    {
                        case Parameter.HomeRootKey:
                            uc = new Main();
                            break;
                        case Parameter.NewWordRootKey:
                            uc = new NewWord();
                            break;
                        case Parameter.EditWordRootKey:
                            uc = new NewWord();
                            break;
                        case Parameter.WordBookRootKey:
                            uc = new WordBookMain();
                            break;
                        case Parameter.AboutRootKey:
                            uc = new About();
                            break;
                        case Parameter.LoginRootKey:
                            uc = new Login();
                            break;
                        case Parameter.NewAccountRootKey:
                            uc = new UserProfile();
                            UserProfileViewModel upvm = new UserProfileViewModel(true);
                            uc.DataContext = upvm;
                            break;
                        case Parameter.UserProfileRootKey:
                            uc = new UserProfile();
                            UserProfileViewModel editUpvm = new UserProfileViewModel(false);
                            uc.DataContext = editUpvm;
                            break;

                        case Parameter.ValidationCodeRootKey:
                            uc = new ValidationCode();
                            ValidationCodeViewModel vcvm = new ValidationCodeViewModel(true);
                            uc.DataContext = vcvm;
                            break;
                        case Parameter.ForgetPasswordRootKey:
                            uc = new ValidationCode();
                            ValidationCodeViewModel editVcvm = new ValidationCodeViewModel(false);
                            uc.DataContext = editVcvm;
                            break;
                        case Parameter.ExamRootKey:
                            uc = new ExamSetting();
                            break;
                        case Parameter.ExamResultRootKey:
                            uc = new ExamResult();
                            ExamType type = param.GetPropertyValue<ExamType>("examType");
                            Dictionary<WordModel, FailureModel> failureWord = param.GetPropertyValue<Dictionary<WordModel, FailureModel>>("failureWords");
                            List<WordModel> examWords = param.GetPropertyValue<List<WordModel>>("words");
                            List<long> wordbookSelIds = param.GetPropertyValue<List<long>>("wordbookSelIds");
                            ExamResultViewModel ervm = new ExamResultViewModel(type, failureWord, examWords, wordbookSelIds);
                            uc.DataContext = ervm;
                            break;
                    }
                    if (uc != null)
                    {
                        root = new RootModel();
                        root.Name = path;
                        root.Content = uc;
                        _Root.Add(root);
                    }
                }

                if (root != null)
                {
                    ChangeContent(root);
                }

            }
        }

        private bool FindRoot(string path, out RootModel root)
        {
            root = _Root.Where(item => item.Name.Equals(path)).FirstOrDefault();
            if (root != null)
            {
                int index = _Root.IndexOf(root);
                RemoveRoot(index);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void RemoveRoot(int index)
        {
            for (int i = index + 1; i < _Root.Count; i++)
            {
                _Root.RemoveAt(i);
            }
        }

        #region Language
        public void SwitchLanguage(string culture)
        {
            //if (CultureInfo.CurrentCulture.Name.Equals(culture, StringComparison.CurrentCultureIgnoreCase))
            if (_currentCultrue.Equals(culture, StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }
            var ci = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            SetLanguageResourceDictionary(GetLocXamlFilePath(culture));
            if (null != LanguageChangedEvent)
            {
                LanguageChangedEvent(this, new EventArgs());
            }
        }

        private string GetLocXamlFilePath(string culture)
        {
            return Path.Combine(Directory, "Resources", "Lang", culture + ".xaml");
        }

        private void SetLanguageResourceDictionary(string cultureFile)
        {
            if (File.Exists(cultureFile))
            {
                _currentCultrue = cultureFile;
                ResourceDictionary rd = new ResourceDictionary();
                rd.Source = new Uri(cultureFile);
                int langDictId = -1;
                for (int i = 0; i < Resources.MergedDictionaries.Count; i++)
                {
                    var md = Resources.MergedDictionaries[i];
                    if (md.Contains("ResourceDictionaryName"))
                    {
                        if (md["ResourceDictionaryName"].ToString().StartsWith("Loc-"))
                        {
                            langDictId = i;
                            break;
                        }
                    }
                }
                if (langDictId == -1)
                {
                    Resources.MergedDictionaries.Add(rd);
                }
                else
                {
                    Resources.MergedDictionaries[langDictId] = rd;
                }
            }
        }

        private void LoadLenguage()
        {
            ResourceDictionary rd = null;
            string lang = CultureInfo.CurrentCulture.Name;
            try
            {
                rd = Application.LoadComponent(new Uri("Resources/Lang/" + lang + ".xaml", UriKind.Relative)) as ResourceDictionary;
            }
            catch
            {

            }

            if (rd != null)
            {
                if (this.Resources.MergedDictionaries.Count > 0)
                {
                    this.Resources.MergedDictionaries.Clear();
                }
                this.Resources.MergedDictionaries.Add(rd);
            }
        }
        #endregion
    }
}
