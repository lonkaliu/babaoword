﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BaBaoWord.ViewModel;

namespace BaBaoWord
{
    /// <summary>
    /// MasterWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MasterWindow : Window
    {
        public MasterWindow()
        {

            this.Closing += (s, e) => ((ViewModelBase)this.DataContext).Dispose();
            //this.Closing +=  ((MasterWindowViewModel)this.DataContext).OnWindowClosing;
        }


    }
}
