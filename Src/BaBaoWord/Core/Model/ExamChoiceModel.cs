﻿using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
   internal class ExamChoiceModel
    {
        public long? Id { get; set; }

        private string _word;
        /// <summary>
        /// 單字
        /// </summary>
        public string Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value.ToLower();
                ShortWord = _word.Substring(0, 12, true);
            }
        }

        public string ShortWord { get; set; }

        private string _explanation;
        /// <summary>
        /// 說明
        /// </summary>
        public string Explanation
        {
            get
            {
                return _explanation;
            }
            set
            {
                ShortExplanation = value.Substring(0, 12, true);
                _explanation = value;
            }
        }

        public string ShortExplanation { get; set; }

        public long? WId { get; set; }
    }
}
