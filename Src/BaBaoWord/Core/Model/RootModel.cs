﻿using System.Windows.Controls;

namespace BaBaoWord.Model
{
    internal class RootModel
    {
        public RootModel()
        {

        }
        public RootModel(string name,UserControl uc)
        {
            Name = name;
            Content = uc;
        }
        public string Name { get; set; }
        public UserControl Content { get; set; }
    }
}
