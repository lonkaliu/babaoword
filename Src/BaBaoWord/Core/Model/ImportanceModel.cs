﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Category;
using BaBaoWord.Util;

namespace BaBaoWord.Model
{
    internal class ImportanceModel
    {
        public ImportanceModel(ImportanceType importance)
        {
            Value = importance;
            Name = importance.ToString();
        }

        public ImportanceType Value { get; set; }

        
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = CommonUtil.GetResourceString(value);
            }
        }
    }
}
