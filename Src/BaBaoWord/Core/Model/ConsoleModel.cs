﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class ConsoleModel
    {
        private bool _showConsole = true;
        public bool ShowConsole
        {
            get
            {
                return _showConsole;
            }
            set
            {
                _showConsole = value;
            }
        }

        private bool _showOkBtn = false;

        public bool ShowOkBtn
        {
            get
            {
                return _showOkBtn;
            }
            set
            {
                _showOkBtn = value;
            }
        }

        private bool _showCancelBtn = false ;
        public bool ShowCancelBtn
        {
            get
            {
                return _showCancelBtn;
            }
            set
            {
                _showCancelBtn = value;
            }
        }

        private bool _enabledCountdown = true;
        public bool EnabledCountdown
        {
            get
            {
                return _enabledCountdown;
            }
            set
            {
                _enabledCountdown = value;
            }
        }
    }
}
