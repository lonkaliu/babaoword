﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class SearchWordInWordBookModel
    {
        public long WordId { get; set; }

        private string _wordBookName;
        public string WordBookName
        {
            get
            {
                return _wordBookName;
            }
            set
            {
                WordBook.Name = value;
                _wordBookName = value;
            }
        }


        private WordBookModel _wordBook = new WordBookModel();
        public WordBookModel WordBook
        {
            get
            {
                return _wordBook;
            }
            set
            {
                _wordBook = value;
            }
        }
    }
}
