﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class ChangeRecordModel
    {
        public ChangeTableName? ChangeTableName { get; set; }
        public ChangeType? ChangeType { get; set; }
        public long? Id1 { get; set; }
        public long? Id2 { get; set; }
    }

    enum ChangeTableName
    {
        Wordbook,
        Word,
        Wordbook_Word,
        ForgetCount,
    }

    enum ChangeType
    {
        New,
        Edit,
        Remove
    }
}
