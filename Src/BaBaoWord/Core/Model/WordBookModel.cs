﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Util;

namespace BaBaoWord.Model
{
    internal class WordBookModel
    {
        public long? Id { get; set; }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string ShortName
        {
            get
            {
                return ("(" + WordCount + ")" + Name).Substring(0, 5, true);
            }
        }

        public bool IsCheck { get; set; }

        public int WordCount { get; set; }

        public bool CheckData(out string errStr)
        {
            errStr = string.Empty;
            bool result = false;
            try
            {
                if (string.IsNullOrEmpty(Name))
                {
                    errStr = CommonUtil.MsgBlankStr("WordBook");
                    return result;

                }
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;

        }

    }
}
