﻿using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class KeyValueModel <T> : List<T>
    {
        public KeyValueModel(T model)
        {
            Value = model;
            Name = model.ToString();
        }

        public T Value { get; set; }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = CommonUtil.GetResourceString(value);
            }
        }
    }
}
