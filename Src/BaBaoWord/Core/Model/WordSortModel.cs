﻿using BaBaoWord.Category;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class WordSortModel
    {
        public WordSortModel(WordSortType wordSort)
        {
            Value = wordSort;
            Name = wordSort.ToString();
        }

        public WordSortType Value { get; set; }


        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = CommonUtil.GetResourceString(value);
            }
        }
    }
}
