﻿using BaBaoWord.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class ExamSettingModel
    {
        public ExamSettingModel()
        {
            Random = true;
            Opacity = 0.7;
            DelayShowExplanation = 2;
        }

        public bool Random { get; set; }
        public double Opacity { get; set; }

        public int DelayShowExplanation { get; set; }

        public ExamType ExamType { get; set; }

    }
}
