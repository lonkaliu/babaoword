﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class FailureModel
    {
        public int FailureCount { get; set; }

        public bool ShowAnswer { get; set; }

        public string FirstErrorAnswer { get; set; }
    }
}
