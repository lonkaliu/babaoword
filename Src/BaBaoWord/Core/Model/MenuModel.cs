﻿using System.Windows.Input;

namespace BaBaoWord.Model
{
    internal class MenuModel
    {
        public MenuModel(int id,string name,string root)
        {
            Id = id;
            Name = name;
            Root = root;
            IsEnabled = true;
            Foreground = "#FF333333";
        }
        public long? Id { get; set; }

        public bool IsSelected { get; set; }
        public string Name { get; set; }
        public string Root { get; set; }

        public bool IsEnabled { get; set; }

        public string Foreground { get; set; }
        public ICommand ClickCommand { get; set; }
    }
}
