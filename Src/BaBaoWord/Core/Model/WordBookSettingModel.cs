﻿using BaBaoWord.Category;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class WordBookSettingModel
    {
        private WordSortType _sortBy = WordSortType.ByWeight;
        public WordSortType SortBy
        {
            get
            {
                return _sortBy;
            }
            set
            {
                _sortBy = value;
            }
        }

        private ObservableCollection<WordFilterModel> _wordFilters = new ObservableCollection<WordFilterModel>();
        public ObservableCollection<WordFilterModel> WordFilters
        {
            get
            {
                return _wordFilters;
            }
            set
            {
                _wordFilters = value;
            }
        }

    }
}
