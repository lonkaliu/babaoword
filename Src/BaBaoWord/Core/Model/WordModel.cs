﻿using System.Collections.Generic;
using BaBaoWord.Category;
using BaBaoWord.MVVM;
using System;
using BaBaoWord.Util;
using System.ComponentModel;

namespace BaBaoWord.Model
{
    internal class WordModel : INotifyPropertyChanged
    {
        public long? Id { get; set; }

        private string _word;
        /// <summary>
        /// 單字
        /// </summary>
        public string Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value.ToLower();
                ShortWord = _word.Substring(0, 10, true);
            }
        }

        //private string _hiddenValueStr = string.Empty;
        public string HiddenValueStr
        {
            get
            {
                return "wi:" + Math.Round(Weight, 2) + " f:" + FORGET_COUNT + " ec:" + ExamCount + " wrc:" + WrongCount + " wr:" + Math.Round(WrongRate, 2)+"%";
            }
        }

        public string HiddenTooltip { get
            {
                return "wi:" + CommonUtil.GetResourceString("Weight") + Environment.NewLine
                    + "f:" + CommonUtil.GetResourceString("ForgetCount") + Environment.NewLine
                    + "ec:" + CommonUtil.GetResourceString("ExamCount") + Environment.NewLine
                    + "wrc:" + CommonUtil.GetResourceString("WrongCount") + Environment.NewLine
                    + "wr:" + CommonUtil.GetResourceString("WrongRate") + Environment.NewLine;
            }
        }

        public string ShortWord { get; set; }

        public long FORGET_COUNT { get; set; }

        public double WrongRate { get; set; }

        public int ExamCount { get; set; }
        public int WrongCount { get; set; }

        public double Weight { get; set; }

        private ImportanceType _importance = ImportanceType.Medium;
        public ImportanceType Importance
        {
            get
            {
                return _importance;
            }
            set
            {
                _importance = value;
            }
        }

        public string Sound { get; set; }

        public bool IsRemoteSound { get; set; }

        private bool _showForget = true;
        public bool ShowForget
        {
            get
            {
                return _showForget;
            }
            set
            {
                _showForget = value;
                OnPropertyChanged("ShowForget");
            }
        }

        /// <summary>
        /// 發音列表
        /// </summary>
        public TrulyObservableCollection<PronunciationModel> Pronunciation { get; set; }


        /// <summary>
        /// 說明列表
        /// </summary>
        public TrulyObservableCollection<ExplanationModel> Explanation { get; set; }

        public bool CheckData(out string errStr)
        {
            errStr = string.Empty;
            bool result = false;
            try
            {
                if (string.IsNullOrEmpty(Word))
                {
                    errStr = CommonUtil.MsgBlankStr("Word");
                    return result;
                }
                if (Pronunciation == null)
                {
                    errStr = CommonUtil.MsgAddItemStr("Pronunciation");
                    return result;
                }
                int index = 0;
                if (Pronunciation.Count > 1)
                {
                    foreach (PronunciationModel pronun in Pronunciation)
                    {
                        index++;
                        if (string.IsNullOrEmpty(pronun.Pronunciation))
                        {
                            errStr = CommonUtil.MsgItemBlankStr("Pronunciation", index);
                            return result;
                        }
                    }
                }
                if (Explanation == null)
                {
                    errStr = CommonUtil.MsgAddItemStr("Explanation");
                    return result;
                }
                index = 0;
                foreach (ExplanationModel explan in Explanation)
                {
                    index++;
                    if (explan.Speech == SpeechType._empty)
                    {
                        errStr = CommonUtil.MsgItemBlankStr("Speech", index);
                        return result;
                    }
                    else if (string.IsNullOrEmpty(explan.Explanation))
                    {
                        errStr = CommonUtil.MsgItemBlankStr("Explanation", index);
                        return result;
                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                errStr = e.Message;
            }
            return result;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
    }
}
