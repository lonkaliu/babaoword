﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Category;
using BaBaoWord.Util;

namespace BaBaoWord.Model
{
    internal class SpeechModel
    {
        public SpeechModel()
        {
            _sortList.Add("n", 1);
            _sortList.Add("v", 2);
            _sortList.Add("a", 3);
            _sortList.Add("s", 4);
            _sortList.Add("art", 5);
            _sortList.Add("ad", 6);
            _sortList.Add("pron", 7);
            _sortList.Add("c", 8);
            _sortList.Add("u", 9);
            _sortList.Add("pt", 10);
            _sortList.Add("pp", 11);
            _sortList.Add("vt", 12);
            _sortList.Add("vi", 13);
            _sortList.Add("conj", 14);
            _sortList.Add("prep", 15);
            _sortList.Add("aux", 16);
            _sortList.Add("int", 17);
            _sortList.Add("pl", 18);
            _sortList.Add("abbr", 19);
            _sortList.Add("ph", 20);
            _sortList.Add("hp", 21);
            _sortList.Add("spe", 22);
        }
        public SpeechModel(SpeechType type)
            : this()
        {
            if(type == SpeechType._empty)
            {
                return;
            }
            Value = type;
            Name = type.ToString();
            Key = type.ToString();
        }

        private Dictionary<string, int> _sortList = new Dictionary<string, int>();
        private int _sortIndex = int.MaxValue;
        public int SortIndex
        {
            get
            {
                if (_sortList.ContainsKey(Key))
                {
                    _sortIndex = _sortList[Key];
                }
                return _sortIndex;
            }
        }


        public SpeechType Value { get; set; }

        private string _key;
        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value.TrimStart('_');
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                Key = value;
                _name = CommonUtil.GetResourceString(value);
            }
        }

    }
}
