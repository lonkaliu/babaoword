﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Category;
using BaBaoWord.Util;

namespace BaBaoWord.Model
{
    internal class UserLanguageModel
    {
        public UserLanguageModel(LanguageType languageType,LanguageLevel languageLevel)
        {
            Language_Name=languageType.ToString();
            Language_Level = languageLevel;
        }

        public string Language_Name { get; set; }
        public LanguageLevel Language_Level { get; set; }

    }
}
