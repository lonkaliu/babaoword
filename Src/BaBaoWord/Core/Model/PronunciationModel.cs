﻿using System.ComponentModel;
using BaBaoWord.Util;

namespace BaBaoWord.Model
{
    internal class PronunciationModel : INotifyPropertyChanged
    {
        public PronunciationModel()
        {
            Pronunciation = string.Empty;
            Sound = string.Empty;
        }
        public long? Id { get; set; }

        public string ShortPronunciation { get; set; }

        private string _pronuncation;
        /// <summary>
        /// 念法
        /// </summary>
        public string Pronunciation
        {
            get
            {
                return _pronuncation;
            }
            set
            {
                ShortPronunciation = value.Substring(0, 8, true);
                _pronuncation = value;
            }
        }

        /// <summary>
        /// mp3
        /// </summary>
        public string Sound { get; set; }

        public bool IsRemoteSound { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

    }
}
