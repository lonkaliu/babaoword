﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class ContentChangeModel
    {
        public string Name { get; set; }
        public string Key { get; set; }

        public object Item { get; set; }
    }
}
