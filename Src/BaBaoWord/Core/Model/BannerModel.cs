﻿
namespace BaBaoWord.Model
{
    internal class BannerModel
    {
        public bool IsVisibility { get; set; }

        public string CurrentLocation { get; set; }
        public string CurrentLocationValue { get; set; }
    }
}
