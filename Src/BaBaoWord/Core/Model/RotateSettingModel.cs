﻿using BaBaoWord.Category;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class RotateSettingModel //: IDataErrorInfo
    {
        public bool BroadcastSound { get; set; }
        public bool Random { get; set; }

        public bool AllWord { get; set; }

        private int _delayShowExplanation = 10;
        public int DelayShowExplanation
        {
            get
            {
                return _delayShowExplanation;
            }
            set
            {
                _delayShowExplanation = value;
            }
        }

        public double Opacity { get; set; }
        public double Interval { get; set; }
        private string _workTime;
        public string WorkTime
        {
            get
            {
                return _workTime;
            }
            set
            {
                _workTime = value.PadLeft(4, '0');
            }
        }

        private string _lunchTimeBegin;
        public string LunchTimeBegin
        {
            get
            {
                return _lunchTimeBegin;
            }
            set
            {
                _lunchTimeBegin = value.PadLeft(4, '0');
            }
        }

        private string _lunchTimeEnd;
        public string LunchTimeEnd
        {
            get
            {
                return _lunchTimeEnd;
            }
            set
            {
                _lunchTimeEnd = value.PadLeft(4, '0');
            }
        }

        private string _offWorkTime;
        public string OffWorkTime
        {
            get
            {
                return _offWorkTime;
            }
            set
            {
                _offWorkTime = value.PadLeft(4, '0');
            }
        }

        private static Dictionary<string, List<DateTimeRange>> _timeRangeList = new Dictionary<string, List<DateTimeRange>>();
        public bool CheckTime(DateTime? datetime)
        {
            DateTime now = datetime == null ? DateTime.Now : datetime.Value;
            if (!_timeRangeList.ContainsKey(now.AddDays(-1).ToString("yyyyMMdd")))
            {
                List<DateTimeRange> timeRangeList = GetDateTimeRange(now);
                _timeRangeList.Add(now.AddDays(-1).ToString("yyyyMMdd"), timeRangeList);
            }


            if (!_timeRangeList.ContainsKey(now.ToString("yyyyMMdd")))
            {
                List<DateTimeRange> timeRangeList = GetDateTimeRange(now);
                _timeRangeList.Add(now.ToString("yyyyMMdd"), timeRangeList);
            }

            foreach (KeyValuePair<string, List<DateTimeRange>> item in _timeRangeList)
            {
                foreach (DateTimeRange timeRange in item.Value)
                {
                    if (now >= timeRange.Begin && now <= timeRange.End)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void ClearTimeRangeList()
        {
            _timeRangeList.Clear();
        }

        private List<DateTimeRange> GetDateTimeRange(DateTime now)
        {
            List<DateTime> timeList = GetTimeList(now);

            List<DateTimeRange> timeRangeList = new List<DateTimeRange>();
            for (int i = 0; i < timeList.Count; i++)
            {
                if (i % 2 == 0)
                {
                    timeRangeList.Add(new DateTimeRange() { Begin = timeList[i] });
                }
                else
                {
                    timeRangeList[timeRangeList.Count - 1].End = timeList[i];
                }
            }


            return timeRangeList;
        }

        private List<DateTime> GetTimeList(DateTime now)
        {
            List<DateTime> timeList = new List<DateTime>();
            DateTime temp;
            if (string.IsNullOrEmpty(WorkTime))
            {
                timeList.Add(CommonUtil.CombineDateTime(now, "0000"));
            }
            else
            {
                timeList.Add(CommonUtil.CombineDateTime(now, WorkTime));
            }
            if (!string.IsNullOrEmpty(LunchTimeBegin))
            {
                temp = CommonUtil.CombineDateTime(now, LunchTimeBegin);
                if (timeList.Count > 0 && timeList[timeList.Count - 1] > temp)
                {
                    temp = temp.AddDays(timeList[timeList.Count - 1].Subtract(temp).Days+1);
                }
                timeList.Add(temp);
            }
            if (!string.IsNullOrEmpty(LunchTimeEnd))
            {
                temp = CommonUtil.CombineDateTime(now, LunchTimeEnd);
                if (timeList.Count > 0 && timeList[timeList.Count - 1] > temp)
                {
                    temp = temp.AddDays(timeList[timeList.Count - 1].Subtract(temp).Days + 1);
                }
                timeList.Add(temp);
            }

            if (string.IsNullOrEmpty(OffWorkTime))
            {

                timeList.Add(CommonUtil.CombineDateTime(timeList[timeList.Count - 1], "2359"));
            }
            else
            {
                temp = CommonUtil.CombineDateTime(now, OffWorkTime);
                if (timeList.Count > 0 && timeList[timeList.Count - 1] > temp)
                {
                    temp = temp.AddDays(timeList[timeList.Count - 1].Subtract(temp).Days + 1);
                }
                timeList.Add(temp);
            }
            return timeList;
        }


        public bool Validation(out string errMsg)
        {
            errMsg = string.Empty;

            if (!ValidationTime(WorkTime))
            {
                errMsg = CommonUtil.MsgSettingErrStr("WorkTime");
                return false;
            }
            if (!ValidationTime(LunchTimeBegin))
            {
                errMsg = CommonUtil.MsgSettingErrStr("LunchTimeBegin");
                return false;
            }
            if (!ValidationTime(LunchTimeEnd))
            {
                errMsg = CommonUtil.MsgSettingErrStr("LunchTimeEnd");
                return false;
            }
            if (!ValidationTime(OffWorkTime))
            {
                errMsg = CommonUtil.MsgSettingErrStr("OffWorkTime");
                return false;
            }
            if (!string.IsNullOrEmpty(LunchTimeBegin) && string.IsNullOrEmpty(LunchTimeEnd))
            {
                errMsg = CommonUtil.MsgBlankStr("LunchTimeEnd");
                return false;
            }
            if (string.IsNullOrEmpty(LunchTimeBegin) && !string.IsNullOrEmpty(LunchTimeEnd))
            {
                errMsg = CommonUtil.MsgBlankStr("LunchTimeBegin");
                return false;
            }

            List<DateTime> timeList = GetTimeList(DateTime.Now);
            if(timeList != null && timeList.Count > 0)
            {
                if(timeList[timeList.Count -1].Day - timeList[0].Day > 1)
                {
                    errMsg = "AbnormalTimeOverTwoDay";
                    return false;
                }
            }
            return true;
        }

        private bool ValidationTime(string time)
        {
            if (!string.IsNullOrEmpty(time) && !CommonUtil.ValidationTime(time))
            {
                return false;
            }
            return true;
        }

        private class DateTimeRange
        {
            public DateTime Begin { get; set; }
            public DateTime End { get; set; }
        }




       
    }
}
