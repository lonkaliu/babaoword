﻿using System.ComponentModel;
using BaBaoWord.Category;
using BaBaoWord.Util;

namespace BaBaoWord.Model
{
    internal class ExplanationModel : INotifyPropertyChanged
    {
        public ExplanationModel()
        {
            ClassHelper.StringProperty2Empty(this);
        }

        public long? Id { get; set; }
        private SpeechType _speech;
        /// <summary>
        /// 詞性
        /// </summary>
        public SpeechType Speech
        {
            get
            {
                return _speech;
            }
            set
            {
                _speech = value;
            }
        }

        public SpeechModel SpeechModel
        {
            get
            {
                return new Model.SpeechModel(Speech);
            }
        }

        /// <summary>
        /// 詞性中文
        /// </summary>
        public string SpeechStr { get; set; }

        private string _explanation;
        /// <summary>
        /// 說明
        /// </summary>
        public string Explanation
        {
            get
            {
                return _explanation;
            }
            set
            {
                ShortExplanation = value.Substring(0, 14, true);
                _explanation = value;
            }
        }

        public string ShortExplanation { get; set; }

        private string _sentence;
        /// <summary>
        /// 例句
        /// </summary>
        public string Sentence { get
            {
                return _sentence;
            }
            set
            {
                _sentence = value;
            }
        }
        public string ShortSentence { get; set; }
        private bool _isShow = false;
        public bool IsShow
        {
            get
            {
                return _isShow;
            }
            set
            {
                _isShow = value;
            }
        }

        public bool Equals(ExplanationModel obj)
        {
            bool result = false;
            if (obj != null
                && SpeechStr.Equals(obj.SpeechStr)
                && Explanation.Equals(obj.Explanation)
                && Sentence.Equals(obj.Sentence)
                && EqualSpeech(obj.Speech)
                )
            {
                result = true;
            }
            return result;
        }

        //private bool EqualSpeech(SpeechModel speech)
        //{
        //    if (Speech == null && speech == null)
        //    {
        //        return true;
        //    }
        //    else if (Speech == null && speech != null)
        //    {
        //        return false;
        //    }
        //    else if (Speech != null && speech == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return Speech.Value.Equals(speech.Value);
        //    }
        //}
        private bool EqualSpeech(SpeechType speech)
        {
            return Speech.ToString().Equals(speech.ToString());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
    }
}
