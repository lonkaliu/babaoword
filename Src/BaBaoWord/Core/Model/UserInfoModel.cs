﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class UserInfoModel
    {
        public UserInfoModel()
        {
            Language = new List<UserLanguageModel>();
            Language.Add(new UserLanguageModel(Category.LanguageType.English,Category.LanguageLevel.Beginner));
        }
        public long? Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public string User_Guid { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public List<UserLanguageModel> Language { get; set; }
    }
}
