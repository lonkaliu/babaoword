﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    internal class SettingModel
    {
        public string Category { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
