﻿using BaBaoWord.Category;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BaBaoWord.Model
{
    public class WordFilterModel : INotifyPropertyChanged
    {
        public CompareType Compare { get; set; }
        public WordSortType WordSort { get; set; }

        public double Value { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
    }
}
