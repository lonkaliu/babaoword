﻿using System;
using System.Collections.Generic;
using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System.ComponentModel;
using System.Linq;

namespace BaBaoWord.ViewModel
{
    internal class MenuListViewModel : ViewModelBase
    {
        public MenuListViewModel()
        {
            PubSub<object>.AddEvent(Parameter.RefreshMenuEventKey, RefreshMenu);
        }

        private void RefreshMenu(object sender, PubSubEventArgs<object> args)
        {
            OnPropertyChanged("MenuList");
        }
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("MenuList");
        }

        #region Binding
        #region DataContent
        private List<MenuModel> _menuList;
        public List<MenuModel> MenuList
        {
            get
            {
                _menuList = new MenuRespository().GetMenuList();
                foreach (MenuModel menu in _menuList)
                {
                    menu.ClickCommand = new RelayCommand(param => ClickCommand(param));
                }
                return _menuList;
            }
        }


        #endregion

        #region Command
        private void ClickCommand(object param)
        {
            if (param != null)
            {
                string root = param.ToString();
                if (root == "zh-TW" || root == "en-US")
                {
                    App.Instance.SwitchLanguage(root);
                }
                else if (root == "Logout")
                {
                    Parameter.GetInstance().UserInfo = null;
                    PubSub<object>.RaiseEvent(Parameter.RefreshNameEventKey, this, null);
                }
                else if (root == "SyncWord")
                {
                    SyncFile();
                }
                else
                {
                    App.Instance.ChangeContent(root);
                    PubSub<object>.RaiseEvent(Parameter.MenuControlEventKey, this, new PubSubEventArgs<object>("Hide"));
                }
                OnPropertyChanged("MenuList");
            }

        }

        public void SyncFile()
        {
            BaseService.ResultData result = new BaseService.ResultData();
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                CommonUtil.ShowProcessMainWindowShadow("Synchronizing");
               result= CommonUtil.SyncFile();
            };
            backgroundWorker.RunWorkerAsync();
            backgroundWorker.RunWorkerCompleted += (s, o) =>
            {
                if (result.Result)
                {
                    CommonUtil.ShowMessageBox(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr("SyncWord")));
                }
                else
                {
                    CommonUtil.ShowMessageBox(CommonUtil.GetResourceString(result.Exception));
                }
                CommonUtil.HighProcessMainWindowShadow();
            };

        }

        

        #endregion

        #endregion
    }
}
