﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;

namespace BaBaoWord.ViewModel
{
    internal class MenuPanelViewModel : ViewModelBase
    {
        public MenuPanelViewModel()
        {
            PubSub<object>.AddEvent(Parameter.MenuControlEventKey, MenuController);
            PubSub<object>.AddEvent(Parameter.BannerControlEventKey, UpdateBanner);
            PubSub<object>.AddEvent(Parameter.RefreshNameEventKey, RefreshName);
        }

        private void RefreshName(object sender, PubSubEventArgs<object> args)
        {
            OnPropertyChanged("UserName");
        }
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("BannerModel");
            OnPropertyChanged("ListStr");
            OnPropertyChanged("Previous");
        }

        #region Binding

        #region DataContent

        public string ListImg
        {
            get
            {
                return Parameter.IconPath + "ic_action_list_2.png";
            }
        }

        public string ListStr
        {
            get
            {
                return CommonUtil.GetResourceString("Menu");
            }
        }

        public string PreviousStr
        {
            get
            {
                return CommonUtil.GetResourceString("Previous");
            }
        }

        public string UserName
        {
            get
            {
                if(Parameter.GetInstance().UserInfo != null)
                {
                    return Parameter.GetInstance().UserInfo.Name;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string PreviousImg
        {
            get
            {
                return Parameter.IconPath + "ic_action_arrow_left.png";
            }
        }

        private View.MenuPanel _menu;
        private View.MenuPanel Menu
        {
            get
            {
                if (null == _menu)
                {
                    //_menu = UIHelper.FindChild<UserControl>(Application.Current.MainWindow, "uc_Menu") as View.Menu;
                    _menu = Application.Current.MainWindow.FindName("uc_Menu") as View.MenuPanel;
                }
                return _menu;
            }
        }

        private bool _showShadow = false;
        public bool ShowShadow
        {
            get
            {
                return _showShadow;
            }
            set
            {
                _showShadow = value;
                OnPropertyChanged("ShowShadow");
            }
        }


        private BannerModel _bannerModel;
        public BannerModel BannerModel
        {
            get
            {
                if (_bannerModel != null)
                {
                    _bannerModel.CurrentLocation = CommonUtil.GetResourceString(_bannerModel.CurrentLocationValue);
                }
                return _bannerModel;
            }
            set
            {
                _bannerModel = value;
                OnPropertyChanged("BannerModel");
            }
        }
        #endregion

        #region Command

        public ICommand Previous
        {
            get
            {
                return new RelayCommand(param => PreviousExecute(param));
            }
        }
        private void PreviousExecute(object param)
        {
            App.Instance.PreviousContent(1);
        }

        #region Menu
        public ICommand ShowMenu
        {
            get
            {
                return new RelayCommand(param => ShowMenuExecute(param));
            }
        }
        public ICommand HideMenu
        {
            get
            {
                return new RelayCommand(param => HideMenuExecute(param));
            }
        }

        private void HideMenuExecute(object param)
        {
            MenuController("Hide");
        }


        private void ShowMenuExecute(object param)
        {
            MenuController("Show");

        }
        public void MenuController(string switchType)
        {
            Storyboard showMenu = App.Current.TryFindResource("showMenu") as Storyboard;
            Storyboard hideMenu = App.Current.TryFindResource("hideMenu") as Storyboard;
            StackPanel menu = Menu.FindName("sp_Menu") as StackPanel;
            if (showMenu != null && hideMenu != null && menu != null )
            {
                if (switchType.Equals("Show"))
                {
                    ShowShadow = true;
                    showMenu.Begin(menu);
                }
                else if (switchType.Equals("Hide"))
                {
                    ShowShadow = false;
                    hideMenu.Begin(menu);
                }
            }
        }
        #endregion
        #endregion

        #endregion
        
        #region Publish-Subscribe

        public void MenuController(object sender, PubSubEventArgs<object> args)
        {
            MenuController(args.Item.ToString());
        }


        public void UpdateBanner(object sender, PubSubEventArgs<object> args)
        {
            BannerModel value = args.Item as BannerModel;
            BannerModel = value;
        }

        #endregion


    }
}
