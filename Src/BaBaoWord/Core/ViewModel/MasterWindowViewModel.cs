﻿using System;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System.Windows.Input;
using System.ComponentModel;

namespace BaBaoWord.ViewModel
{
    internal class MasterWindowViewModel : ViewModelBase
    {
        public MasterWindowViewModel()
        {
            PubSub<object>.AddEvent(Parameter.ProcessControlEventKey, ProcessController);
            PubSub<object>.AddEvent(Parameter.HighShadowEventKey, HighShadowContorller);
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("Wait");
            OnPropertyChanged("ProcessModel");
        }

        #region Publish-Subscribe

        public void ProcessController(object sender, PubSubEventArgs<object> args)
        {
            ProcessModel value = args.Item as ProcessModel;
            ProcessModel = value;
        }
        private void HighShadowContorller(object sender, PubSubEventArgs<object> args)
        {
            if (args.Item is bool)
            {
                ShowHighShadow = (bool)args.Item;
            }
        }

        #endregion

        #region Binding

        #region DataContent
        public string Wait
        {
            get
            {
                return CommonUtil.GetResourceString("Wait");
            }
        }

        private ProcessModel _processModel = new ProcessModel();
        public ProcessModel ProcessModel
        {
            get
            {
                return _processModel;
            }
            set
            {
                _processModel = value;
                _processModel.Message = CommonUtil.GetResourceString(_processModel.Message);
                OnPropertyChanged("ProcessModel");
            }
        }

        private bool _showHighShadow = false;
        public bool ShowHighShadow
        {
            get
            {
                return _showHighShadow;
            }
            set
            {
                _showHighShadow = value;
                OnPropertyChanged("ShowHighShadow");
            }
        }
        #endregion

        #region Command
        private bool isClose = false;
        public ICommand ClosingForm
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (Parameter.GetInstance().UserInfo != null && Parameter.GetInstance().UserInfo.Id != null)
                    {
                        CommonUtil.ShowProcessMainWindowShadow("Synchronizing");
                        BaseService.ResultData result = new BaseService.ResultData();
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            result = CommonUtil.SyncFile();
                            if (!result.Result)
                            {
                                Log.WriteLog(LogLevel.Error, result.Exception);
                            }
                            isClose = true;
                        };
                        backgroundWorker.RunWorkerAsync();
                    }
                    else
                    {
                        isClose = true;
                    }

                    while (true)
                    {
                        if (isClose)
                        {
                            break;
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                    BaseDao.Dispose();
                    base.Dispose();
                });
            }
        }

        public ICommand ClosedForm
        {
            get
            {
                return new RelayCommand(param =>
                {
                });
            }
        }

        #endregion

        #endregion



    }
}
