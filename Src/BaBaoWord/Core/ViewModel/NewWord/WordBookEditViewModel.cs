﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BaBaoWord.Util;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.DataAccess;

namespace BaBaoWord.ViewModel
{
    internal class WordBookEditViewModel : EditViewModelBase
    {
        private WordBookModel _wordBook;
        private WordBookRespository _wordbookDao;

        public WordBookEditViewModel(WordBookModel wordBook)
        {
            _wordbookDao = new WordBookRespository();
            WordBook = new WordBookModel();
            WordBook.Copy(wordBook);
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("WordBook");
            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent

        public WordBookModel WordBook
        {
            get
            {
                return _wordBook;
            }
            set
            {
                _wordBook = value;
                OnPropertyChanged("WordBook");
            }
        }

        //Binding
        #endregion

        #region Command

        public ICommand EditClick
        {
            get
            {
                return new RelayCommand(param =>
                {
                    string errStr;
                    if (_wordBook.CheckData(out errStr))
                    {
                        if (_wordbookDao.EditItem(_wordBook))
                        {
                            //CommonUtil.ShowMessageBox(CommonUtil.GetResourceString("EditSuccess"));
                        }
                        else
                        {
                            CommonUtil.ShowMessageBox(string.Format(CommonUtil.GetResourceString("EditFailure"), string.Empty));
                        }
                        ContentChangeModel content = new ContentChangeModel()
                        {
                            Name = "List",
                        };
                        PubSub<object>.RaiseEvent(Parameter.ChangeWordBookContentEventKey, this, new PubSubEventArgs<object>(content));
                    }
                    else
                    {
                        CommonUtil.ShowMessageBox(CommonUtil.ParseMsgStr(errStr));
                    }
                });
            }
        }
        public ICommand CancelClick
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ContentChangeModel content = new ContentChangeModel()
                    {
                        Name = "List",
                    };
                    PubSub<object>.RaiseEvent(Parameter.ChangeWordBookContentEventKey, this, new PubSubEventArgs<object>(content));
                });
            }
        }
        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion
    }
}
