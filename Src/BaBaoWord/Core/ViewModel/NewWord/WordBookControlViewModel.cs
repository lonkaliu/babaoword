﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BaBaoWord.MVVM;
using System.Windows;
using System.Windows.Controls;
using BaBaoWord.View;
using BaBaoWord.Util;
using BaBaoWord.Model;
using BaBaoWord.DataAccess;

namespace BaBaoWord.ViewModel
{
    internal class WordBookControlViewModel : MessageViewModelBase
    {
        public WordBookControlViewModel()
        {
            _consoleModel.ShowOkBtn = true;
            _consoleModel.ShowCancelBtn = true;
            _consoleModel.EnabledCountdown = false;
            ConsoleModel = _consoleModel;
            WordBookContent = new WordBookList();
            EventStr = "SelWordBook";
            Title = CommonUtil.GetResourceString("WordBook");
            PubSub<object>.AddEvent(Parameter.ChangeWordBookContentEventKey, ChangeWordBookContent);
        }

        #region Binding

        #region DataContent

        private object _wordBookContent;
        public object WordBookContent
        {
            get
            {
                return _wordBookContent;
            }
            set
            {
                _wordBookContent = value;
                OnPropertyChanged("WordBookContent");
            }
        }

        private bool _showEditBtn = true;
        public bool ShowEditBtn
        {
            get
            {
                return _showEditBtn;
            }
            set
            {
                _showEditBtn = value;
                OnPropertyChanged("ShowEditBtn");
            }
        }

        private string _eventStr;
        public string EventStr
        {
            get
            {
                return _eventStr;
            }
            set
            {
                _eventStr = CommonUtil.GetResourceString(value);
                OnPropertyChanged("EventStr");
            }
        }

        public string AddWordBookStr
        {
            get
            {
                return CommonUtil.GetResourceString("AddWordBook");
            }
        }

        #endregion

        #region Command

        public ICommand Add
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ContentChangeModel arg = new ContentChangeModel()
                    {
                        Name = "Add"
                    };
                    ChangeWordBookContent(null, new PubSubEventArgs<object>(arg));
                });
            }
        }

        public ICommand PreCancel
        {
            get
            {
                return new RelayCommand(param =>
                {

                });
            }
        }

        public ICommand PreOk
        {
            get
            {
                return new RelayCommand(param =>
                {
                    WordBookRespository wordBookDao = new WordBookRespository();
                    wordBookDao.Commit();
                });
            }
        }
        #endregion

        #endregion

        #region Publish-Subscribe
        private void ChangeWordBookContent(object sender, PubSubEventArgs<object> args)
        {
            if (args.Item is ContentChangeModel)
            {
                ContentChangeModel arg = (ContentChangeModel)args.Item;
                switch (arg.Name)
                {
                    case "List":
                        WordBookContent = new WordBookList();
                        _consoleModel.ShowOkBtn = true;
                        _consoleModel.ShowCancelBtn = true;
                        ShowEditBtn = true;
                        EventStr = "SelWordBook";
                        break;
                    case "Edit":
                        WordBookAddEdit wordBookEdit = new WordBookAddEdit();
                        wordBookEdit.DataContext = new WordBookEditViewModel((WordBookModel)arg.Item);
                        WordBookContent = wordBookEdit;
                        _consoleModel.ShowOkBtn = false;
                        _consoleModel.ShowCancelBtn = false;
                        ShowEditBtn = false;
                        EventStr = "EditWordBook";
                        break;
                    case "Add":
                        wordBookEdit = new WordBookAddEdit();
                        wordBookEdit.DataContext = new WordBookAddViewModel();
                        WordBookContent = wordBookEdit;
                        _consoleModel.ShowOkBtn = false;
                        _consoleModel.ShowCancelBtn = false;
                        ShowEditBtn = false;
                        EventStr = "AddWordBook";
                        break;
                }
                OnPropertyChanged("ConsoleModel");
            }
        }
        #endregion

    }
}
