﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using BaBaoWord.Category;
using System.Collections.ObjectModel;
using System.Windows;
using BaBaoWord.View;
using BaBaoWord.DataAccess;
using System.Windows.Controls;
using System.Data;

namespace BaBaoWord.ViewModel
{
    internal class NewWordViewModel : EditViewModelBase
    {
        WordRespository _wordDao = new WordRespository();
        WordBookRespository _wordBookDao = new WordBookRespository();

        public NewWordViewModel()
        {
            object wordId = CommonUtil.GetPageParam(Parameter.EditWordRootKey, "WordId");
            CommonUtil.RemovePageParam(Parameter.EditWordRootKey, "WordId");
            if (wordId != null && wordId is long)
            {
                ChangeWord((long)wordId);
                IsExpandAll = false;
            }
            else
            {
                WordInstance.Pronunciation = new TrulyObservableCollection<PronunciationModel>();
                WordInstance.Pronunciation.Add(new PronunciationModel());
                WordInstance.Explanation = new TrulyObservableCollection<ExplanationModel>();
                WordInstance.Explanation.Add(new ExplanationModel() { IsShow = true });
                //_wordBookDao.SetCheckIds(new List<long>());
            }

            _sound = new MediaElement();
            _sound.LoadedBehavior = MediaState.Manual;
            _sound.MediaEnded += _sound_MediaEnded;
            PubSub<object>.AddEvent(Parameter.ChangeWordEventKey, ChangeWordEvent);
        }

        private void ChangeWordEvent(object sender, PubSubEventArgs<object> args)
        {
            if (args.Item is long)
            {
                ChangeWord((long)args.Item);
                OnPropertyChanged("WordInstance");
            }
        }

        private void ChangeWord(long wordId)
        {
            _wordDao.GetWord(wordId, out _wordInstance);
            List<long> wordBookIds;
            if (_wordBookDao.GetWordBookIds(wordId, out wordBookIds))
            {
                _wordBookDao.SetCheckIds(wordBookIds);
            }
        }


        private bool _switchLanguage = false;
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            _switchLanguage = true;
            OnPropertyChanged("ImportanceList");
            OnPropertyChanged("SpeechList");
            OnPropertyChanged("WordStr");
            OnPropertyChanged("YahooSearchStr");
            OnPropertyChanged("AddPronunciationStr");
            OnPropertyChanged("RemovePronunciationStr");
            OnPropertyChanged("SelWordBookStr");
            OnPropertyChanged("RemoveWordStr");
            OnPropertyChanged("ExpandAllStr");
            OnPropertyChanged("CollapseAllStr");
            OnPropertyChanged("AddExplanationStr");
            OnPropertyChanged("RemoveExplanationStr");
            OnPropertyChanged("PronunciationStr");
            OnPropertyChanged("PlaySoundStr");
            OnPropertyChanged("FindSoundStr");
            OnPropertyChanged("RemoveSoundStr");
            OnPropertyChanged("SearchWordStr");
            OnPropertyChanged("SpeechStr");
            OnPropertyChanged("ExplanationStr");
            OnPropertyChanged("SentenceStr");
            OnPropertyChanged("ImportanceStr");
            OnPropertyChanged("WordInstance");
            foreach (ExplanationModel explanation in WordInstance.Explanation)
            {
                explanation.OnPropertyChanged("SpeechModel");
            }
            _switchLanguage = false;
            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent

        private WordModel _wordInstance;
        public WordModel WordInstance
        {
            get
            {
                if (_wordInstance == null)
                {
                    _wordInstance = new WordModel();
                }
                return _wordInstance;
            }
            set
            {
                _wordInstance = value;
                OnPropertyChanged("WordInstance");
            }
        }

        public string WordStr
        {
            get
            {
                return CommonUtil.GetResourceString("Word");
            }
        }

        public string YahooSearchStr
        {
            get
            {
                return CommonUtil.GetResourceString("YahooSearchStr");
            }
        }

        public string SearchWordStr
        {
            get
            {
                return CommonUtil.GetResourceString("SearchWord");
            }
        }

        public string SelWordBookStr
        {
            get
            {
                return CommonUtil.GetResourceString("SelWordBook");
            }
        }
        public string RemoveWordStr
        {
            get
            {
                return CommonUtil.GetResourceString("RemoveWord");
            }
        }
        public string YahooImg
        {
            get
            {
                return Parameter.YahooImg;
            }
        }

        public string AddImg
        {
            get
            {
                return Parameter.AddImg;
            }
        }
        public string RemoveImg
        {
            get
            {
                return Parameter.RemoveImg;
            }
        }

        private bool _settingConsole = false;
        public bool SettingConsole
        {
            get
            {
                return _settingConsole;
            }
            set
            {
                _settingConsole = value;
                OnPropertyChanged("SettingConsole");
            }
        }

        #region Importance
        public string ImportanceStr
        {
            get
            {
                return CommonUtil.GetResourceString("Importance");
            }
        }

        private ObservableCollection<ImportanceModel> _importanceList;
        public ObservableCollection<ImportanceModel> ImportanceList
        {
            get
            {
                if (_switchLanguage || _importanceList == null)
                {
                    _importanceList = new ObservableCollection<ImportanceModel>();
                    foreach (ImportanceType importance in Enum.GetValues(typeof(ImportanceType)))
                    {
                        _importanceList.Add(new ImportanceModel(importance));
                    }
                }
                return _importanceList;
            }
        }

        #endregion

        #region Explanation

        

        public string ExpandAllStr
        {
            get
            {
                return CommonUtil.GetResourceString("ExpandAll");
            }
        }
        public string CollapseAllStr
        {
            get
            {
                return CommonUtil.GetResourceString("CollapseAll");
            }
        }

        public string AddExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("AddExplanation");
            }
        }
        public string RemoveExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("RemoveExplanation");
            }
        }
 

        public string ExpandAllImg
        {
            get
            {
                return Parameter.ExpandAllImg;
            }
        }
        public string CollapseAllImg
        {
            get
            {
                return Parameter.CollapseAllImg;
            }
        }

        private bool _isExpandAll = true;
        public bool IsExpandAll
        {
            get
            {
                return _isExpandAll;
            }
            set
            {
                _isExpandAll = value;
                OnPropertyChanged("IsExpandAll");
            }
        }
        public string SpeechStr
        {
            get
            {
                return CommonUtil.GetResourceString("Speech");
            }
        }

        public string ExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("Explanation");
            }
        }

        public string SentenceStr
        {
            get
            {
                return CommonUtil.GetResourceString("Sentence");
            }
        }

        private ObservableCollection<SpeechModel> _speechList;
        public ObservableCollection<SpeechModel> SpeechList
        {
            get
            {
                if (_switchLanguage || _speechList == null)
                {
                    _speechList = new ObservableCollection<SpeechModel>();
                    foreach (SpeechType speech in Enum.GetValues(typeof(SpeechType)))
                    {
                        if (speech.Equals(SpeechType._empty))
                        {
                            continue;
                        }
                        _speechList.Add(new SpeechModel(speech));
                    }
                    _speechList = _speechList.OrderBy(item => item.SortIndex).ToObservableCollection();
                }
                return _speechList;
            }
        }

        #endregion

        #region Pronunciation

        public string EditSoundImg
        {
            get
            {
                return Parameter.EditSoundImg;
            }
        }

        public string FindSoundImg
        {
            get
            {
                return Parameter.FindSoundImg;
            }
        }

        public string RemoveSoundStr
        {
            get
            {
                return CommonUtil.GetResourceString("RemoveSound");
            }
        }

        public string FindSoundStr
        {
            get
            {
                return CommonUtil.GetResourceString("FindSound");
            }
        }
        public string PlaySoundStr
        {
            get
            {
                return CommonUtil.GetResourceString("PlaySound");
            }
        }
        public string PronunciationStr
        {
            get
            {
                return CommonUtil.GetResourceString("Pronunciation");
            }
        }

        public string AddPronunciationStr
        {
            get
            {
                return CommonUtil.GetResourceString("AddPronunciation");
            }
        }

        public string RemovePronunciationStr
        {
            get
            {
                return CommonUtil.GetResourceString("RemovePronunciation");
            }
        }

        private readonly MediaElement _sound;
        public MediaElement Sound
        {
            get { return _sound; }
        }
        #endregion


        #endregion

        #region Command
        public ICommand Setting
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = !SettingConsole;
                });
            }
        }

        public ICommand CloseConsole
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = false;
                });
            }
        }



        public ICommand ShowWordSearchInWordBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = false;
                    CommonUtil.ShowMainWindowShadow();
                    SearchWordInWordBook searchWordInWordBook = new SearchWordInWordBook();
                    if (searchWordInWordBook.ShowDialog().Value)
                    {

                    }
                    CommonUtil.HideMainWindowShadow();

                });
            }
        }

        public ICommand Search
        {
            get
            {
                return new RelayCommand(param => DoSearch(param));
            }
        }

        private void DoSearch(object param)
        {
            if (param != null)
            {
                WordInstance.Word = param.ToString();
            }
            IsExpandAll = false;
            SettingConsole = false;
            bool failure = false;
            CommonUtil.ShowProcessMainWindowShadow("Searching");
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                WordModel word;
                if (YahooTranslator.Translator(WordInstance.Word, out word))
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        WordInstance.Pronunciation = new TrulyObservableCollection<PronunciationModel>();
                        if (word.Pronunciation.Count > 0)
                        {
                            foreach (PronunciationModel pronunciation in word.Pronunciation)
                            {
                                var hasData = WordInstance.Pronunciation.Where(item => item.Pronunciation == pronunciation.Pronunciation).SingleOrDefault();
                                if (hasData == null)
                                {
                                    if (WordInstance.Pronunciation.Count >= 3)
                                    {
                                        WordInstance.Pronunciation.RemoveAt(0);
                                    }
                                    WordInstance.Pronunciation.Add(pronunciation);
                                }
                            }
                        }
                        else
                        {
                            WordInstance.Pronunciation.Add(new PronunciationModel());
                        }

                        WordInstance.Explanation = new TrulyObservableCollection<ExplanationModel>();
                        if (word.Explanation.Count > 0)
                        {
                            foreach (ExplanationModel explanation in word.Explanation)
                            {
                                var hasData = WordInstance.Explanation.Where(item => item.Equals(explanation)).SingleOrDefault();
                                if (hasData == null)
                                {
                                    WordInstance.Explanation.Add(explanation);
                                }
                            }
                        }
                        else
                        {
                            WordInstance.Explanation.Add(new ExplanationModel() { IsShow = true });
                        }
                    });
                    WordInstance.Sound = word.Sound;
                    WordInstance.IsRemoteSound = word.IsRemoteSound;
                    OnPropertyChanged("WordInstance");
                }
                else
                {
                    failure = true;
                }
            };
            backgroundWorker.RunWorkerCompleted += (s, o) =>
            {
                CommonUtil.HighProcessMainWindowShadow();
                if (failure)
                {
                    CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString("SearchNotFound"));
                    //todo Alarm 查詢失敗 並跳出建議（可能沒辦法，yahoo沒提供）
                }
            };
            backgroundWorker.RunWorkerAsync();
        }

        public ICommand ClearAll
        {
            get
            {
                return new RelayCommand(param => ClearAllExecute(param));
            }
        }

        private void ClearAllExecute(object param)
        {
            ClearAllExecute();

            _wordBookDao.SetCheckIds(new List<long>());
            OnPropertyChanged("WordInstance");
        }

        private void ClearAllExecute()
        {
            SettingConsole = false;
            WordInstance.Word = string.Empty;
            WordInstance.Sound = string.Empty;
            WordInstance.IsRemoteSound = false;
            WordInstance.Pronunciation = new TrulyObservableCollection<PronunciationModel>();
            WordInstance.Pronunciation.Add(new PronunciationModel());
            WordInstance.Importance = ImportanceType.Medium;
            WordInstance.Explanation = new TrulyObservableCollection<ExplanationModel>();
            WordInstance.Explanation.Add(new ExplanationModel() { IsShow = true });
            WordInstance.Id = null;
            OnPropertyChanged("WordInstance");
        }

        private void ClearEmptyPronunciation()
        {
            List<int> deleteIndex = new List<int>();
            for (int i = 0; i < WordInstance.Pronunciation.Count; i++)
            {
                if (WordInstance.Pronunciation[i].Pronunciation == null
                   || string.IsNullOrEmpty(WordInstance.Pronunciation[i].Pronunciation))
                {
                    deleteIndex.Add(i);
                }
            }
            for (int i = deleteIndex.Count - 1; i >= 0; i--)
            {
                WordInstance.Pronunciation.RemoveAt(deleteIndex[i]);
            }
        }

        private void ClearEmptyExplanation()
        {
            List<int> deleteIndex = new List<int>();
            for (int i = 0; i < WordInstance.Explanation.Count; i++)
            {
                if ((WordInstance.Explanation[i].Explanation == null || string.IsNullOrEmpty(WordInstance.Explanation[i].Explanation))
                   && (WordInstance.Explanation[i].Sentence == null || string.IsNullOrEmpty(WordInstance.Explanation[i].Sentence)))
                {
                    deleteIndex.Add(i);
                }
            }
            for (int i = deleteIndex.Count - 1; i >= 0; i--)
            {
                WordInstance.Explanation.RemoveAt(deleteIndex[i]);
            }
        }

        public ICommand Save
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = false;
                    bool failure = false;
                    List<long> wordBookSelId;
                    string errStr;
                    string processType = WordInstance.Id == null ? "Add" : "Edit";
                    if (WordInstance.CheckData(out errStr) && CheckSelBook(out wordBookSelId, out errStr)
                        && !CheckDuplicateWord(wordBookSelId, out errStr) )//&& CheckSound())
                    {
                        CommonUtil.ShowProcessMainWindowShadow("Saving");
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            if (!_wordDao.Commit(WordInstance, wordBookSelId))
                            {
                                failure = true;
                            }
                        };
                        backgroundWorker.RunWorkerCompleted += (s, o) =>
                        {
                            if (failure)
                            {
                                CommonUtil.ShowMessageBox(CommonUtil.ParseMsgStr(CommonUtil.MsgFailureStr(processType)));
                            }
                            else
                            {
                                CommonUtil.ShowMessageBox(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr(processType)));
                                PubSub<object>.RaiseEvent(Parameter.RefreshWordBookListEventKey, this, null);
                                _wordBookDao.SetCheckIds(wordBookSelId);
                                if (processType.Equals("Add"))
                                {
                                    ClearAllExecute();
                                    //foreach (PronunciationModel pronunciation in WordInstance.Pronunciation)
                                    //{
                                    //    pronunciation.OnPropertyChanged("Sound");
                                    //}
                                }
                            }
                            CommonUtil.HighProcessMainWindowShadow();
                            if (processType.Equals("Edit"))
                            {
                                PubSub<object>.RaiseEvent(Parameter.ReloadWordBookWordsEventKey, this, null);
                                App.Instance.PreviousContent(1);
                            }

                        };
                        backgroundWorker.RunWorkerAsync();
                    }
                    else
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(errStr));
                    }
                });
            }
        }

        public ICommand RemoveWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = false;
                    string msg = string.Format(CommonUtil.GetResourceString("ConfirmDelItem"), WordInstance.Word);
                    if (CommonUtil.ShowConfirmMessageBoxWithShadow(msg))
                    {
                        _wordDao.Remove(WordInstance.Id.Value);
                        ClearAllExecute(null);
                    }
                });
            }
        }

        private bool CheckDuplicateWord(List<long> wordBookSelId, out string errStr)
        {
            errStr = string.Empty;
            bool result = false;
            DataTable duplicateData;
            if (_wordDao.CheckDuplicate(WordInstance.Word, (WordInstance.Id == null ? wordBookSelId : null), out duplicateData))
            {
                string msg = string.Empty;
                foreach (DataRow dr in duplicateData.Rows)
                {
                    msg += CommonUtil.MsgAlreadyInStr(WordInstance.Word, dr["NAME"].ToString()) + "-";
                }
                errStr = msg.TrimEnd('-');
                result = true;
            }
            return result;
        }

        private bool CheckSelBook(out List<long> selId, out string errStr)
        {
            selId = new List<long>();
            errStr = string.Empty;

            //if (_wordBookDao.GetItems().Count(item => item.IsCheck) == 0)
            if (_wordBookDao.GetCheckIds(out selId) && selId.Count == 0)
            {
                errStr = "PlzSelWordbookFirst";
                return false;
            }
            else
            {
                //selId = _wordBookDao.GetItems().Where(item => item.IsCheck).Select(item => item.Id.Value).ToList();
                return true;
            }
        }

        public ICommand ShowBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = false;
                    CommonUtil.ShowMainWindowShadow();
                    WordBookControl workBookControl = new WordBookControl();
                    if (workBookControl.ShowDialog().Value)
                    {
                        //直接從respository拿來用
                    }
                    CommonUtil.HideMainWindowShadow();
                });
            }
        }

        #region Sound

        private bool CheckSound()
        {
            if (!WordInstance.IsRemoteSound)
            {
                WordInstance.Sound = string.Empty;
                if (YahooTranslator.GetSound(WordInstance.Word))
                {
                    WordInstance.Sound = WordInstance.Word + ",Temp";
                }
                else
                {
                    string[] token = WordInstance.Word.Split("".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string word in token)
                    {
                        if (YahooTranslator.GetSound(word))
                        {
                            WordInstance.Sound += word + "-";
                        }
                    }
                    if (!string.IsNullOrEmpty(WordInstance.Sound))
                    {
                        WordInstance.Sound = WordInstance.Sound.TrimEnd('-') + ",Temp";
                    }
                }
                OnPropertyChanged("WordInstance");
            }
            if (string.IsNullOrEmpty(WordInstance.Sound))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool playEndFlag = true;
        public ICommand PlaySound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param != null && param is string)
                    {
                        string[] structure = param.ToString().Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                        if (structure.Length != 2)
                        {
                            return;
                        }
                        string path = string.Empty;
                        if (structure[1].Equals("Temp"))
                        {
                            path = Parameter.GetInstance().Mp3TempDirectory;
                        }
                        else if (structure[1].Equals("Real"))
                        {
                            path = Parameter.GetInstance().Mp3Directory;
                            CommonUtil.GetSound(param.ToString());
                        }
                        else
                        {
                            return;
                        }
                        string[] words = structure[0].Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            int i = 0;
                            while (i < words.Length)
                            {
                                if (playEndFlag)
                                {
                                    if (i != 0)
                                    {
                                        //System.Threading.Thread.Sleep(50);
                                    }
                                    playEndFlag = false;
                                    App.Current.Dispatcher.Invoke((Action)delegate
                                    {
                                        Sound.Source = new Uri(System.IO.Path.Combine(path, words[i] + ".mp3"), UriKind.Absolute);
                                        Sound.Play();
                                    });
                                    i++;
                                }
                                System.Threading.Thread.Sleep(100);
                            }
                        };
                        backgroundWorker.RunWorkerAsync();

                    }
                });
            }
        }

        public ICommand FoundSound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = false;
                    bool failure = false;
                    CommonUtil.ShowProcessMainWindowShadow("Finding");
                    BackgroundWorker backgroundWorker = new BackgroundWorker();
                    backgroundWorker.DoWork += (s, o) =>
                    {
                        failure = !CheckSound();
                    };
                    backgroundWorker.RunWorkerCompleted += (s, o) =>
                    {
                        if (failure)
                        {
                            CommonUtil.ShowMessageBox(CommonUtil.GetResourceString("SoundNotFound"));
                        }
                        else
                        {

                        }
                        CommonUtil.HighProcessMainWindowShadow();
                    };
                    backgroundWorker.RunWorkerAsync();
                });
            }
        }

        public ICommand RemoveSound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SettingConsole = false;
                    WordInstance.Sound = string.Empty;
                    WordInstance.IsRemoteSound = false;
                    OnPropertyChanged("WordInstance");
                });
            }
        }

        void _sound_MediaEnded(object sender, RoutedEventArgs e)
        {
            playEndFlag = true;
            Sound.Source = null;
        }

        #endregion

        #region Explanation
        public ICommand AddExplanation
        {
            get
            {
                return new RelayCommand(param =>
                {
                    CollapseAllExplanationExecute(null);
                    WordInstance.Explanation.Add(new ExplanationModel() { IsShow = true });
                });
            }
        }


        public ICommand RemoveExplanation
        {
            get
            {
                return new RelayCommand(param =>
                {
                    int index;
                    if (param != null && int.TryParse(param.ToString(), out index))
                    {
                        if (index == 0 && WordInstance.Explanation.Count == 1)
                        {
                            WordInstance.Explanation[0] = new ExplanationModel() { IsShow = true };
                        }
                        else
                        {
                            WordInstance.Explanation.RemoveAt(index);
                        }
                    }
                });
            }
        }

        public ICommand ExpandExplanation
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        WordInstance.Explanation[(int)param].IsShow = true;
                        WordInstance.Explanation[(int)param].OnPropertyChanged("IsShow");
                    }
                });
            }
        }


        public ICommand CollapseExplanation
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        WordInstance.Explanation[(int)param].IsShow = false;
                        WordInstance.Explanation[(int)param].OnPropertyChanged("IsShow");
                    }
                });
            }
        }

        public ICommand ExpandAllExplanation
        {
            get
            {
                return new RelayCommand(param =>
                {
                    IsExpandAll = true;
                    for (int i = 0; i < WordInstance.Explanation.Count; i++)
                    {
                        WordInstance.Explanation[i].IsShow = true;
                        WordInstance.Explanation[i].OnPropertyChanged("IsShow");
                    }
                });
            }
        }

        public ICommand CollapseAllExplanation
        {
            get
            {
                return new RelayCommand(param => CollapseAllExplanationExecute(param));
            }
        }

        public void CollapseAllExplanationExecute(object param)
        {
            IsExpandAll = false;
            for (int i = 0; i < WordInstance.Explanation.Count; i++)
            {
                WordInstance.Explanation[i].IsShow = false;
                WordInstance.Explanation[i].OnPropertyChanged("IsShow");
            }
        }

        //https://social.msdn.microsoft.com/Forums/en-US/6a6128bd-f5b6-46d8-98d1-151d6e68a1ce/command-not-firing-on-itemscontrol?forum=windowsgeneraldevelopmentissues
        public ICommand SpeechChanged
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        WordInstance.Explanation[(int)param].OnPropertyChanged("Speech");
                        WordInstance.Explanation[(int)param].OnPropertyChanged("SpeechModel");
                    }
                });
            }
        }

        #endregion

        #region Pronunciation
        public ICommand RemovePronunciation
        {
            get
            {
                return new RelayCommand(param =>
                {
                    int index;
                    if (param != null && int.TryParse(param.ToString(), out index))
                    {
                        if (index == 0 && WordInstance.Pronunciation.Count == 1)
                        {
                            WordInstance.Pronunciation[0] = new PronunciationModel();
                        }
                        else
                        {
                            WordInstance.Pronunciation.RemoveAt(index);
                        }
                    }
                });
            }
        }

        public ICommand AddPronunciation
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (WordInstance.Pronunciation.Count < 3)
                    {
                        WordInstance.Pronunciation.Add(new PronunciationModel());
                    }
                });
            }
        }


        #endregion

        #endregion

        #endregion

    }
}
