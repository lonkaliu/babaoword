﻿using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace BaBaoWord.ViewModel
{
    internal class WordBookAddViewModel : EditViewModelBase
    {
        private WordBookModel _wordBook;
        private WordBookRespository _wordbookDao;

        public WordBookAddViewModel()
        {
            _wordbookDao = new WordBookRespository();
            WordBook = new WordBookModel();
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("WordBook");
            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent

        public WordBookModel WordBook
        {
            get
            {
                return _wordBook;
            }
            set
            {
                _wordBook = value;
                OnPropertyChanged("WordBook");
            }
        }

        //Binding
        #endregion

        #region Command

        public ICommand EditClick
        {
            get
            {
                return new RelayCommand(param =>
                {
                    string errStr;
                    if (_wordBook.CheckData(out errStr))
                    {
                        if (_wordbookDao.AddItem(_wordBook))
                        {
                            //CommonUtil.ShowMessageBox(CommonUtil.GetResourceString("AddSuccess"));
                        }
                        else
                        {
                            CommonUtil.ShowMessageBox(string.Format(CommonUtil.GetResourceString("AddFailure"), string.Empty));
                        }
                        ContentChangeModel content = new ContentChangeModel()
                        {
                            Name = "List",
                        };
                        PubSub<object>.RaiseEvent(Parameter.ChangeWordBookContentEventKey, this, new PubSubEventArgs<object>(content));
                    }
                    else
                    {
                        CommonUtil.ShowMessageBox(CommonUtil.ParseMsgStr(errStr));
                    }

                });
            }
        }
        public ICommand CancelClick
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ContentChangeModel content = new ContentChangeModel()
                    {
                        Name = "List",
                    };
                    PubSub<object>.RaiseEvent(Parameter.ChangeWordBookContentEventKey, this, new PubSubEventArgs<object>(content));
                });
            }
        }
        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion
    }
}
