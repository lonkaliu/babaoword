﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Model;
using BaBaoWord.DataAccess;
using BaBaoWord.Util;
using System.Windows.Input;
using BaBaoWord.MVVM;
using System.Collections.ObjectModel;

namespace BaBaoWord.ViewModel
{
    internal class WordBookListViewModel : EditViewModelBase
    {
        private WordBookRespository _wordbookDao;
        public WordBookListViewModel()
        {
            _wordbookDao = new WordBookRespository();
            PubSub<object>.AddEvent(Parameter.RefreshWordBookListEventKey, RefreshWordBookList);
        }

        private void RefreshWordBookList(object sender, PubSubEventArgs<object> args)
        {
            _wordbookDao.ReloadWordBooks();
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("PlzAddWordbookFirstStr");
            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent
        public ObservableCollection<WordBookModel> WordBooks
        {
            get
            {
                return new ObservableCollection<WordBookModel>(_wordbookDao.GetItems());
            }
        }

        public string RemoveImg
        {
            get
            {
                return Parameter.RemoveImg;
            }
        }

        public string EditImg
        {
            get
            {
                return Parameter.EditImg;
            }
        }

        public string PlzAddWordbookFirstStr
        {
            get
            {
                return CommonUtil.GetResourceString("PlzAddWordbookFirst");
            }
        }

        #endregion

        #endregion

        #region Command

        public ICommand RemoveWordBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {

                        string msg = string.Format(CommonUtil.GetResourceString("ConfirmDelItem"), WordBooks[(int)param].Name);
                        if (CommonUtil.ShowConfirmMessageBox(msg))
                        {
                            _wordbookDao.RemoveItem(WordBooks[(int)param]);
                            OnPropertyChanged("WordBooks");
                        }

                    }
                });
            }
        }

        public ICommand EditWordBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        ContentChangeModel content = new ContentChangeModel()
                        {
                            Name = "Edit",
                            Item = WordBooks[(int)param],
                            Key = param.ToString()
                        };
                        PubSub<object>.RaiseEvent(Parameter.ChangeWordBookContentEventKey, this, new PubSubEventArgs<object>(content));
                    }
                });
            }
        }
        #endregion

    }
}
