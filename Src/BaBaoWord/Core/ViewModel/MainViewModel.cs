﻿using System;
using System.Windows.Input;
using BaBaoWord.Util;
using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using System.Collections.Generic;
using BaBaoWord.MVVM;
using BaBaoWord.View;

namespace BaBaoWord.ViewModel
{
    internal class MainViewModel : ViewModelBase
    {
        private WordBookRespository _wordBookDao = new WordBookRespository();
        public MainViewModel()
        {
            //bool result = CommonUtil.DecryptData();
        }
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("NewWordStr");
            OnPropertyChanged("WordBookStr");
            OnPropertyChanged("ExamStr");
        }

        #region Binding

        #region DataContent
        public string NewWordStr
        {
            get
            {
                return CommonUtil.GetResourceString("NewWord");
            }
        }
        public string WordBookStr
        {
            get
            {
                return CommonUtil.GetResourceString("WordBook");
            }
        }
        public string ExamStr
        {
            get
            {
                return CommonUtil.GetResourceString("Exam");
            }
        }
        #endregion

        #region Command
        public ICommand NewWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    List<long> ids = ShowBookDialog();
                    if (ids == null || ids.Count == 0)
                    {
                        return;
                    }
                    App.Instance.ChangeContent(Parameter.NewWordRootKey);
                }, null);
            }
        }
        public ICommand WordBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    List<long> ids = ShowBookDialog();
                    if (ids == null || ids.Count == 0)
                    {
                        return;
                    }
                    App.Instance.ChangeContent(Parameter.WordBookRootKey);
                }, null);
            }
        }

        public ICommand Exam
        {
            get
            {
                return new RelayCommand(param =>
                {
                    List<long> ids = ShowBookDialog();
                    if (ids == null || ids.Count == 0)
                    {
                        return;
                    }
                    App.Instance.ChangeContent(Parameter.ExamRootKey);
                }, null);
            }
        }

        private List<long> ShowBookDialog()
        {
            CommonUtil.ShowMainWindowShadow();
            WordBookControl workBookControl = new WordBookControl();
            List<long> ids = null;
            if (workBookControl.ShowDialog().Value)
            {
                _wordBookDao.GetCheckIds(out ids);
                CommonUtil.SetPageParam(Parameter.WordBookRootKey, "WordBookSelIds", ids);
            }
            CommonUtil.HideMainWindowShadow();
            return ids;
        }
        #endregion

        #endregion





    }


}
