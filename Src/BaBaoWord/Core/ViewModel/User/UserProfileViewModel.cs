﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using BaBaoWord.Model;
using System.Collections.ObjectModel;
using BaBaoWord.Category;
using System.Windows.Input;
using System.Windows.Controls;

namespace BaBaoWord.ViewModel
{
    internal class UserProfileViewModel : EditViewModelBase
    {
        private bool _switchLanguage = false;

        public UserProfileViewModel(bool isNew)
        {
            IsNew = isNew;
            UserInfo = new UserInfoModel();

            if (!isNew)
            {
                UserInfo.Copy(Parameter.GetInstance().UserInfo);
            }
        }


        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            _switchLanguage = true;
            OnPropertyChanged("LanguageList");
            OnPropertyChanged("NameStr");
            OnPropertyChanged("LanguageLevelStr");
            OnPropertyChanged("EmailStr");
            OnPropertyChanged("PasswordStr");
            OnPropertyChanged("UserInfo");
            _switchLanguage = false;

            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent

        public bool IsNew { get; set; }

        public UserInfoModel UserInfo { get; set; }

        public string NameStr
        {
            get
            {
                return CommonUtil.GetResourceString("Name");
            }
        }
        public string LanguageLevelStr
        {
            get
            {
                return CommonUtil.GetResourceString("LanguageLevel");
            }
        }
        public string EmailStr
        {
            get
            {
                return CommonUtil.GetResourceString("Email");
            }
        }
        public string PasswordStr
        {
            get
            {
                return CommonUtil.GetResourceString("Password");
            }
        }
        public string RegisterStr
        {
            get
            {
                if (IsNew)
                {
                    return CommonUtil.GetResourceString("Register");
                }
                else
                {
                    return CommonUtil.GetResourceString("Edit");
                }
            }
        }

        private ObservableCollection<LanguageItem> _languageList;
        public ObservableCollection<LanguageItem> LanguageList
        {
            get
            {
                if (_switchLanguage || _languageList == null)
                {
                    _languageList = new ObservableCollection<LanguageItem>();
                    foreach (LanguageLevel language in Enum.GetValues(typeof(LanguageLevel)))
                    {
                        _languageList.Add(new LanguageItem(language));
                    }
                }
                return _languageList;
            }
        }

        public class LanguageItem
        {
            public LanguageItem(LanguageLevel type)
            {
                Name = type.ToString();
                Value = type;
            }
            private string _name;
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = CommonUtil.GetResourceString(value);
                }
            }
            public LanguageLevel Value { get; set; }
        }
        #endregion

        #region Command
        public ICommand Register
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (!CommonUtil.IsValidEmail(UserInfo.Email))
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString("InvalidEmail"));
                        UserInfo.Email = string.Empty;
                        OnPropertyChanged("UserInfo");
                        return;
                    }
                    var result = WebServiceDao.CheckAccount(UserInfo.Email);
                    if(IsNew && !result.Result)
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString(result.Exception));
                        return;
                    }
                    if (string.IsNullOrEmpty(UserInfo.Password))
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgBlankStr("Password")));
                        return;
                    }
                    if (string.IsNullOrEmpty(UserInfo.Name))
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgBlankStr("Name")));
                        return;
                    }
                    if (IsNew)
                    {
                        CommonUtil.SetPageParam(Parameter.NewAccountRootKey, "UserInfo", UserInfo);
                        App.Instance.ChangeContent(Parameter.ValidationCodeRootKey);
                    }
                    else
                    {
                        UserInfo.Password = UserInfo.Password.ToSHA256();
                        result = WebServiceDao.EditAccount(UserInfo);
                        if (result.Result)
                        {
                            Parameter.GetInstance().UserInfo.Copy(UserInfo);
                            CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr("Edit")));
                            PubSub<object>.RaiseEvent(Parameter.RefreshNameEventKey, this, null);
                            App.Instance.ChangeContent(Parameter.HomeRootKey);
                        }
                        else
                        {
                            CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString(result.Exception));
                        }
                    }
                });
            }
        }
        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion


    }
}
