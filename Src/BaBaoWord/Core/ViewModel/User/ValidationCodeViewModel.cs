﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using BaBaoWord.Model;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Controls;

namespace BaBaoWord.ViewModel
{
    internal class ValidationCodeViewModel : EditViewModelBase
    {
        public ValidationCodeViewModel(bool isNew)
        {
            IsNew = isNew;
            object userInfo = null;
            if (isNew)
            {
                userInfo = CommonUtil.GetPageParam(Parameter.NewAccountRootKey, "UserInfo");
            }
            else
            {
                userInfo = CommonUtil.GetPageParam(Parameter.ForgetPasswordRootKey, "UserInfo");
            }
            if (userInfo != null)
            {
                _userInfo = (UserInfoModel)userInfo;
                SendMailAndWait();
            }
        }

        #region Binding

        #region DataContent

        public bool IsNew { get; set; }
        private string _validationCode = string.Empty;

        private UserInfoModel _userInfo;

        private int _second = 300;
        public int Second
        {
            get
            {
                return _second;
            }
            set
            {
                _second = value;
                OnPropertyChanged("Second");
            }
        }

        public string InputCode { get; set; }

        public string ValidationCodeStr
        {
            get
            {
                return CommonUtil.GetResourceString("ValidationCode");
            }
        }
        public string ValidationCodeExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("ValidationCodeExplanation");
            }
        }
        public string NewAccountStr
        {
            get
            {
                return CommonUtil.GetResourceString("NewAccount");
            }
        }
        public string CodeRemainingSecondStr
        {
            get
            {
                return CommonUtil.GetResourceString("CodeRemainingSecond");
            }
        }

        public string PasswordStr
        {
            get
            {
                return CommonUtil.GetResourceString("Password");
            }
        }

        #endregion

        #region Command
        private void SendMailAndWait()
        {
            NewValidationCode();
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                WebServiceDao.SendVerificationCode(_userInfo.Email, _validationCode);
                while (Second > 0)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        Second--;
                    });
                    System.Threading.Thread.Sleep(1000);
                }
            };
            backgroundWorker.RunWorkerAsync();
            backgroundWorker.RunWorkerCompleted += (s, o) =>
            {
                App.Instance.PreviousContent(1);
            };
        }

        private void NewValidationCode()
        {
            _validationCode = new Random(Guid.NewGuid().GetHashCode()).Next(0, 999999).ToString().PadLeft(6, '0');
        }
        public ICommand NewAccount
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (InputCode != _validationCode)
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgIncorrectFieldStr("ValidationCode")));
                        return;
                    }
                    BaseService.ResultData result = new BaseService.ResultData();
                    BackgroundWorker backgroundWorker = new BackgroundWorker();
                    backgroundWorker.DoWork += (s, o) =>
                    {
                        _userInfo.Password = _userInfo.Password.ToSHA256();
                        result = WebServiceDao.NewAccount(_userInfo);
                    };
                    backgroundWorker.RunWorkerAsync();
                    backgroundWorker.RunWorkerCompleted += (s, o) =>
                    {
                        if (result.Result)
                        {
                            long id;
                            long.TryParse(result.Values["ID"].ToString(), out id);
                            string token = result.Values["Token"].ToString();
                            string userGuid = result.Values["USER_GUID"].ToString();
                            Parameter.GetInstance().UserInfo = new Model.UserInfoModel()
                            {
                                Id = id,
                                Email = _userInfo.Email,
                                Name = _userInfo.Name,
                                Token = token,
                                User_Guid = userGuid,
                                Language = _userInfo.Language
                            };
                            PubSub<object>.RaiseEvent(Parameter.RefreshMenuEventKey, this, null);
                            PubSub<object>.RaiseEvent(Parameter.RefreshNameEventKey, this, null);
                            CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr("Register")));
                            App.Instance.ChangeContent(Parameter.HomeRootKey);
                        }
                        else
                        {
                            CommonUtil.ShowMessageBox(CommonUtil.GetResourceString(result.Exception));
                            App.Instance.PreviousContent(1);
                        }
                    };

                });
            }
        }

        public ICommand ChangePassword
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (InputCode != _validationCode)
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgIncorrectFieldStr("ValidationCode")));
                        return;
                    }
                    if (param != null && param is PasswordBox)
                    {
                        _userInfo.Password = ((PasswordBox)param).Password.ToSHA256();
                    }
                    BaseService.ResultData result = new BaseService.ResultData();
                    BackgroundWorker backgroundWorker = new BackgroundWorker();
                    backgroundWorker.DoWork += (s, o) =>
                    {
                        result = WebServiceDao.ChangePassword(_userInfo.Email, _userInfo.Password);
                        if (result.Result)
                        {
                            result = WebServiceDao.Login(_userInfo);
                        }
                    };
                    backgroundWorker.RunWorkerAsync();
                    backgroundWorker.RunWorkerCompleted += (s, o) =>
                    {
                        if (result.Result)
                        {

                            long id;
                            long.TryParse(result.Values["ID"].ToString(), out id);
                            string token = result.Values["Token"].ToString();
                            string userGuid = result.Values["USER_GUID"].ToString();
                            string name = result.Values["Name"].ToString();
                            BaseService.LanguageLevel[] lang = WebServiceDao.GetLanguage(id);
                            List<UserLanguageModel> userLang = new List<UserLanguageModel>();
                            if (lang != null)
                            {
                                foreach (var item in lang)
                                {
                                    userLang.Add(new UserLanguageModel(
                                        (Category.LanguageType)Enum.Parse(typeof(Category.LanguageType), item.Name)
                                        , (Category.LanguageLevel)Enum.Parse(typeof(Category.LanguageLevel), item.Level.ToString()))
                                    );
                                }
                            }
                            Parameter.GetInstance().UserInfo = new Model.UserInfoModel()
                            {
                                Id = id,
                                Email = _userInfo.Email,
                                Name = name,
                                Token = token,
                                User_Guid = userGuid,
                                Language = userLang
                            };
                            PubSub<object>.RaiseEvent(Parameter.RefreshMenuEventKey, this, null);
                            PubSub<object>.RaiseEvent(Parameter.RefreshNameEventKey, this, null);
                            CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr("Edit")));
                            App.Instance.ChangeContent(Parameter.HomeRootKey);
                        }
                        else
                        {
                            CommonUtil.ShowMessageBox(CommonUtil.GetResourceString(result.Exception));
                            App.Instance.PreviousContent(1);
                        }
                    };

                });
            }
        }

        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("ValidationCodeExplanationStr");
            OnPropertyChanged("CodeRemainingSecondStr");
            OnPropertyChanged("ValidationCodeStr");
            base.LanguageChangedEvent(sender, e);
        }

    }
}
