﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using BaBaoWord.Model;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Controls;
using BaBaoWord.DataAccess;
using System.IO;

namespace BaBaoWord.ViewModel
{
    internal class LoginViewModel : EditViewModelBase
    {
        public LoginViewModel()
        {
            UserInfo = new UserInfoModel();
            SettingRespository settingDao = new SettingRespository();
            List<SettingModel> settings;
            if (settingDao.GetSetting("System", "TempAccount", out settings))
            {
                if (settings.Count > 0)
                {
                    UserInfo.Email = settings[0].Value;
                    OnPropertyChanged("UserInfo");
                }
            }

        }
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("EmailStr");
            OnPropertyChanged("NewAccountStr");
            OnPropertyChanged("LoginStr");
            OnPropertyChanged("ForgetPasswordStr");
            OnPropertyChanged("PasswordStr");
            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent
        private UserInfoModel _userInfo;
        public UserInfoModel UserInfo
        {
            get
            {
                if (_userInfo == null)
                {
                    _userInfo = new UserInfoModel();
                }
                return _userInfo;
            }
            set
            {
                _userInfo = value;
                OnPropertyChanged("UserInfo");
            }
        }
        public string EmailStr
        {
            get
            {
                return CommonUtil.GetResourceString("Email");
            }
        }

        public string NewAccountStr
        {
            get
            {
                return CommonUtil.GetResourceString("NewAccount");
            }
        }

        public string LoginStr
        {
            get
            {
                return CommonUtil.GetResourceString("Login");
            }
        }

        public string ForgetPasswordStr
        {
            get
            {
                return CommonUtil.GetResourceString("ForgetPassword");
            }
        }

        public string PasswordStr
        {
            get
            {
                return CommonUtil.GetResourceString("Password");
            }
        }
        #endregion

        #region Command
        public ICommand Login
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (!CommonUtil.IsValidEmail(UserInfo.Email))
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString("InvalidEmail"));
                        UserInfo.Email = string.Empty;
                        OnPropertyChanged("UserInfo");
                        return;
                    }
                    if (param != null && param is PasswordBox)
                    {
                        UserInfo.Password = ((PasswordBox)param).Password.ToSHA256();
                    }
                    if (string.IsNullOrEmpty(UserInfo.Password))
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgBlankStr("Password")));
                        return;
                    }

                    BaseService.ResultData result = new BaseService.ResultData();
                    BackgroundWorker backgroundWorker = new BackgroundWorker();
                    backgroundWorker.DoWork += (s, o) =>
                    {
                        CommonUtil.ShowProcessMainWindowShadow(string.Empty);
                        result = WebServiceDao.Login(UserInfo);
                    };
                    backgroundWorker.RunWorkerAsync();
                    backgroundWorker.RunWorkerCompleted += (s, o) =>
                    {
                        if (result.Result)
                        {
                            long id;
                            long.TryParse(result.Values["ID"].ToString(), out id);
                            string token = result.Values["Token"].ToString();
                            string name = result.Values["Name"].ToString();
                            string userGuid = result.Values["USER_GUID"].ToString();
                            BaseService.LanguageLevel[] lang = WebServiceDao.GetLanguage(id);
                            List<UserLanguageModel> userLang = new List<UserLanguageModel>();
                            if (lang != null)
                            {
                                foreach (var item in lang)
                                {
                                    userLang.Add(new UserLanguageModel(
                                        (Category.LanguageType)Enum.Parse(typeof(Category.LanguageType), item.Name)
                                        , (Category.LanguageLevel)Enum.Parse(typeof(Category.LanguageLevel), item.Level.ToString()))
                                    );
                                }
                            }
                            Parameter.GetInstance().UserInfo = new Model.UserInfoModel()
                            {
                                Id = id,
                                Email = UserInfo.Email,
                                Name = name,
                                Token = token,
                                User_Guid = userGuid,
                                Language = userLang
                            };
                            System.Collections.Generic.List<SettingModel> settings;
                            new DataAccess.SettingRespository().GetSetting("System", "TempAccount", out settings);
                            if (settings!= null && settings.Count > 0 && !settings[0].Value.Equals(UserInfo.Email))
                            {
                                File.Copy(CommonUtil.GetFilePath("data.sqlite"), Path.Combine(App.Directory,string.Format( "data_{0}_{1}.sqlite", settings[0].Value,DateTime.Now.ToString("yyyyMMddHHmm"))), true);
                                File.Delete(CommonUtil.GetFilePath("data.sqlite"));
                                BaseDao.CreateDB();
                                CommonUtil.RemoveAllPageParam();
                            }

                            new DataAccess.SettingRespository().SaveSetting(new List<SettingModel>() { new SettingModel() { Category = "System", Key = "TempAccount", Value = UserInfo.Email } });

                            PubSub<object>.RaiseEvent(Parameter.RefreshMenuEventKey, this, null);
                            PubSub<object>.RaiseEvent(Parameter.RefreshNameEventKey, this, null);

                            CommonUtil.SyncFile(true);
                            BaseDao.Dispose();
                            new DataAccess.WordBookRespository().SetCheckIds(new List<long>());
                            CommonUtil.ShowMessageBox(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr("Login")));
                            App.Instance.ChangeContent(Parameter.HomeRootKey);
                        }
                        else
                        {
                            CommonUtil.ShowMessageBox(CommonUtil.GetResourceString(result.Exception));
                        }
                        CommonUtil.HighProcessMainWindowShadow();
                    };

                });
            }
        }

        public ICommand NewAccount
        {
            get
            {
                return new RelayCommand(param =>
                {
                    App.Instance.ChangeContent(Parameter.NewAccountRootKey);
                });
            }
        }

        public ICommand ForgetPassword
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (!CommonUtil.IsValidEmail(UserInfo.Email))
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString("InvalidEmail"));
                        UserInfo.Email = string.Empty;
                        OnPropertyChanged("UserInfo");
                        return;
                    }
                    CommonUtil.SetPageParam(Parameter.ForgetPasswordRootKey, "UserInfo", UserInfo);
                    App.Instance.ChangeContent(Parameter.ForgetPasswordRootKey);
                });
            }
        }
        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion







    }
}
