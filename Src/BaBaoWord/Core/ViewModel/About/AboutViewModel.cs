﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System.Reflection;
using System.Diagnostics;

namespace BaBaoWord.ViewModel
{
    internal class AboutViewModel:EditViewModelBase
    {
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("ProgramName");
            OnPropertyChanged("AllRightsReserved");
            OnPropertyChanged("Copyright");
            OnPropertyChanged("Version");
            OnPropertyChanged("ContactAuthor");
            base.LanguageChangedEvent(sender, e);
        }

        public string ProgramName
        {
            get
            {
                return CommonUtil.GetResourceString("BaBaoWord");
            }
        }

        public string Copyright
        {
            get
            {
                return CommonUtil.GetResourceString("Copyright");
            }
        }
        public string AllRightsReserved
        {
            get
            {
                return CommonUtil.GetResourceString("AllRightsReserved");
            }
        }
        public string Version
        {
            get
            {
                return CommonUtil.GetResourceString("Version") +" "+ FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
            }
        }
        public string ContactAuthor
        {
            get
            {
                return CommonUtil.GetResourceString("ContactAuthor");
            }
        }
        public string Email
        {
            get
            {
                return "BaBaoTech@gmail.com";
            }
        }
    }
}
