﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Model;
using BaBaoWord.DataAccess;
using System.Windows.Input;
using BaBaoWord.Util;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows;
using System.Collections.ObjectModel;
using BaBaoWord.View;
using BaBaoWord.Core.MVVM;

namespace BaBaoWord.ViewModel
{
    internal class WordBookWordListViewModel : EditViewModelBase
    {
        private WordReadRespository _wordReadDao = new WordReadRespository();
        private WordBookRespository _wordBookDao = new WordBookRespository();
        private WordRespository _wordDao = new WordRespository();
        private WordBookMappingRespository _wordbookMappingDao = new WordBookMappingRespository();
        private bool _stopAddWord = false;
        private bool _loadingComplate = false;
        private AsyncObservableCollection<WordModel> _tempWords = new AsyncObservableCollection<WordModel>();


        public WordBookWordListViewModel(string wordListType)
        {
            WordListType = wordListType;
            if (WordBookSelIds.Count == 0)
            {
                ShowBookDialog();
            }
            LoadData();
            PubSub<object>.AddEvent(Parameter.ReloadWordBookWordsEventKey, LoadData);

            _sound = new MediaElement();
            _sound.LoadedBehavior = MediaState.Manual;
            _sound.MediaEnded += _sound_MediaEnded;


        }

        private void LoadData(object sender, PubSubEventArgs<object> args)
        {
            LoadData();
        }

        private void LoadData()
        {
            List<WordModel> words = new List<WordModel>(); ;
            switch (WordListType)
            {
                case "All":
                    _wordReadDao.GetWords(WordBookSelIds, out words);
                    if (words.Count > 0)
                    {
                        ShowSearchImg = Visibility.Visible;
                    }
                    else
                    {
                        ShowSearchImg = Visibility.Collapsed;
                    }
                    break;
                case "UnRead":
                    _wordReadDao.GetUnReadWords(WordBookSelIds, out words);
                    if (words.Count > 0)
                    {
                        ShowSearchImg = Visibility.Visible;
                    }
                    else
                    {
                        ShowSearchImg = Visibility.Collapsed;
                    }
                    break;
                case "Read":
                    _wordReadDao.GetReadWords(WordBookSelIds, out words);
                    if (words.Count > 0)
                    {
                        ShowReturnUnreadImg = Visibility.Visible;
                    }
                    else
                    {
                        ShowReturnUnreadImg = Visibility.Collapsed;
                    }
                    break;
            }
            ShowWords(words,false);
        }

        private void ShowWords(List<WordModel> words,bool isSearch)
        {
            _stopAddWord = true;
            Words.Clear();
            if (isSearch)
            {
                System.Threading.Thread.Sleep(500);
            }
            BackgroundWorker _showWords = new BackgroundWorker();
            _stopAddWord = false;
            _loadingComplate = false;
            _showWords.DoWork += (s, o) =>
            {
                foreach (WordModel word in words)
                {
                    if (_stopAddWord)
                    {
                        break;
                    }
                    try
                    {
                        Words.Add(word);
                    }
                    catch
                    {
                    }
                    System.Threading.Thread.Sleep(10);
                }
                if (!isSearch)
                {
                    _tempWords = new AsyncObservableCollection<WordModel>(words);
                }
            };
            _showWords.RunWorkerCompleted += (s, o) =>
            {
                _loadingComplate = true;
            };
            _showWords.RunWorkerAsync();
        }

        #region Binding

        #region DataContent


        private readonly MediaElement _sound;
        public MediaElement Sound
        {
            get { return _sound; }
        }

        private List<long> WordBookSelIds
        {
            get
            {
                List<long> ids = new List<long>();
                object value = CommonUtil.GetPageParam(Parameter.WordBookRootKey, "WordBookSelIds");
                if (value != null && value is List<long>)
                {
                    ids = (List<long>)value;
                    _wordBookDao.SetCheckIds(ids);
                }
                return ids;
            }
        }

        private AsyncObservableCollection<WordModel> _words = new AsyncObservableCollection<WordModel>();
        public AsyncObservableCollection<WordModel> Words
        {
            get
            {
                return _words;
            }
            set
            {
                _words = value;
                //OnPropertyChanged("Words");
            }
        }

        private string _wordListType;

        public string WordListType
        {
            get
            {
                return _wordListType;
            }
            set
            {
                _wordListType = value;
            }
        }

        public string ReturnAllUnreadStr
        {
            get
            {
                return CommonUtil.GetResourceString("ReturnAllUnread");
            }
        }

        private Visibility _showReturnUnreadImg = Visibility.Collapsed;
        public Visibility ShowReturnUnreadImg
        {
            get
            {
                return _showReturnUnreadImg;
            }
            set
            {
                _showReturnUnreadImg = value;
                OnPropertyChanged("ShowReturnUnreadImg");
            }
        }

        public string SearchWordStr
        {
            get
            {
                return CommonUtil.GetResourceString("SearchWord");
            }
        }

        private Visibility _ShowSearchImg = Visibility.Collapsed;
        public Visibility ShowSearchImg
        {
            get
            {
                return _ShowSearchImg;
            }
            set
            {
                _ShowSearchImg = value;
                OnPropertyChanged("ShowSearchImg");
            }
        }

        private Visibility _showSearchForm = Visibility.Hidden;
        public Visibility ShowSearchForm
        {
            get
            {
                return _showSearchForm;
            }
            set
            {
                _showSearchForm = value;
                OnPropertyChanged("ShowSearchForm");
            }
        }

        private bool _isSearchFocus = false;
        public bool IsSearchFocus
        {
            get
            {
                return _isSearchFocus;

            }
            set
            {
                _isSearchFocus = value;
                OnPropertyChanged("IsSearchFocus");
            }
        }

        private Visibility _isShowExplanation = Visibility.Visible;
        public Visibility IsShowExplanation
        {
            get
            {
                return _isShowExplanation;
            }
            set
            {
                _isShowExplanation = value;
                OnPropertyChanged("IsShowExplanation");
            }
        }

        private Visibility _isShowHiddenValue = Visibility.Hidden;
        public Visibility IsShowHiddenValue
        {
            get
            {
                return _isShowHiddenValue;
            }
            set
            {
                _isShowHiddenValue = value;
                OnPropertyChanged("IsShowHiddenValue");
            }
        }

        public string EnhanceMemoryStr
        {
            get
            {
                return CommonUtil.GetResourceString("EnhanceMemory");
            }
        }

        public string RememberStr
        {
            get
            {
                return CommonUtil.GetResourceString("Remember");
            }
        }

        public string EditEnhanceImg
        {
            get
            {
                return Parameter.EditEnhanceImg;
            }
        }

        public string SupportStr
        {
            get
            {
                return CommonUtil.GetResourceString("HotKey") + Environment.NewLine +
                    "F1:" + CommonUtil.GetResourceString("SearchWord") + Environment.NewLine +
                    "F4:" + CommonUtil.GetResourceString("SwitchExplanation") 
                    ;
            }
        }
        #endregion

        #region Command
        private bool playEndFlag = true;
        public ICommand PlaySound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param != null && param is string)
                    {
                        string[] structure = param.ToString().Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                        if (structure.Length != 2)
                        {
                            return;
                        }
                        string path = string.Empty;
                        if (structure[1].Equals("Temp"))
                        {
                            path = Parameter.GetInstance().Mp3TempDirectory;
                        }
                        else if (structure[1].Equals("Real"))
                        {
                            path = Parameter.GetInstance().Mp3Directory;
                            CommonUtil.GetSound(param.ToString());
                        }
                        else
                        {
                            return;
                        }
                        string[] words = structure[0].Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            int i = 0;
                            while (i < words.Length)
                            {
                                if (playEndFlag)
                                {
                                    if (i != 0)
                                    {
                                        //System.Threading.Thread.Sleep(50);
                                    }
                                    playEndFlag = false;
                                    App.Current.Dispatcher.Invoke((Action)delegate
                                    {

                                        Sound.Source = new Uri(System.IO.Path.Combine(path, words[i] + ".mp3"), UriKind.Absolute);
                                        Sound.Play();
                                    });
                                    i++;
                                }
                                System.Threading.Thread.Sleep(100);
                            }
                        };
                        backgroundWorker.RunWorkerAsync();
                        IsSearchFocus = true;
                    }
                });
            }
        }

        void _sound_MediaEnded(object sender, RoutedEventArgs e)
        {
            playEndFlag = true;
            Sound.Source = null;
        }


        public ICommand View
        {
            get
            {
                return new RelayCommand(param =>
                {
                    WordModel viewWord;
                    if (_wordDao.GetWord(Words[(int)param].Id.Value, out viewWord))
                    {
                        ShowViewWord(viewWord);
                        _wordReadDao.SaveReadWord(WordBookSelIds, Words[(int)param].Id.Value);
                        LoadData();
                    }
                });
            }
        }

        public ICommand Edit
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        CommonUtil.SetPageParam(Parameter.EditWordRootKey, "WordId", Words[(int)param].Id.Value);
                        App.Instance.ChangeContent(Parameter.EditWordRootKey);
                    }
                });
            }
        }

        public ICommand ForgetWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        BaseDao.IncreaseForget(Words[(int)param].Id.Value);
                        Words[(int)param].ShowForget = false;
                    }
                });
            }
        }

        public ICommand ReturnForget
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        BaseDao.ReturnForgetZero(Words[(int)param].Id.Value);
                    }
                });
            }
        }

        public ICommand Remove
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        string msg = string.Format(CommonUtil.GetResourceString("ConfirmDelItem"), Words[(int)param].Word);
                        if (CommonUtil.ShowConfirmMessageBoxWithShadow(msg))
                        {
                            List<WordBookMapping> wordbookMappings;
                            if (_wordbookMappingDao.GetWordbookMapping(null, Words[(int)param].Id.Value, out wordbookMappings))
                            {
                                bool removeWord = false;
                                if (wordbookMappings.Count == 1)
                                {
                                    removeWord = true;
                                }
                                else if (WordBookSelIds.Count >= wordbookMappings.Count)
                                {

                                    int containCount = 0;
                                    foreach (WordBookMapping wordbookMapping in wordbookMappings)
                                    {
                                        if (WordBookSelIds.Contains(wordbookMapping.WordbookId))
                                        {
                                            containCount++;
                                        }
                                    }
                                    if (containCount == wordbookMappings.Count)
                                    {
                                        removeWord = true;
                                    }
                                }
                                if (removeWord)
                                {
                                    _wordDao.Remove(Words[(int)param].Id.Value);
                                }
                                else
                                {
                                    foreach (long wordbookId in WordBookSelIds)
                                    {
                                        if (wordbookMappings.Where(item => item.WordbookId.Equals(wordbookId)).Any())
                                        {
                                            _wordbookMappingDao.RemoveWordbookMapping(wordbookId, Words[(int)param].Id.Value);
                                        }
                                    }
                                }
                            }
                            LoadData();
                        }
                    }
                });
            }
        }

        public ICommand ReturnAllUnread
        {
            get
            {
                return new RelayCommand(param =>
                {
                    _wordReadDao.ReturnAllUnRead(WordBookSelIds);
                    LoadData();
                });
            }
        }

        public ICommand ShowSearchWordForm
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (ShowSearchImg == Visibility.Collapsed)
                    {
                        return;
                    }
                    if (ShowSearchForm == Visibility.Visible)
                    {
                        ShowSearchForm = Visibility.Hidden;
                        IsSearchFocus = false;
                    }
                    else
                    {
                        ShowSearchForm = Visibility.Visible;
                        IsSearchFocus = true;
                    }
                });
            }
        }

        public ICommand SearchWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    string searchKey = string.Empty;
                    if (param != null)
                    {
                        searchKey = param.ToString();
                    }
                    CommonUtil.ShowProcessMainWindowShadow("Searching");
                    BackgroundWorker searchWorker = new BackgroundWorker();
                    searchWorker.DoWork += (s, o) =>
                    {
                        while (true)
                        {
                            if (_loadingComplate)
                            {
                                break;
                            }
                            System.Threading.Thread.Sleep(100);
                        }
                        ShowWords(_tempWords.Where(item => item.Word.IndexOf(searchKey) > -1).ToList(),true);
                    };
                    searchWorker.RunWorkerCompleted += (s, o) =>
                    {
                        CommonUtil.HighProcessMainWindowShadow();
                    };
                    searchWorker.RunWorkerAsync();
                });
            }
        }

        public ICommand SwitchExplanationTextView
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (IsShowExplanation == Visibility.Hidden)
                    {
                        IsShowExplanation = Visibility.Visible;
                    }
                    else
                    {
                        IsShowExplanation = Visibility.Hidden;
                    }
                });
            }
        }

        public ICommand SwitchHiddenTextView
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (IsShowHiddenValue == Visibility.Hidden)
                    {
                        IsShowHiddenValue = Visibility.Visible;
                    }
                    else
                    {
                        IsShowHiddenValue = Visibility.Hidden;
                    }
                });
            }
        }

        public ICommand ShowBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowBookDialog();
                });
            }
        }

        private void ShowBookDialog()
        {
            CommonUtil.ShowMainWindowShadow();
            WordBookControl workBookControl = new WordBookControl();
            if (workBookControl.ShowDialog().Value)
            {
                List<long> ids;
                _wordBookDao.GetCheckIds(out ids);
                CommonUtil.SetPageParam(Parameter.WordBookRootKey, "WordBookSelIds", ids);
            }
            CommonUtil.HideMainWindowShadow();
        }

        private void ShowViewWord(WordModel word)
        {

            CommonUtil.ShowMainWindowShadow();
            WordBookViewWord wordBookViewWord = new WordBookViewWord();
            WordBookViewWordViewModel wordBookViewWordViewModel = new WordBookViewWordViewModel(word);
            wordBookViewWord.DataContext = wordBookViewWordViewModel;
            if (wordBookViewWord.ShowDialog().Value)
            {

            }
            CommonUtil.HideMainWindowShadow();
        }


        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion



        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("EnhanceMemoryStr");
            OnPropertyChanged("RememberStr");
            OnPropertyChanged("ReturnAllUnreadStr");
            OnPropertyChanged("SearchWordStr");
            OnPropertyChanged("SupportStr");
            foreach (WordModel word in Words)
            {
                word.OnPropertyChanged("HiddenTooltip");
            }
            base.LanguageChangedEvent(sender, e);
        }





    }
}
