﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using System.Windows.Input;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media.Animation;
using BaBaoWord.Util;
using BaBaoWord.Model;
using System.Windows.Controls;
using BaBaoWord.DataAccess;

namespace BaBaoWord.ViewModel
{
    internal class DesktopWordViewModel : EditViewModelBase
    {
        public DesktopWordViewModel()
        {
        }

        public DesktopWordViewModel(WordModel word, RotateSettingModel setting, int wordIndex, int total)
        {
            Word = word;
            OpacityValue = setting.Opacity;
            PubSub<object>.AddEvent(Parameter.CloseRotateWordEventKey, CloseExecute);
            _sound = new MediaElement();
            _sound.Volume = 1;
            _sound.LoadedBehavior = MediaState.Manual;
            _sound.MediaEnded += _sound_MediaEnded;
            WordNum = wordIndex + " / " + total;
            Setting = setting;
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("DetailStr");
            OnPropertyChanged("RememberStr");
            OnPropertyChanged("EnhanceMemoryStr");
            OnPropertyChanged("SupportStr");

            base.LanguageChangedEvent(sender, e);
        }

        //public static readonly RoutedEvent ClosedEvent = EventManager.RegisterRoutedEvent(
        //    "Closed", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesktopWordViewModel));

        #region Binding

        #region DataContent


        private double _windowLeft = 0;
        public double WindowLeft
        {
            get
            {
                return _windowLeft;
            }
            set
            {
                _windowLeft = value;
                OnPropertyChanged("WindowLeft");
            }
        }

        private double _windowTop = 0;
        public double WindowTop
        {
            get
            {
                return _windowTop;
            }
            set
            {
                _windowTop = value;
                OnPropertyChanged("WindowTop");
            }
        }

        private double _windowHeight = 69;
        public double WindowHeight
        {
            get
            {
                return _windowHeight;
            }
            set
            {
                _windowHeight = value;
                OnPropertyChanged("WindowHeight");
            }
        }

        private double _windowWidth = 200;
        public double WindowWidth
        {
            get
            {
                return _windowWidth;
            }
            set
            {
                _windowWidth = value;
                OnPropertyChanged("WindowWidth");
            }
        }

        private bool _show = false;
        public bool Show
        {
            get
            {
                return _show;
            }
            set
            {
                _show = value;
                OnPropertyChanged("Show");
            }
        }

        private bool _showWindow = true;
        public bool ShowWindow
        {
            get
            {
                return _showWindow;
            }
            set
            {
                _showWindow = value;
                OnPropertyChanged("ShowWindow");
            }
        }

        private bool _showExplanation = false;
        public bool ShowExplanation
        {
            get
            {
                return _showExplanation;
            }
            set
            {
                _showExplanation = value;
                OnPropertyChanged("ShowExplanation");
            }
        }
        public string WordNum { get; set; }

        private Visibility _showDetail = Visibility.Hidden;
        public Visibility ShowDetail
        {
            get
            {
                return _showDetail;
            }
            set
            {
                _showDetail = value;
                OnPropertyChanged("ShowDetail");
            }
        }

        private Uri _showWordSoundPath;
        public Uri ShowWordSoundPath
        {
            get
            {
                return _showWordSoundPath;
            }
            set
            {
                _showWordSoundPath = value;
                OnPropertyChanged("ShowWordSoundPath");
            }
        }

        private WordModel _word;
        public WordModel Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value;
                OnPropertyChanged("Word");
            }
        }

        private readonly MediaElement _sound;
        public MediaElement Sound
        {
            get { return _sound; }
        }

        public string CancelImg
        {
            get
            {
                return Parameter.IconPath + "ic_action_cancel.png";
            }
        }

        public string DetailStr
        {
            get
            {
                return CommonUtil.GetResourceString("Detail");
            }
        }

        public double OpacityValue { get; set; }

        public RotateSettingModel Setting { get; set; }

        public string EnhanceMemoryStr
        {
            get
            {
                return CommonUtil.GetResourceString("EnhanceMemory");
            }
        }

        public string RememberStr
        {
            get
            {
                return CommonUtil.GetResourceString("Remember");
            }
        }

        public string EditEnhanceImg
        {
            get
            {
                return Parameter.EditEnhanceImg;
            }
        }

        public string SupportStr
        {
            get
            {
                return CommonUtil.GetResourceString("HotKey") + Environment.NewLine +
                    "F1:" + CommonUtil.GetResourceString("View") + Environment.NewLine +
                    "F2:" + CommonUtil.GetResourceString("PreviousWord") + Environment.NewLine +
                    "F3:" + CommonUtil.GetResourceString("NextWord") + Environment.NewLine +
                    "F5:" + CommonUtil.GetResourceString("PlaySound") + Environment.NewLine +
                    "F6:" + CommonUtil.GetResourceString("Remember") + Environment.NewLine +
                    "F7:" + CommonUtil.GetResourceString("EnhanceMemory") + Environment.NewLine +
                    "F8:" + CommonUtil.GetResourceString("Pause")+ Environment.NewLine +
                    "ESC:" + CommonUtil.GetResourceString("Close") 
                    ;
            }
        }
        #endregion

        #region Command
        public ICommand DesktopWord_Loaded
        {
            get
            {
                return new RelayCommand(param =>
                {
                    Rect workAreaRectangle = System.Windows.SystemParameters.WorkArea;
                    WindowLeft = workAreaRectangle.Right - 200 - 2;
                    WindowTop = workAreaRectangle.Bottom - 440 - 2;

                    Show = true;
                    if (Setting.BroadcastSound)
                    {
                        if (Setting.Interval >= 1)
                        {
                            ShowWordSoundPath = new Uri(Parameter.RotateShowWordSound, UriKind.Relative);
                        }
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            if (Setting.Interval >= 1)
                            {
                                System.Threading.Thread.Sleep(6000);
                            }
                            else
                            {
                                System.Threading.Thread.Sleep(500);
                            }
                            if (Show)
                            {
                                PlaySoundExecute();
                            }
                        };
                        backgroundWorker.RunWorkerAsync();
                    }
                    ShowExplanation = false;
                    if (Setting.DelayShowExplanation > 0)
                    {
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            System.Threading.Thread.Sleep(Setting.DelayShowExplanation * 1000);
                        };
                        backgroundWorker.RunWorkerAsync();
                        backgroundWorker.RunWorkerCompleted += (s, o) =>
                        {
                            ShowExplanation = true;
                        };
                    }
                    else
                    {
                        ShowExplanation = true;
                    }


                });
            }
        }

        public ICommand Close
        {
            get
            {
                return new RelayCommand(param =>
                {
                    Closing();

                });
            }
        }

        private void Closing()
        {
            Show = false;
            ShowWordSoundPath = null;
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                System.Threading.Thread.Sleep(1000);
                ShowWindow = false;
            };
            backgroundWorker.RunWorkerAsync();
        }

        public ICommand View
        {
            get
            {
                return new RelayCommand(param =>
                {
                    WordRespository wordDao = new WordRespository();
                    WordModel tempWord = null;
                    if (wordDao.GetWord(Word.Id.Value, out tempWord))
                    {
                        Word = tempWord;
                    }
                    if (ShowDetail == Visibility.Hidden)
                    {
                        ShowDetail = Visibility.Visible;
                    }
                    else if (ShowDetail == Visibility.Visible)
                    {
                        ShowDetail = Visibility.Hidden;
                    }
                });
            }
        }

        private bool playEndFlag = true;
        public ICommand PlaySound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    PlaySoundExecute();
                });
            }
        }

        private void PlaySoundExecute()
        {
            if (!string.IsNullOrEmpty(Word.Sound))
            {
                string[] structure = Word.Sound.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if (structure.Length != 2)
                {
                    return;
                }
                string path = string.Empty;
                if (structure[1].Equals("Temp"))
                {
                    path = Parameter.GetInstance().Mp3TempDirectory;
                }
                else if (structure[1].Equals("Real"))
                {
                    path = Parameter.GetInstance().Mp3Directory;
                    CommonUtil.GetSound(Word.Sound.ToString());
                }
                else
                {
                    return;
                }
                string[] words = structure[0].Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.DoWork += (s, o) =>
                {
                    int i = 0;
                    while (i < words.Length)
                    {
                        if (playEndFlag)
                        {
                            if (i != 0)
                            {
                                //System.Threading.Thread.Sleep(50);
                            }
                            playEndFlag = false;
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {

                                Sound.Source = new Uri(System.IO.Path.Combine(path, words[i] + ".mp3"), UriKind.Absolute);
                                Sound.Play();
                            });
                            i++;
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                };
                backgroundWorker.RunWorkerAsync();

            }
        }

        void _sound_MediaEnded(object sender, RoutedEventArgs e)
        {
            playEndFlag = true;
            Sound.Source = null;
        }

        public ICommand CloseDetail
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowDetail = Visibility.Hidden;
                });
            }
        }

        public ICommand ForgetWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.IncreaseForget(Word.Id.Value);
                    Word.ShowForget = false;
                });
            }
        }

        public ICommand ReturnForget
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.ReturnForgetZero(Word.Id.Value);
                });
            }
        }

        public ICommand NextWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    PubSub<object>.RaiseEvent(Parameter.NextRotateWordEventKey, this, null);
                });
            }
        }

        public ICommand PreviousWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    PubSub<object>.RaiseEvent(Parameter.PreviousRotateWordEventKey, this, null);
                });
            }
        }

        public ICommand Pause
        {
            get
            {
                return new RelayCommand(param =>
                {
                    PubSub<object>.RaiseEvent(Parameter.PauseRotateEventKey, this, null);
                });
            }
        }
        #endregion

        #endregion

        #region Publish-Subscribe
        private void CloseExecute(object sender, PubSubEventArgs<object> args)
        {
            Closing();
        }
        #endregion
    }
}
