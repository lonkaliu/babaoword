﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.View;
using System.Windows.Input;
using BaBaoWord.Util;

namespace BaBaoWord.ViewModel
{
    internal class WordBookMainViewModel : EditViewModelBase
    {

        public WordBookMainViewModel()
        {
            SelectTabValue = "All";
            WordBookWordListViewModel wordBookWordListViewModel = new WordBookWordListViewModel(SelectTabValue);
            WordBookWordList wordBookWordList = new WordBookWordList();
            wordBookWordList.DataContext = wordBookWordListViewModel;
            WordBookContent = wordBookWordList;
        }


        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("AllStr");
            OnPropertyChanged("ReadStr");
            OnPropertyChanged("UnReadStr");
            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent

        private object _wordBookContent;
        public object WordBookContent
        {
            get
            {
                return _wordBookContent;
            }
            set
            {
                _wordBookContent = value;
                OnPropertyChanged("WordBookContent");
            }
        }

        private string _selectTabValue;
        public string SelectTabValue
        {
            get
            {
                return _selectTabValue;
            }
            set
            {
                _selectTabValue = value;
                OnPropertyChanged("SelectTabValue");
            }
        }

        public string AllStr
        {
            get
            {
                return CommonUtil.GetResourceString("All");
            }
        }
        public string ReadStr
        {
            get
            {
                return CommonUtil.GetResourceString("Read");
            }
        }
        public string UnReadStr
        {
            get
            {
                return CommonUtil.GetResourceString("UnRead");
            }
        }

        private static object _settingViewModel;

        #endregion

        #region Command
        public ICommand SelectTab
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param != null)
                    {
                        SelectTabValue = param.ToString();
                        switch (SelectTabValue)
                        {
                            case "All":
                            case "UnRead":
                            case "Read":
                                WordBookWordListViewModel wordBookWordListViewModel = new WordBookWordListViewModel(SelectTabValue);
                                WordBookWordList wordBookWordList = new WordBookWordList();
                                wordBookWordList.DataContext = wordBookWordListViewModel;
                                WordBookContent = wordBookWordList;
                                break;
                            case "Setting":
                                if (_settingViewModel == null)
                                {
                                    _settingViewModel = new WordBookSetting();
                                }
                                WordBookContent = _settingViewModel;
                                break;
                        }

                    }
                });
            }
        }


        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion


    }
}
