﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System.Windows.Input;
using BaBaoWord.View;
using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Collections.ObjectModel;
using BaBaoWord.Category;

namespace BaBaoWord.ViewModel
{
    internal class WordBookSettingViewModel : EditViewModelBase
    {
        private WordBookRespository _wordBookDao = new WordBookRespository();
        private SettingRespository _settingDao = new SettingRespository();
        private WordReadRespository _wordReadDao = new WordReadRespository();
        private int _wordIndex = -1;
        private List<int> _randomWordList = new List<int>();
        private List<WordModel> _rotateWords = new List<WordModel>();
        private bool _closeWordFlag = false;
        private RotateControl _rotateControl = RotateControl.Next;
        private enum RotateControl
        {
            Previous,
            Next,

        }

        public WordBookSettingViewModel()
        {
            _settingDao.GetRatateSetting(out _rotateSetting);
            _settingDao.GetWordbookSetting(out _wordBookSetting);
            PubSub<object>.AddEvent(Parameter.NextRotateWordEventKey, NextWord);
            PubSub<object>.AddEvent(Parameter.PreviousRotateWordEventKey, PreviousWord);
            PubSub<object>.AddEvent(Parameter.PauseRotateEventKey, PauseWord);
            PauseAndStartImg = Parameter.EditPauseImg;
            ShowWordSetting = true;
        }

        private bool _switchLanguage = false;
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            _switchLanguage = true;

            OnPropertyChanged("SortByList");
            OnPropertyChanged("CompareList");
            OnPropertyChanged("WordFilters");
            OnPropertyChanged("ChangeWordBookStr");
            OnPropertyChanged("RotateSettingStr");
            OnPropertyChanged("WordbookSettingStr");
            OnPropertyChanged("BroadcastSoundStr");
            OnPropertyChanged("IntervalStr");
            OnPropertyChanged("WorkTimeStr");
            OnPropertyChanged("LunchTimeStr");
            OnPropertyChanged("OffWorkTimeStr");
            OnPropertyChanged("MinuteStr");
            OnPropertyChanged("OpacityStr");
            OnPropertyChanged("ShowAgainStr");
            OnPropertyChanged("RandomStr");
            OnPropertyChanged("SecondStr");
            OnPropertyChanged("DelayExplanationStr");
            OnPropertyChanged("PauseAndStartStr");
            OnPropertyChanged("AllWordStr");
            OnPropertyChanged("ExportStr");
            OnPropertyChanged("SortByStr");
            OnPropertyChanged("FilterByStr");
            OnPropertyChanged("FilterBy");
            OnPropertyChanged("RotateSetting");
            OnPropertyChanged("WordBookSetting");
            OnPropertyChanged("WorkBookSettingStr");
            foreach (var item in WordBookSetting.WordFilters)
            {
                item.OnPropertyChanged("WordSort");
                item.OnPropertyChanged("Compare");
            }

            _switchLanguage = false;

            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent

        public string SecondStr
        {
            get
            {
                return CommonUtil.GetResourceString("Second");
            }
        }

        public string DelayExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("DelayExplanation");
            }
        }
        public string ChangeWordBookStr
        {
            get
            {
                return CommonUtil.GetResourceString("ChangeWordBook");
            }
        }
        public string RandomStr
        {
            get
            {
                return CommonUtil.GetResourceString("Random");
            }
        }

        public string RotateSettingStr
        {
            get
            {
                return CommonUtil.GetResourceString("RotateSetting");
            }
        }
        public string WorkBookSettingStr
        {
            get
            {
                return CommonUtil.GetResourceString("WorkBookSetting");
            }
        }

        public string BroadcastSoundStr
        {
            get
            {
                return CommonUtil.GetResourceString("BroadcastSound");
            }
        }

        public string IntervalStr
        {
            get
            {
                return CommonUtil.GetResourceString("Interval");
            }
        }

        public string WorkTimeStr
        {
            get
            {
                return CommonUtil.GetResourceString("WorkTime");
            }
        }

        public string LunchTimeStr
        {
            get
            {
                return CommonUtil.GetResourceString("LunchTime");
            }
        }

        public string OffWorkTimeStr
        {
            get
            {
                return CommonUtil.GetResourceString("OffWorkTime");
            }
        }

        public string MinuteStr
        {
            get
            {
                return CommonUtil.GetResourceString("Minute");
            }
        }

        public string ExportStr
        {
            get
            {
                return CommonUtil.GetResourceString("Export");
            }
        }

        public string OpacityStr
        {
            get
            {
                return CommonUtil.GetResourceString("Opacity");
            }
        }

        public string ShowAgainStr
        {
            get
            {
                return CommonUtil.GetResourceString("ShowAgain");
            }
        }

        private string _pauseAndStartStr = "Pause";
        public string PauseAndStartStr
        {
            get
            {
                return CommonUtil.GetResourceString(_pauseAndStartStr);
            }
        }

        public string AllWordStr
        {
            get
            {
                return CommonUtil.GetResourceString("AllWord");
            }
        }

        public string WordStr
        {
            get
            {
                return CommonUtil.GetResourceString("Word"); ;
            }
        }
        public string CountStr
        {
            get
            {
                return CommonUtil.GetResourceString("Count"); ;
            }
        }

        public string PronunciationStr
        {
            get
            {
                return CommonUtil.GetResourceString("Pronunciation"); ;
            }
        }

        public string WeightStr
        {
            get
            {
                return CommonUtil.GetResourceString("Weight"); ;
            }
        }

        public string ForgetCountStr
        {
            get
            {
                return CommonUtil.GetResourceString("ForgetCount"); ;
            }
        }

        public string ImportanceStr
        {
            get
            {
                return CommonUtil.GetResourceString("Importance"); ;
            }
        }

        public string ExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("Explanation"); ;
            }
        }

        public string SpeechStr
        {
            get
            {
                return CommonUtil.GetResourceString("Speech"); ;
            }
        }
        public string SentenceStr
        {
            get
            {
                return CommonUtil.GetResourceString("Sentence"); ;
            }
        }

        public string SortByStr
        {
            get
            {
                return CommonUtil.GetResourceString("SortBy"); ;
            }
        }
        public string FilterByStr
        {
            get
            {
                return CommonUtil.GetResourceString("FilterBy"); ;
            }
        }

        public string PauseAndStartImg { get; set; }

        private ObservableCollection<WordSortModel> _sortByList;
        public ObservableCollection<WordSortModel> SortByList
        {
            get
            {
                if (_switchLanguage || _sortByList == null)
                {
                    _sortByList = new ObservableCollection<WordSortModel>();
                    foreach (WordSortType sortType in Enum.GetValues(typeof(WordSortType)))
                    {
                        _sortByList.Add(new WordSortModel(sortType));
                    }
                }
                return _sortByList;
            }
        }

        private ObservableCollection<WordSortModel> _filterSortByList;
        public ObservableCollection<WordSortModel> FilterSortByList
        {
            get
            {
                if (_switchLanguage || _filterSortByList == null)
                {
                    _filterSortByList = new ObservableCollection<WordSortModel>();
                    foreach (WordSortType sortType in Enum.GetValues(typeof(WordSortType)))
                    {
                        if(sortType == WordSortType.ByWord || sortType == WordSortType.ByImportance|| sortType == WordSortType.ById)
                        {
                            continue;
                        }
                        _filterSortByList.Add(new WordSortModel(sortType));
                    }
                }
                return _filterSortByList;
            }
        }

        private ObservableCollection<CompareModel> _compareList;
        public ObservableCollection<CompareModel> CompareList
        {
            get
            {
                if (_switchLanguage || _compareList == null)
                {
                    _compareList = new ObservableCollection<CompareModel>();
                    foreach (CompareType compareType in Enum.GetValues(typeof(CompareType)))
                    {
                        _compareList.Add(new CompareModel(compareType));
                    }
                }
                return _compareList;
            }
        }


        public bool IsPause { get; set; }

        private bool _isStartRotate = false;
        public bool IsStartRotate
        {
            get
            {
                return _isStartRotate;
            }
            set
            {
                _isStartRotate = value;
                OnPropertyChanged("IsStartRotate");
            }
        }

        public bool IsShowWord { get; set; }

        public bool AgainEnabled
        {
            get
            {
                return !IsShowWord && RotateWord != null;
            }
        }

        public WordModel RotateWord { get; set; }

        private RotateSettingModel _rotateSetting = new RotateSettingModel();
        public RotateSettingModel RotateSetting
        {
            get
            {
                return _rotateSetting;
            }
            set
            {
                _rotateSetting = value;
                OnPropertyChanged("RotateSetting");
            }
        }

        private WordBookSettingModel _wordBookSetting = new WordBookSettingModel();
        public WordBookSettingModel WordBookSetting
        {
            get
            {
                return _wordBookSetting;
            }
            set
            {
                _wordBookSetting = value;
                OnPropertyChanged("WordBookSetting");
            }
        }
        private int previousCount = 0;

        

        public bool ShowWordSetting { get; set; }
        public bool ShowRotateSetting { get; set; }

        #endregion

        #region Command
        public ICommand ChangeWordBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowBookDialog();
                });
            }
        }

        private void ShowBookDialog()
        {
            CommonUtil.ShowMainWindowShadow();
            WordBookControl workBookControl = new WordBookControl();
            if (workBookControl.ShowDialog().Value)
            {
                List<long> ids;
                _wordBookDao.GetCheckIds(out ids);
                CommonUtil.SetPageParam(Parameter.WordBookRootKey, "WordBookSelIds", ids);
            }
            CommonUtil.HideMainWindowShadow();
        }

        private List<long> WordBookSelIds
        {
            get
            {
                List<long> ids = new List<long>();
                object value = CommonUtil.GetPageParam(Parameter.WordBookRootKey, "WordBookSelIds");
                if (value != null && value is List<long>)
                {
                    ids = (List<long>)value;
                    _wordBookDao.SetCheckIds(ids);
                }
                return ids;
            }
        }


        public ICommand PauseAndStart
        {
            get
            {
                return new RelayCommand(param =>
                {
                    DoPause();
                });
            }
        }

        private void DoPause()
        {
            if (IsPause)
            {
                _pauseAndStartStr = "Pause";
                PauseAndStartImg = Parameter.EditPauseImg;
                IsPause = false;
            }
            else
            {
                _pauseAndStartStr = "Start";
                PauseAndStartImg = Parameter.EditStartImg;
                IsPause = true;
            }
            OnPropertyChanged("PauseAndStartStr");
            OnPropertyChanged("PauseAndStartImg");
        }

        public ICommand StartRotate
        {
            get
            {
                return new RelayCommand(param =>
                {
                    previousCount = 0;
                    string errStr = string.Empty;
                    if (RotateSetting.Validation(out errStr))
                    {
                        _settingDao.SaveRotateSetting(RotateSetting);
                        RotateSetting.ClearTimeRangeList();
                        if (WordBookSelIds.Count == 0)
                        {
                            ShowBookDialog();
                            if (WordBookSelIds.Count == 0)
                            {
                                return;
                            }
                        }
                        DoRotate();
                    }
                    else
                    {
                        CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(errStr));
                    }
                });
            }
        }

        public ICommand StopRotate
        {
            get
            {
                return new RelayCommand(param =>
                {
                    IsStartRotate = false;
                    _rotateBackgroundWorker.Abort();
                    _rotateBackgroundWorker.Dispose();
                    HideRotateWord();
                });
            }
        }

        public ICommand ShowAgain
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (!IsShowWord && RotateWord != null)
                    {
                        ShowRotateWord();
                    }
                });
            }
        }

        public ICommand Export
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SaveFileDialog openFileDialog = new SaveFileDialog();
                    openFileDialog.Filter = "Text files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                    if (string.IsNullOrEmpty(Parameter.GetInstance().ExportPath))
                    {
                        openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    }
                    string fileName = string.Empty;
                    List<WordBookModel> wordBooks;
                    WordBookRespository wordBookDao = new WordBookRespository();
                    wordBookDao.GetWordBooks(WordBookSelIds, out wordBooks);
                    if (wordBooks != null)
                    {
                        foreach (WordBookModel wordBook in wordBooks)
                        {
                            fileName += wordBook.Name + "_";
                        }
                    }
                    fileName += "Words_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx";
                    openFileDialog.FileName = fileName;
                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        Parameter.GetInstance().ExportPath = openFileDialog.FileName.Replace(fileName, string.Empty);
                        CommonUtil.ShowProcessMainWindowShadow(string.Empty);
                        bool failure = false;
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            List<WordModel> words;
                            _wordReadDao.GetExportWords(WordBookSelIds, out words);
                            var explanationCount = words.Select(item => item.Explanation.Count).Max();
                            DataTable exportDt = new DataTable();
                            exportDt.Columns.Add("ID");
                            exportDt.Columns.Add("Weight");
                            exportDt.Columns.Add("ForgetCount");
                            exportDt.Columns.Add("Importance");
                            exportDt.Columns.Add("Word");
                            exportDt.Columns.Add("Pronunciation");
                            for (int i = 0; i < explanationCount; i++)
                            {
                                exportDt.Columns.Add("Speech_" + i);
                                exportDt.Columns.Add("Explanation_" + i);
                                exportDt.Columns.Add("Sentence_" + i);
                            }
                            foreach (WordModel item in words)
                            {
                                DataRow dr = exportDt.NewRow();
                                dr["ID"] = item.Id;
                                dr["Weight"] = item.Weight;
                                dr["ForgetCount"] = item.FORGET_COUNT;
                                dr["Importance"] = CommonUtil.GetResourceString(item.Importance.ToString());
                                dr["Word"] = item.Word;
                                if (item.Pronunciation.Count > 0)
                                {
                                    dr["Pronunciation"] = item.Pronunciation[0].Pronunciation;
                                }
                                for (int i = 0; i < item.Explanation.Count; i++)
                                {
                                    dr["Speech_" + i] = CommonUtil.GetResourceString(item.Explanation[i].Speech.ToString());
                                    dr["Explanation_" + i] = item.Explanation[i].Explanation;
                                    dr["Sentence_" + i] = item.Explanation[i].Sentence;

                                }
                                exportDt.Rows.Add(dr);
                            }

                            List<string> columnName = new List<string>()
                            {
                                "ID",WeightStr,ForgetCountStr,ImportanceStr, WordStr, PronunciationStr
                            };
                            for (int i = 0; i < explanationCount; i++)
                            {
                                columnName.Add(SpeechStr);
                                columnName.Add(ExplanationStr);
                                columnName.Add(SentenceStr);
                            }

                            App.Current.Dispatcher.Invoke((Action)delegate
                            {
                                failure = !ExcelUtil.Export(columnName, exportDt, openFileDialog.FileName);
                            });
                        };
                        backgroundWorker.RunWorkerAsync();
                        backgroundWorker.RunWorkerCompleted += (s, o) =>
                        {
                            CommonUtil.HighProcessMainWindowShadow();
                            if (failure)
                            {
                                CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgFailureStr("Export")));
                            }
                            else
                            {
                                CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr("Export")));
                            }
                        };
                    }
                });
            }
        }


        private void NextRotateWord()
        {
            if (previousCount > 0)
            {
                previousCount--;
            }
            _closeWordFlag = true;
            _rotateControl = RotateControl.Next;
        }

        private void PreviousRotateWord()
        {
            if (previousCount == 0 && _wordIndex > 0)
            {
                previousCount += 2;
            }
            else
            {
                if (previousCount > 0 && _wordIndex == 0)
                {

                }
                else
                {
                    previousCount++;
                }
            }
            _closeWordFlag = true;
            _rotateControl = RotateControl.Previous;
        }
        private void ChangeRotateWord()
        {
            if (RotateSetting.Random)
            {
                RotateWord = _rotateWords[_randomWordList[_wordIndex]];
            }
            else
            {
                RotateWord = _rotateWords[_wordIndex];
            }
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                ShowRotateWord();
                if (previousCount == 0)
                {
                    _wordReadDao.SaveReadWord(WordBookSelIds, RotateWord.Id.Value);
                }
            });

            CloseRotateWord();
        }

        private void CloseRotateWord()
        {
            _closeWordFlag = false;
            Sleep((int)(_rotateSetting.Interval * 60 - 1));
            PubSub<object>.RaiseEvent(Parameter.CloseRotateWordEventKey, this, null);
            RotateWord = null;
            System.Threading.Thread.Sleep(1500);
        }

        private bool Sleep(int intervalSec)
        {
            int intervalMicroSec = intervalSec * 1000;
            while (IsStartRotate && intervalMicroSec > 0)
            {
                System.Threading.Thread.Sleep(100);
                if (IsPause)
                {
                    continue;
                }
                if (_closeWordFlag)
                {
                    break;
                }
                intervalMicroSec -= 100;
            }
            return true;
        }


        private void ShowRotateWord()
        {
            IsShowWord = true;
            OnPropertyChanged("AgainEnabled");
            DesktopWord dw = new DesktopWord();
            dw.Closed += dw_Closed;
            DesktopWordViewModel dwVM = new DesktopWordViewModel(RotateWord, _rotateSetting, _wordIndex + 1, _rotateWords.Count);
            dw.DataContext = dwVM;
            dw.Show();
        }

        void dw_Closed(object sender, EventArgs e)
        {
            IsShowWord = false;
            OnPropertyChanged("AgainEnabled");
        }

        private void HideRotateWord()
        {
            IsShowWord = false;
            OnPropertyChanged("AgainEnabled");
            PubSub<object>.RaiseEvent(Parameter.CloseRotateWordEventKey, this, null);
        }

        private DateTime releaseTime = DateTime.Now;
        AbortableBackgroundWorker _rotateBackgroundWorker = null;
        private void DoRotate()
        {
            _rotateBackgroundWorker = new AbortableBackgroundWorker();
            int count;
            if (_wordReadDao.GetWords(WordBookSelIds, out count) && count > 0)
            {
                _rotateWords = new List<WordModel>();
                if (RotateSetting.AllWord)
                {
                    _wordReadDao.GetWords(WordBookSelIds, out _rotateWords);
                }
                else
                {
                    _wordReadDao.GetUnReadWords(WordBookSelIds, out _rotateWords);
                }
                IsStartRotate = true;
                //App.Current.MainWindow.WindowState = System.Windows.WindowState.Minimized;
                _randomWordList = new List<int>();
                if (RotateSetting.Random)
                {
                    Random random = new Random();
                    var weightList = _rotateWords.Select(item => item.Weight).Distinct().ToList();
                    int fCount = 0;
                    foreach (var weightCount in weightList)
                    {
                        var weightWords = _rotateWords.Where(item => item.Weight.Equals(weightCount));
                        var forgetCountList = weightWords.Select(item => item.FORGET_COUNT).Distinct().ToList();
                        int tempCount = 0;
                        List<int> temp = new List<int>();
                        foreach (var forgetCount in forgetCountList)
                        {
                            tempCount = weightWords.Where(item => item.FORGET_COUNT.Equals(forgetCount)).Count();
                            temp = Enumerable.Range(fCount, tempCount)
                                .OrderBy(n => n * n * (new Random()).Next())
                                .ToList();
                            fCount += tempCount;
                            _randomWordList.AddRange(temp);
                        }
                    }
                }

                _rotateBackgroundWorker.DoWork += (s, o) =>
                {
                    if (o.Argument is List<WordModel>)
                    {
                        _rotateWords = o.Argument as List<WordModel>;
                    }
                    else
                    {
                        return;
                    }
                    _wordIndex = -1;
                    while (IsStartRotate)
                    {
                        if (DateTime.Now.Subtract(releaseTime).TotalMinutes > 10)
                        {
                            System.GC.Collect();
                            releaseTime = DateTime.Now;
                        }
                        if (IsPause)
                        {
                            System.Threading.Thread.Sleep(1000);
                            continue;
                        }
                        if (!_rotateSetting.CheckTime(DateTime.Now))
                        {
                            System.Threading.Thread.Sleep((int)(_rotateSetting.Interval * 60 * 1000));
                            continue;
                        }


                        if (_rotateControl == RotateControl.Next)
                        {
                            _wordIndex++;
                        }
                        else if (_rotateControl == RotateControl.Previous)
                        {
                            if (_wordIndex > 0)
                            {
                                _wordIndex--;
                            }
                        }

                        if (_wordIndex == _rotateWords.Count)
                        {
                            bool confirmResult = false;
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {
                                App.Current.MainWindow.WindowState = System.Windows.WindowState.Normal;
                                if (CommonUtil.ShowConfirmMessageBoxWithShadow(CommonUtil.GetResourceString("WordBookFinishPlayReturnAllUnread")))
                                {
                                    _wordReadDao.ReturnAllUnRead(WordBookSelIds);
                                    _wordReadDao.GetUnReadWords(WordBookSelIds, out _rotateWords);
                                    _wordIndex = 0;
                                    confirmResult = true;
                                    //App.Current.MainWindow.WindowState = System.Windows.WindowState.Minimized;
                                }
                                else
                                {
                                    IsStartRotate = false;
                                }
                            });
                            if (!confirmResult)
                            {
                                break;
                            }
                        }


                        ChangeRotateWord();



                    }
                };
                _rotateBackgroundWorker.RunWorkerAsync(_rotateWords);
            }
            else
            {
                CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString("PlzAddWordInBookFirst"));
            }
        }


        public ICommand SaveWordBookSetting
        {
            get
            {
                return new RelayCommand(param =>
                {
                    _settingDao.SaveWordbookSetting(WordBookSetting);
                    Parameter.GetInstance().SortBy = WordBookSetting.SortBy;
                    Parameter.GetInstance().WordFilters = WordBookSetting.WordFilters;
                });
            }
        }

        public ICommand ToggleWordSetting
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowWordSetting = !ShowWordSetting;
                    OnPropertyChanged("ShowWordSetting");
                });
            }
        }
        public ICommand ToggleRotateSetting
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowRotateSetting = !ShowRotateSetting;
                    OnPropertyChanged("ShowRotateSetting");
                });
            }
        }

        public ICommand AddWordFilter
        {
            get
            {
                return new RelayCommand(param =>
                {
                    WordBookSetting.WordFilters.Add(new Model.WordFilterModel() { Compare = CompareType.GreaterThenEqual, WordSort = WordSortType.ByWeight, Value = 0 });
                    OnPropertyChanged("WordBookSetting");
                });
            }
        }
        public ICommand RemoveWordFilter
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param is int)
                    {
                        WordBookSetting.WordFilters.RemoveAt((int)param);
                        OnPropertyChanged("WordBookSetting");
                    }
                });
            }
        }


        #endregion

        #endregion

        #region Publish-Subscribe
        private void NextWord(object sender, PubSubEventArgs<object> args)
        {
            NextRotateWord();
        }
        private void PreviousWord(object sender, PubSubEventArgs<object> args)
        {
            PreviousRotateWord();
        }

        private void PauseWord(object sender, PubSubEventArgs<object> args)
        {
            DoPause();
        }

        #endregion

    }
}
