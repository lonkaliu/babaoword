﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using BaBaoWord.Model;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;

namespace BaBaoWord.ViewModel
{
    internal class WordBookViewWordViewModel : MessageViewModelBase
    {
        public WordBookViewWordViewModel(WordModel word)
        {
            Word = word;
            Title = CommonUtil.GetResourceString("ViewWord");
            _consoleModel.ShowConsole = false;
            _consoleModel.EnabledCountdown = false;
            ConsoleModel = _consoleModel;

            _sound = new MediaElement();
            _sound.LoadedBehavior = MediaState.Manual;
            _sound.MediaEnded += _sound_MediaEnded;
            PlaySoundExecute();
        }
        #region Binding

        #region DataContent
        private WordModel _word;
        public WordModel Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value;
                OnPropertyChanged("Word");
            }
        }

        private readonly MediaElement _sound;
        public MediaElement Sound
        {
            get { return _sound; }
        }

        public string EnhanceMemoryStr
        {
            get
            {
                return CommonUtil.GetResourceString("EnhanceMemory");
            }
        }

        public string RememberStr
        {
            get
            {
                return CommonUtil.GetResourceString("Remember");
            }
        }

        public string EditEnhanceImg
        {
            get
            {
                return Parameter.EditEnhanceImg;
            }
        }

        #endregion

        #region Command
        private bool playEndFlag = true;
        public ICommand PlaySound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    PlaySoundExecute();
                });
            }
        }
        public ICommand ForgetWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.IncreaseForget(Word.Id.Value);
                    Word.ShowForget = false;
                });
            }
        }

        public ICommand ReturnForget
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.ReturnForgetZero(Word.Id.Value);
                });
            }
        }
        private void PlaySoundExecute()
        {
            if (!string.IsNullOrEmpty(Word.Sound))
            {
                string[] structure = Word.Sound.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if (structure.Length != 2)
                {
                    return;
                }
                string path = string.Empty;
                if (structure[1].Equals("Temp"))
                {
                    path = Parameter.GetInstance().Mp3TempDirectory;
                }
                else if (structure[1].Equals("Real"))
                {
                    path = Parameter.GetInstance().Mp3Directory;
                    CommonUtil.GetSound(Word.Sound.ToString());
                }
                else
                {
                    return;
                }
                string[] words = structure[0].Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.DoWork += (s, o) =>
                {
                    int i = 0;
                    while (i < words.Length)
                    {
                        if (playEndFlag)
                        {
                            if (i != 0)
                            {
                                //System.Threading.Thread.Sleep(50);
                            }
                            playEndFlag = false;
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {

                                Sound.Source = new Uri(System.IO.Path.Combine(path, words[i] + ".mp3"), UriKind.Absolute);
                                Sound.Play();
                            });
                            i++;
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                };
                backgroundWorker.RunWorkerAsync();

            }
        }

        void _sound_MediaEnded(object sender, RoutedEventArgs e)
        {
            playEndFlag = true;
            Sound.Source = null;
        }


        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("RememberStr");
            OnPropertyChanged("EnhanceMemoryStr");
            base.LanguageChangedEvent(sender, e);
        }
    }
}
