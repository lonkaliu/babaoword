﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;

namespace BaBaoWord.ViewModel
{
    internal class MessageConfirmViewModel : MessageViewModelBase
    {
        public MessageConfirmViewModel()
        {
            Title = CommonUtil.GetResourceString("Message");
            _consoleModel.ShowOkBtn = true;
            _consoleModel.EnabledCountdown = false;
            _consoleModel.ShowCancelBtn = true;
            ConsoleModel = _consoleModel;
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            base.LanguageChangedEvent(sender, e);
        }

        private string _msg = string.Empty;
        public string Msg
        {
            get
            {
                return _msg;
            }
            set
            {
                _msg = value;
            }
        }


    }
}
