﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System.Windows.Input;
using System.Collections.ObjectModel;
using BaBaoWord.Model;
using BaBaoWord.DataAccess;

namespace BaBaoWord.ViewModel
{
    internal class SearchWordInWordBookViewModel : MessageViewModelBase
    {
        private WordBookRespository _wordbookDao;

        public SearchWordInWordBookViewModel()
        {
            _wordbookDao = new WordBookRespository();


            Title = CommonUtil.GetResourceString("SearchWord");
            _consoleModel.ShowOkBtn = true;
            _consoleModel.ShowCancelBtn = true;
            _consoleModel.EnabledCountdown = false;
            _wordbookDao = new WordBookRespository();
            ConsoleModel = _consoleModel;
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("WordStr");
            base.LanguageChangedEvent(sender, e);
        }

        public ICommand PreOk
        {
            get
            {
                return new RelayCommand(param =>
                {
                    long wordId = long.MinValue;
                    foreach (SearchWordInWordBookModel word in WordBooks)
                    {
                        if(word.WordBook.IsCheck)
                        {
                            wordId = word.WordId;
                            break;
                        }
                    }
                    PubSub<object>.RaiseEvent(Parameter.ChangeWordEventKey, this, new PubSubEventArgs<object>(wordId));
                });
            }
        }

        public string WordStr
        {
            get
            {
                return CommonUtil.GetResourceString("Word");
            }
        }



        public string Word { get; set; }


        public ICommand Search
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param != null)
                    {
                        Word = param.ToString();
                    }
                    _wordbookDao.GetWordBooks(Word, out _wordBooks);
                    OnPropertyChanged("WordBooks");
                });
            }
        }

        private List<SearchWordInWordBookModel> _wordBooks;
        public List<SearchWordInWordBookModel> WordBooks
        {
            get
            {
                return _wordBooks;
            }
            set
            {
                _wordBooks = value;
            }
        }


    }
}
