﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using BaBaoWord.Model;
using BaBaoWord.Util;
using BaBaoWord.MVVM;

namespace BaBaoWord.ViewModel
{
    internal class MessageAlertViewModel : MessageViewModelBase
    {

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            base.LanguageChangedEvent(sender, e);
        }

        public MessageAlertViewModel()
        {
            Title = CommonUtil.GetResourceString("Message");
            _consoleModel.ShowOkBtn = true;
            _consoleModel.ShowCancelBtn = false;
            ConsoleModel = _consoleModel;
        }

        private string _msg = string.Empty;
        public string Msg
        {
            get
            {
                return _msg;
            }
            set
            {
                _msg =value;
            }
        }

    }
}
