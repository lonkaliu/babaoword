﻿using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using System.Data;
using BaBaoWord.Category;

namespace BaBaoWord.ViewModel
{
    internal class ExamResultViewModel : EditViewModelBase
    {
        ExamRespository _examDao = new ExamRespository();
        private List<WordModel> _examWords = new List<WordModel>();
        private int _examWordTotal = 0;
        private int _failureCount;
        private List<long> _wordBookSelIds;

        public ExamResultViewModel(ExamType type, Dictionary<WordModel, FailureModel> failureWords, List<WordModel> examWords, List<long> wordBookSelIds)
        {
            _examWords = examWords;
            _examWordTotal = examWords.Count;
            _failureCount = failureWords.Count;
            FailureWord = failureWords;
            _wordBookSelIds = wordBookSelIds;

            _examDao.AddExamResult(type, _wordBookSelIds, _examWords, failureWords);

           
        }
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("TotalCountStr");
            OnPropertyChanged("FailureCountStr");
            OnPropertyChanged("CorrectRateStr");
            OnPropertyChanged("WordStr");
            OnPropertyChanged("CountStr");
            OnPropertyChanged("ExportStr");
            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent
        public string TotalCountStr
        {
            get
            {
                return string.Format(CommonUtil.GetResourceString("ExamTotalCount"), _examWordTotal); ;
            }
        }
        public string FailureCountStr
        {
            get
            {
                return string.Format(CommonUtil.GetResourceString("FailureCount"), _failureCount); ;
            }
        }

        public string CorrectRateStr
        {
            get
            {
                double correctRate = CommonUtil.GetCorrectRate(_examWordTotal, _failureCount);

                return string.Format(CommonUtil.GetResourceString("CorrectRate"), correctRate);
            }
        }


        public double CorrectRate
        {
            get
            {
                double correctRate = CommonUtil.GetCorrectRate(_examWordTotal, _failureCount);
                return Math.Round(correctRate, 0);
            }
        }

        public string ErrorWordsListStr
        {
            get
            {
                return CommonUtil.GetResourceString("ErrorWordsList"); ;
            }
        }
        public string WordStr
        {
            get
            {
                return CommonUtil.GetResourceString("Word"); ;
            }
        }
        public string CountStr
        {
            get
            {
                return CommonUtil.GetResourceString("Count"); ;
            }
        }

        public string PronunciationStr
        {
            get
            {
                return CommonUtil.GetResourceString("Pronunciation"); ;
            }
        }

        public string ExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("Explanation"); ;
            }
        }

        public string ExportStr
        {
            get
            {
                return CommonUtil.GetResourceString("Export"); ;
            }
        }

        public string SpeechStr
        {
            get
            {
                return CommonUtil.GetResourceString("Speech"); ;
            }
        }
        public string SentenceStr
        {
            get
            {
                return CommonUtil.GetResourceString("Sentence"); ;
            }
        }

        public string EnhanceStr
        {
            get
            {
                return CommonUtil.GetResourceString("Enhance"); ;
            }
        }



        public Dictionary<WordModel, FailureModel> FailureWord { get; set; }

        #endregion

        #region Command
        public ICommand Export
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SaveFileDialog openFileDialog = new SaveFileDialog();
                    openFileDialog.Filter = "Text files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                    if (string.IsNullOrEmpty(Parameter.GetInstance().ExportPath))
                    {
                        openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    }
                    string fileName = string.Empty;
                    List<WordBookModel> wordBooks;
                    WordBookRespository wordBookDao = new WordBookRespository();
                    wordBookDao.GetWordBooks(_wordBookSelIds, out wordBooks);
                    if (wordBooks != null)
                    {
                        foreach (WordBookModel wordBook in wordBooks)
                        {
                            fileName += wordBook.Name + "_";
                        }
                    }
                    fileName += "Exam Result_" + CorrectRate.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx";
                    openFileDialog.FileName = fileName;
                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        Parameter.GetInstance().ExportPath = openFileDialog.FileName.Replace(fileName, string.Empty);
                        CommonUtil.ShowProcessMainWindowShadow(string.Empty);
                        bool failure = false;
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            DataTable exportDt = new DataTable();
                            exportDt.Columns.Add("ID");
                            exportDt.Columns.Add("Enhance");
                            exportDt.Columns.Add("Count");
                            exportDt.Columns.Add("Word");
                            exportDt.Columns.Add("Pronunciation");
                            exportDt.Columns.Add("Speech");
                            exportDt.Columns.Add("Explanation");
                            exportDt.Columns.Add("Sentence");
                            exportDt.Columns.Add("SpeechO");
                            exportDt.Columns.Add("ExplanationO");
                            exportDt.Columns.Add("SentenceO");
                            foreach (KeyValuePair<WordModel, FailureModel> item in FailureWord)
                            {
                                DataRow dr = exportDt.NewRow();
                                dr["ID"] = item.Key.Id;
                                dr["Enhance"] = item.Value.ShowAnswer ? "★" : string.Empty;
                                dr["Count"] = item.Value.FailureCount;
                                dr["Word"] = item.Key.Word;
                                if (item.Key.Pronunciation.Count > 0)
                                {
                                    dr["Pronunciation"] = item.Key.Pronunciation[0].Pronunciation;
                                }
                                if (item.Key.Explanation.Count > 0)
                                {
                                    dr["Speech"] = CommonUtil.GetResourceString(item.Key.Explanation[0].Speech.ToString());
                                    dr["Explanation"] = item.Key.Explanation[0].Explanation;
                                    dr["Sentence"] = item.Key.Explanation[0].Sentence;
                                }
                                if (item.Key.Explanation.Count > 1)
                                {
                                    dr["SpeechO"] = CommonUtil.GetResourceString(item.Key.Explanation[1].Speech.ToString());
                                    dr["ExplanationO"] = item.Key.Explanation[1].Explanation;
                                    dr["SentenceO"] = item.Key.Explanation[1].Sentence;
                                }
                                exportDt.Rows.Add(dr);
                            }
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {
                                failure = !ExcelUtil.Export(new List<string> { "ID", EnhanceStr, CountStr, WordStr, PronunciationStr, SpeechStr, ExplanationStr, SentenceStr, SpeechStr, ExplanationStr, SentenceStr }, exportDt, openFileDialog.FileName);
                            });
                        };
                        backgroundWorker.RunWorkerAsync();
                        backgroundWorker.RunWorkerCompleted += (s, o) =>
                        {
                            CommonUtil.HighProcessMainWindowShadow();
                            if (failure)
                            {
                                CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgFailureStr("Export")));
                            }
                            else
                            {
                                CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr(CommonUtil.MsgSuccessStr("Export")));
                            }
                        };
                    }
                });
            }
        }
        #endregion

        #endregion

        #region Publish-Subscribe
        //PubSub
        #endregion
    }
}
