﻿using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BaBaoWord.ViewModel
{
    internal class DesktopChoiceExamWordViewModel : EditViewModelBase
    {
        ExamRespository _examDao = new ExamRespository();
        public DesktopChoiceExamWordViewModel(WordModel word, ExamSettingModel examSetting, int currentIndex, int total)
        {
            Word = word;
            Setting = examSetting;
            OpacityValue = Setting.Opacity;
            WordNum = currentIndex + " / " + total;
            PubSub<object>.AddEvent(Parameter.CloseExamWordEventKey, CloseExecute);

            _sound = new MediaElement();
            _sound.Volume = 1;
            _sound.LoadedBehavior = MediaState.Manual;
            _sound.MediaEnded += _sound_MediaEnded;
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("PlaySoundStr");
            OnPropertyChanged("RememberStr");
            OnPropertyChanged("EnhanceMemoryStr");
            OnPropertyChanged("SupportStr");
            OnPropertyChanged("AnswerStr");

            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent
        public double OpacityValue { get; set; }

        private double _windowLeft = 0;
        public double WindowLeft
        {
            get
            {
                return _windowLeft;
            }
            set
            {
                _windowLeft = value;
                OnPropertyChanged("WindowLeft");
            }
        }

        private double _windowTop = 0;
        public double WindowTop
        {
            get
            {
                return _windowTop;
            }
            set
            {
                _windowTop = value;
                OnPropertyChanged("WindowTop");
            }
        }
        private bool _show = false;
        public bool Show
        {
            get
            {
                return _show;
            }
            set
            {
                _show = value;
                OnPropertyChanged("Show");
            }
        }
        private bool _showWindow = true;
        public bool ShowWindow
        {
            get
            {
                return _showWindow;
            }
            set
            {
                _showWindow = value;
                OnPropertyChanged("ShowWindow");
            }
        }

        private Visibility _showDetail = Visibility.Hidden;
        public Visibility ShowDetail
        {
            get
            {
                return _showDetail;
            }
            set
            {
                _showDetail = value;
                OnPropertyChanged("ShowDetail");
            }
        }

        private Visibility _isShowAnswer = Visibility.Hidden;
        public Visibility IsShowAnswer
        {
            get
            {
                return _isShowAnswer;
            }
            set
            {
                _isShowAnswer = value;
                OnPropertyChanged("IsShowAnswer");
            }
        }
        public string WordNum { get; set; }

        private WordModel _word;
        public WordModel Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value;
                OnPropertyChanged("Word");
            }
        }
        public ExamSettingModel Setting { get; set; }

        public List<ExamChoiceModel> ExamChoiceItems { get; set; }
        private Uri _showWordSoundPath;
        public Uri ShowWordSoundPath
        {
            get
            {
                return _showWordSoundPath;
            }
            set
            {
                _showWordSoundPath = value;
                OnPropertyChanged("ShowWordSoundPath");
            }
        }
        private readonly MediaElement _sound;
        public MediaElement Sound
        {
            get { return _sound; }
        }

        private bool _answerOneEnable = true;
        public bool AnswerOneEnable
        {
            get
            {
                return _answerOneEnable;
            }
            set
            {
                _answerOneEnable = value;
                OnPropertyChanged("AnswerOneEnable");
            }
        }

        private bool _answerTwoEnable = true;
        public bool AnswerTwoEnable
        {
            get
            {
                return _answerTwoEnable;
            }
            set
            {
                _answerTwoEnable = value;
                OnPropertyChanged("AnswerTwoEnable");
            }
        }

        private bool _answerThreeEnable = true;
        public bool AnswerThreeEnable
        {
            get
            {
                return _answerThreeEnable;
            }
            set
            {
                _answerThreeEnable = value;
                OnPropertyChanged("AnswerThreeEnable");
            }
        }

        private bool _answerFourEnable = true;
        public bool AnswerFourEnable
        {
            get
            {
                return _answerFourEnable;
            }
            set
            {
                _answerFourEnable = value;
                OnPropertyChanged("AnswerFourEnable");
            }
        }


        private string _choiceAnswerStr = string.Empty;
        public string ChoiceAnswerStr
        {
            get
            {
                return _choiceAnswerStr;
            }
            set
            {
                _choiceAnswerStr = value;
                OnPropertyChanged("ChoiceAnswerStr");
            }
        }

        private string _questionTooltipStr = string.Empty;
        public string QuestionTooltipStr
        {
            get
            {
                return _questionTooltipStr;
            }
            set
            {
                _questionTooltipStr = value;
                OnPropertyChanged("QuestionTooltipStr");
            }
        }
        private string _questionStr = string.Empty;
        public string QuestionStr
        {
            get
            {
                return _questionStr;
            }
            set
            {
                _questionStr = value;
                OnPropertyChanged("QuestionStr");
            }
        }
        private string _answerOneStr = string.Empty;
        public string AnswerOneStr
        {
            get
            {
                return _answerOneStr;
            }
            set
            {
                _answerOneStr = value;
                OnPropertyChanged("AnswerOneStr");
            }
        }

        private string _answerTwoStr = string.Empty;

        public string AnswerTwoStr
        {
            get
            {
                return _answerTwoStr;
            }
            set
            {
                _answerTwoStr = value;
                OnPropertyChanged("AnswerTwoStr");
            }
        }
        private string _answerThreeStr = string.Empty;

        public string AnswerThreeStr
        {
            get
            {
                return _answerThreeStr;
            }
            set
            {
                _answerThreeStr = value;
                OnPropertyChanged("AnswerThreeStr");
            }
        }
        private string _answerFourStr = string.Empty;

        public string AnswerFourStr
        {
            get
            {
                return _answerFourStr;
            }
            set
            {
                _answerFourStr = value;
                OnPropertyChanged("AnswerFourStr");
            }
        }

        private string _answerOneTooltipStr = string.Empty;
        public string AnswerOneTooltipStr
        {
            get
            {
                return _answerOneTooltipStr;
            }
            set
            {
                _answerOneTooltipStr = value;
                OnPropertyChanged("AnswerOneTooltipStr");
            }
        }

        private string _answerTwoTooltipStr = string.Empty;

        public string AnswerTwoTooltipStr
        {
            get
            {
                return _answerTwoTooltipStr;
            }
            set
            {
                _answerTwoTooltipStr = value;
                OnPropertyChanged("AnswerTwoTooltipStr");
            }
        }
        private string _answerThreeTooltipStr = string.Empty;

        public string AnswerThreeTooltipStr
        {
            get
            {
                return _answerThreeTooltipStr;
            }
            set
            {
                _answerThreeTooltipStr = value;
                OnPropertyChanged("AnswerThreeTooltipStr");
            }
        }
        private string _answerFourTooltipStr = string.Empty;

        public string AnswerFourTooltipStr
        {
            get
            {
                return _answerFourTooltipStr;
            }
            set
            {
                _answerFourTooltipStr = value;
                OnPropertyChanged("AnswerFourTooltipStr");
            }
        }

        public string DetailStr
        {
            get
            {
                return CommonUtil.GetResourceString("Detail");
            }
        }
        public string PlaySoundStr
        {
            get
            {
                return CommonUtil.GetResourceString("PlaySound");
            }
        }

        public string RememberStr
        {
            get
            {
                return CommonUtil.GetResourceString("Remember");
            }
        }
        public string EnhanceMemoryStr
        {
            get
            {
                return CommonUtil.GetResourceString("EnhanceMemory");
            }
        }
        public string AnswerStr
        {
            get
            {
                return CommonUtil.GetResourceString("Answer");
            }
        }

        public string EditEnhanceImg
        {
            get
            {
                return Parameter.EditEnhanceImg;
            }
        }
        public string QuestionImg
        {
            get
            {
                return Parameter.QuestionImg;
            }
        }
        public string CancelImg
        {
            get
            {
                return Parameter.IconPath + "ic_action_cancel.png";
            }
        }

        public string SupportStr
        {
            get
            {
                return CommonUtil.GetResourceString("HotKey") + Environment.NewLine +
                    "1:A" + Environment.NewLine +
                    "2:B" + Environment.NewLine +
                    "3:C" + Environment.NewLine +
                    "4:D" + Environment.NewLine +
                    "F1:" + CommonUtil.GetResourceString("View") + Environment.NewLine +
                    "F4:" + CommonUtil.GetResourceString("Answer") + Environment.NewLine +
                    "F5:" + CommonUtil.GetResourceString("PlaySound") + Environment.NewLine +
                    "ESC:" + CommonUtil.GetResourceString("Close") + Environment.NewLine
                    ;
            }
        }
        private int FailureCount = 0;
        private string Answer = string.Empty;
        private string _firstWrongAnswer;
        #endregion

        #region Command
        public ICommand DesktopChoiceExamWord_Loaded
        {
            get
            {
                return new RelayCommand(param =>
                {
                    Rect workAreaRectangle = System.Windows.SystemParameters.WorkArea;
                    WindowLeft = workAreaRectangle.Right - 205 - 2;
                    WindowTop = workAreaRectangle.Bottom - 535 - 2;

                    if (Setting.ExamType == Category.ExamType.ChoiceExplan)
                    {
                        QuestionStr = Word.Word.Substring(0, 20, true);
                        QuestionTooltipStr = Word.Word;
                    }
                    else
                    {
                        QuestionStr = Word.Explanation[0].Explanation.Substring(0, 20, true);
                        QuestionTooltipStr = Word.Explanation[0].Explanation;
                    }

                    List<ExamChoiceModel> examChoices;
                    _examDao.GetChoiceItems(Word.Id.Value, out examChoices);
                    ExamChoiceItems = examChoices;
                    int i = 0;
                    string answerStr = string.Empty;
                    string answerTooltipStr = string.Empty;
                    foreach (ExamChoiceModel choice in examChoices)
                    {
                        if (choice.WId.Value.Equals(Word.Id.Value))
                        {
                            Answer = "A" + (i + 1);
                            ChoiceAnswerStr = ((char)(65 + i)).ToString();
                        }
                        if (Setting.ExamType == Category.ExamType.ChoiceExplan)
                        {
                            answerStr = choice.ShortExplanation;
                            answerTooltipStr = choice.Explanation;
                        }
                        else
                        {
                            answerStr = choice.ShortWord;
                            answerTooltipStr = choice.Word;
                        }

                        switch (i)
                        {
                            case 0:
                                AnswerOneStr = answerStr;
                                AnswerOneTooltipStr = answerTooltipStr;
                                break;
                            case 1:
                                AnswerTwoStr = answerStr;
                                AnswerTwoTooltipStr = answerTooltipStr;
                                break;
                            case 2:
                                AnswerThreeStr = answerStr;
                                AnswerThreeTooltipStr = answerTooltipStr;
                                break;
                            case 3:
                                AnswerFourStr = answerStr;
                                AnswerFourTooltipStr = answerTooltipStr;
                                break;

                        }
                        i++;
                    }

                    Show = true;
                    if (Setting.ExamType == Category.ExamType.ChoiceExplan)
                    {
                        BackgroundWorker backgroundWorker = new BackgroundWorker();
                        backgroundWorker.DoWork += (s, o) =>
                        {
                            System.Threading.Thread.Sleep(200);
                            if (Show)
                            {
                                PlaySoundExecute();
                            }
                        };
                        backgroundWorker.RunWorkerAsync();
                    }


                });
            }
        }

        public ICommand ForgetWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.IncreaseForget(Word.Id.Value);
                    Word.ShowForget = false;
                });
            }
        }


        public ICommand ReturnForget
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.ReturnForgetZero(Word.Id.Value);
                });
            }
        }

        public ICommand Close
        {
            get
            {
                return new RelayCommand(param =>
                {
                    Closing();

                });
            }
        }

        public ICommand View
        {
            get
            {
                return new RelayCommand(param =>
                {
                    WordRespository wordDao = new WordRespository();
                    WordModel tempWord = null;
                    if (wordDao.GetWord(Word.Id.Value, out tempWord))
                    {
                        tempWord.ShowForget = Word.ShowForget;
                        Word = tempWord;
                    }
                    if (ShowDetail == Visibility.Hidden)
                    {
                        ShowDetail = Visibility.Visible;
                    }
                    else if (ShowDetail == Visibility.Visible)
                    {
                        ShowDetail = Visibility.Hidden;
                    }
                });
            }
        }
        public ICommand CloseDetail
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowDetail = Visibility.Hidden;
                });
            }
        }

        public ICommand ShowAnswer
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.IncreaseForget(Word.Id.Value);
                    Word.ShowForget = false;
                    FailureCount++;
                    IsShowAnswer = Visibility.Visible;
                });
            }
        }

        public ICommand CheckAnswer
        {
            get
            {
                return new RelayCommand(param =>
                {
                    string answerNum = param.ToString();

                    if (Answer.Equals(answerNum))
                    {
                        Seccess();
                    }
                    else
                    {
                        Failure(answerNum);
                    }
                });
            }
        }

        private void Seccess()
        {
            AnswerOneEnable = false;
            AnswerTwoEnable = false;
            AnswerThreeEnable = false;
            AnswerFourEnable = false;
            Sound.Source = new Uri(Parameter.SuccessSound, UriKind.Relative);
            Sound.Play();

            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                System.Threading.Thread.Sleep(900);

                FailureModel failureModel = new FailureModel()
                {
                    FailureCount = FailureCount,
                    FirstErrorAnswer = _firstWrongAnswer,
                    ShowAnswer = IsShowAnswer == Visibility.Hidden ? false : true
                };

                PubSub<object>.RaiseEvent(Parameter.NextExamWordEventKey, this, new PubSubEventArgs<object>(failureModel));

            };
            backgroundWorker.RunWorkerAsync();

        }

        private void Failure(string answerNum)
        {
            ExamChoiceModel choice = ExamChoiceItems[int.Parse(answerNum.Replace("A", string.Empty)) - 1];
            if (FailureCount == 0)
            {
                BaseDao.IncreaseForget(Word.Id.Value);
                Word.ShowForget = false;
                if (Setting.ExamType == Category.ExamType.ChoiceExplan)
                {
                    _firstWrongAnswer = choice.Explanation;
                }
                else
                {
                    _firstWrongAnswer = choice.Word;
                }
            }
            FailureCount++;

            string tooltip = choice.Word + Environment.NewLine + choice.Explanation;
            switch (answerNum)
            {
                case "A1":
                    AnswerOneEnable = false;
                    AnswerOneTooltipStr = tooltip;
                    break;
                case "A2":
                    AnswerTwoEnable = false;
                    AnswerTwoTooltipStr = tooltip;
                    break;
                case "A3":
                    AnswerThreeEnable = false;
                    AnswerThreeTooltipStr = tooltip;
                    break;
                case "A4":
                    AnswerFourEnable = false;
                    AnswerFourTooltipStr = tooltip;
                    break;
            }

            Sound.Source = new Uri(Parameter.FailureSound, UriKind.Relative);
            Sound.Play();

        }
        private void Closing()
        {
            Show = false;
            ShowWordSoundPath = null;
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                System.Threading.Thread.Sleep(200);
                ShowWindow = false;
            };
            backgroundWorker.RunWorkerAsync();
        }

        private bool playEndFlag = true;
        public ICommand PlaySound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    PlaySoundExecute();
                });
            }
        }
        private void PlaySoundExecute()
        {
            if (!string.IsNullOrEmpty(Word.Sound))
            {
                string[] structure = Word.Sound.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if (structure.Length != 2)
                {
                    return;
                }
                string path = string.Empty;
                if (structure[1].Equals("Temp"))
                {
                    path = Parameter.GetInstance().Mp3TempDirectory;
                }
                else if (structure[1].Equals("Real"))
                {
                    path = Parameter.GetInstance().Mp3Directory;
                    CommonUtil.GetSound(Word.Sound.ToString());
                }
                else
                {
                    return;
                }
                string[] words = structure[0].Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.DoWork += (s, o) =>
                {
                    int i = 0;
                    while (i < words.Length)
                    {
                        if (playEndFlag)
                        {
                            if (i != 0)
                            {
                                //System.Threading.Thread.Sleep(50);
                            }
                            playEndFlag = false;
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {

                                Sound.Source = new Uri(System.IO.Path.Combine(path, words[i] + ".mp3"), UriKind.Absolute);
                                Sound.Play();
                            });
                            i++;
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                };
                backgroundWorker.RunWorkerAsync();

            }
        }

        void _sound_MediaEnded(object sender, RoutedEventArgs e)
        {
            playEndFlag = true;
            Sound.Source = null;
        }


        #endregion

        #endregion

        #region Publish-Subscribe
        private void CloseExecute(object sender, PubSubEventArgs<object> args)
        {
            Closing();
        }

        #endregion


    }
}
