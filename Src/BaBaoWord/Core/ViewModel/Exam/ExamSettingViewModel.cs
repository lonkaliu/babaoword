﻿using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using BaBaoWord.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Input;
using BaBaoWord.Category;

namespace BaBaoWord.ViewModel
{
    internal class ExamSettingViewModel : EditViewModelBase
    {
        private WordBookRespository _wordBookDao = new WordBookRespository();
        private WordReadRespository _wordReadDao = new WordReadRespository();
        private WordRespository _wordDao = new WordRespository();

        public ExamSettingViewModel()
        {
            if (WordBookSelIds.Count == 0)
            {
                ShowBookDialog();
            }
            PubSub<object>.AddEvent(Parameter.NextExamWordEventKey, NextWord);
        }


        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("ChangeWordBookStr");
            OnPropertyChanged("ExcelLoadStr");
            OnPropertyChanged("RandomStr");
            OnPropertyChanged("ExamSettingStr");
            OnPropertyChanged("OpacityStr");
            OnPropertyChanged("ShowAgainStr");
            OnPropertyChanged("DelayExplanationStr");
            OnPropertyChanged("RecognizeStr");
            OnPropertyChanged("SpellStr");
            OnPropertyChanged("ChoiceExplanationStr");
            OnPropertyChanged("ChoiceWordStr");

            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent

        public string ChangeWordBookStr
        {
            get
            {
                return CommonUtil.GetResourceString("ChangeWordBook");
            }
        }

        public string ExcelLoadStr
        {
            get
            {
                return CommonUtil.GetResourceString("ExcelLoad");
            }
        }


        public string RandomStr
        {
            get
            {
                return CommonUtil.GetResourceString("Random");
            }
        }

        public string ExamSettingStr
        {
            get
            {
                return CommonUtil.GetResourceString("ExamSetting");
            }
        }
        public string OpacityStr
        {
            get
            {
                return CommonUtil.GetResourceString("Opacity");
            }
        }

        public string ShowAgainStr
        {
            get
            {
                return CommonUtil.GetResourceString("ShowAgain");
            }
        }

        public string DelayExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("DelayExplanation");
            }
        }

        public string RecognizeStr
        {
            get
            {
                return CommonUtil.GetResourceString("Recognize");
            }
        }
        public string SpellStr
        {
            get
            {
                return CommonUtil.GetResourceString("Spell");
            }
        }
        public string ChoiceExplanationStr
        {
            get
            {
                return CommonUtil.GetResourceString("ChoiceExplanation");
            }
        }

        public string ChoiceWordStr
        {
            get
            {
                return CommonUtil.GetResourceString("ChoiceWord");
            }
        }
        private bool _isRecognizeStartExam = false;
        public bool IsRecognizeStartExam
        {
            get
            {
                return _isRecognizeStartExam;
            }
            set
            {
                _isRecognizeStartExam = value;
                OnPropertyChanged("IsRecognizeStartExam");
            }
        }

        private bool _isRecognizeEnable = true;
        public bool IsRecognizeEnable
        {
            get
            {
                return _isRecognizeEnable;
            }
            set
            {
                _isRecognizeEnable = value;
                OnPropertyChanged("IsRecognizeEnable");
            }
        }

        private bool _isSpellStartExam = false;
        public bool IsSpellStartExam
        {
            get
            {
                return _isSpellStartExam;
            }
            set
            {
                _isSpellStartExam = value;
                OnPropertyChanged("IsSpellStartExam");
            }
        }

        private bool _isSpellEnable = true;
        public bool IsSpellEnable
        {
            get
            {
                return _isSpellEnable;
            }
            set
            {
                _isSpellEnable = value;
                OnPropertyChanged("IsSpellEnable");
            }
        }

        private bool _isChoiceExplanStartExam = false;
        public bool IsChoiceExplanStartExam
        {
            get
            {
                return _isChoiceExplanStartExam;
            }
            set
            {
                _isChoiceExplanStartExam = value;
                OnPropertyChanged("IsChoiceExplanStartExam");
            }
        }

        private bool _isChoiceExplanEnable = true;
        public bool IsChoiceExplanEnable
        {
            get
            {
                return _isChoiceExplanEnable;
            }
            set
            {
                _isChoiceExplanEnable = value;
                OnPropertyChanged("IsChoiceExplanEnable");
            }
        }

        private bool _isChoiceWordStartExam = false;
        public bool IsChoiceWordStartExam
        {
            get
            {
                return _isChoiceWordStartExam;
            }
            set
            {
                _isChoiceWordStartExam = value;
                OnPropertyChanged("IsChoiceWordStartExam");
            }
        }

        private bool _isChoiceWordEnable = true;
        public bool IsChoiceWordEnable
        {
            get
            {
                return _isChoiceWordEnable;
            }
            set
            {
                _isChoiceWordEnable = value;
                OnPropertyChanged("IsChoiceWordEnable");
            }
        }

        public bool IsShowWord { get; set; }

        public bool AgainEnabled
        {
            get
            {
                return !IsShowWord && ExamWord != null;
            }
        }

        public bool AgainVisible
        {
            get
            {
                return IsRecognizeStartExam || IsSpellStartExam || IsChoiceExplanStartExam || IsChoiceWordStartExam;
            }
        }

        private ExamSettingModel _examSetting = new ExamSettingModel();
        public ExamSettingModel ExamSetting
        {
            get
            {
                return _examSetting;
            }
            set
            {
                _examSetting = value;
                OnPropertyChanged("ExamSetting");
            }
        }

        private List<WordModel> _examWords = new List<WordModel>();
        private List<int> _randomWordList = new List<int>();
        private Dictionary<WordModel, FailureModel> _failureWordCount = new Dictionary<WordModel, FailureModel>();
        private WordModel _examWord = null;
        public WordModel ExamWord
        {
            get
            {
                if (_examWords.Count > 0)
                {
                    if (_examSetting.Random)
                    {
                        _examWord = _examWords[_randomWordList[ExamWordIndex]];
                    }
                    else
                    {
                        _examWord = _examWords[ExamWordIndex];
                    }
                }
                return _examWord;
            }
            set
            {
                _examWord = value;
            }
        }

        public int ExamWordIndex { get; set; }


        private List<long> WordBookSelIds
        {
            get
            {
                List<long> ids = new List<long>();
                object value = CommonUtil.GetPageParam(Parameter.WordBookRootKey, "WordBookSelIds");
                if (value != null && value is List<long>)
                {
                    ids = (List<long>)value;
                    _wordBookDao.SetCheckIds(ids);
                }
                return ids;
            }
        }

        private string _excelFileName = string.Empty;
        private string _excelFilePath = string.Empty;
        public string ExcelFileName
        {
            get
            {
                return _excelFileName;
            }
            set
            {
                _excelFileName = value;
                OnPropertyChanged("ExcelFileName");
            }
        }

        private ExamType? _examType = null;

        #endregion

        #region Command
        public ICommand ChangeWordBook
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowBookDialog();
                });
            }
        }

        public ICommand ExcelLoad
        {
            get
            {
                return new RelayCommand(param =>
                {
                    _excelFilePath = string.Empty;
                    ExcelFileName = string.Empty;
                    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                    dlg.DefaultExt = ".xlsx";
                    dlg.Filter = "Excel(*.xlsx)|*.xlsx|Excel 97-2003(*.xls)|*.xls";
                    Nullable<bool> result = dlg.ShowDialog();
                    if (result == true)
                    {
                        _excelFilePath = dlg.FileName;
                        ExcelFileName = dlg.FileName.Substring(dlg.FileName.LastIndexOf("\\") + 1);
                    }
                });
            }
        }

        public ICommand StartExam
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (string.IsNullOrEmpty(_excelFilePath) && (WordBookSelIds == null || WordBookSelIds.Count == 0))
                    {
                        //CommonUtil.ShowMessageBoxWithShadow(CommonUtil.ParseMsgStr("PlzSelWordbookFirst"));
                        if (WordBookSelIds.Count == 0)
                        {
                            ShowBookDialog();
                            if (WordBookSelIds.Count == 0)
                            {
                                return;
                            }
                        }
                        return;
                    }
                    _examType = (ExamType)Enum.Parse(typeof(ExamType), param.ToString());
                    DoExam();

                });
            }
        }

        private void DoExam()
        {
            _examWords = new List<WordModel>();
            _failureWordCount = new Dictionary<WordModel, FailureModel>();
            _randomWordList = new List<int>();


            bool hasWord = false;
            string errorStr = string.Empty;
            if (string.IsNullOrEmpty(_excelFilePath))
            {
                errorStr = "CheckNoWord";
                if (_wordReadDao.GetWords(WordBookSelIds, out _examWords) && _examWords.Count > 0)
                {
                    hasWord = true;
                }
            }
            else
            {
                errorStr = "InvalidExcel";
                System.Data.DataTable value = new System.Data.DataTable();
                if (ExcelUtil.Import(_excelFilePath, out value))
                {
                    if (value.Columns.Contains("ID"))
                    {
                        if (_wordDao.GetWords(value.AsEnumerable().Select(item =>
                             {
                                 long result;
                                 if (!long.TryParse(item["ID"].ToString(), out result))
                                 {
                                     result = -1;
                                 }
                                 return result;
                             }
                            ).ToList(), string.Empty, out _examWords, true) && _examWords.Count > 0)
                        {
                            hasWord = true;
                        }

                    }

                }
            }

            if (hasWord)
            {
                SetExamStatus(true);
                OnPropertyChanged("AgainVisible");
                ExamWord = null;
                ExamWordIndex = 0;
                if (_examSetting.Random)
                {
                    Random random = new Random();
                    List<int> temp = Enumerable.Range(0, _examWords.Count)
                                .OrderBy(n => n * n * (new Random()).Next())
                                .ToList();
                    _randomWordList.AddRange(temp);
                }

                ShowExamWord();
            }
            else
            {
                CommonUtil.ShowMessageBoxWithShadow(CommonUtil.GetResourceString(errorStr));
            }

        }

        private void SetExamStatus(bool isStart)
        {
            switch (_examType)
            {
                case ExamType.Recognize:
                    IsRecognizeStartExam = isStart;
                    IsSpellEnable = !isStart;
                    IsChoiceExplanEnable = !isStart;
                    IsChoiceWordEnable = !isStart;
                    break;
                case ExamType.Spell:
                    IsSpellStartExam = isStart;
                    IsChoiceExplanEnable = !isStart;
                    IsChoiceWordEnable = !isStart;
                    IsRecognizeEnable = !isStart;
                    break;
                case ExamType.ChoiceExplan:
                    IsChoiceExplanStartExam = isStart;
                    IsChoiceWordEnable = !isStart;
                    IsSpellEnable = !isStart;
                    IsRecognizeEnable = !isStart;
                    break;
                case ExamType.ChoiceWord:
                    IsChoiceWordStartExam = isStart;
                    IsChoiceExplanEnable = !isStart;
                    IsSpellEnable = !isStart;
                    IsRecognizeEnable = !isStart;
                    break;
                default:
                    break;
            }
        }

        public ICommand ShowAgain
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (!IsShowWord && ExamWord != null)
                    {
                        ShowExamWord();
                    }
                });
            }
        }

        private void ShowExamWord()
        {
            IsShowWord = true;
            OnPropertyChanged("AgainEnabled");
            _examSetting.ExamType = _examType.Value;
            switch (_examType)
            {
                case ExamType.Recognize:
                case ExamType.Spell:
                    DesktopExamWord dew = new DesktopExamWord();
                    dew.Closed += dew_Closed;
                    DesktopExamWordViewModel dewVM = new DesktopExamWordViewModel(ExamWord, _examSetting, ExamWordIndex + 1, _examWords.Count);
                    dew.DataContext = dewVM;
                    dew.Show();
                    break;
                case ExamType.ChoiceExplan:
                case ExamType.ChoiceWord:
                    DesktopChoiceExamWord dcew = new DesktopChoiceExamWord();
                    dcew.Closed += dew_Closed;
                    DesktopChoiceExamWordViewModel dcewVM = new DesktopChoiceExamWordViewModel(ExamWord, _examSetting, ExamWordIndex + 1, _examWords.Count);
                    dcew.DataContext = dcewVM;
                    dcew.Show();
                    break;
            }

        }

        void dew_Closed(object sender, EventArgs e)
        {
            IsShowWord = false;
            OnPropertyChanged("AgainEnabled");
        }

        private void HideExamWord()
        {
            IsShowWord = false;
            OnPropertyChanged("AgainEnabled");
            PubSub<object>.RaiseEvent(Parameter.CloseExamWordEventKey, this, null);
        }

        public ICommand StopExam
        {
            get
            {
                return new RelayCommand(param =>
                {
                    SetExamStatus(false);
                    OnPropertyChanged("AgainVisible");
                    ExamWord = null;
                    ChangeToResult();
                    _examWords = new List<WordModel>();
                    HideExamWord();
                });
            }
        }



        private void ShowBookDialog()
        {
            CommonUtil.ShowMainWindowShadow();
            WordBookControl workBookControl = new WordBookControl();
            if (workBookControl.ShowDialog().Value)
            {
                List<long> ids;
                _wordBookDao.GetCheckIds(out ids);
                CommonUtil.SetPageParam(Parameter.WordBookRootKey, "WordBookSelIds", ids);
            }
            CommonUtil.HideMainWindowShadow();
        }


        #endregion

        #endregion

        #region Publish-Subscribe
        private void NextWord(object sender, PubSubEventArgs<object> args)
        {
            _wordReadDao.SaveReadWord(WordBookSelIds, ExamWord.Id.Value);

            if (args != null && args.Item != null)
            {
                FailureModel failureModel = (FailureModel)args.Item;
                if (failureModel.FailureCount > 0)
                {
                    _failureWordCount[ExamWord] = failureModel;
                }
            }
            if (ExamWordIndex == _examWords.Count - 1)
            {
                ExamWordIndex += 1;
                SetExamStatus(false);
                HideExamWord();
                ChangeToResult();
                ExamWord = null;
                _examWords = new List<WordModel>();
            }
            else
            {
                ExamWord = null;
                ExamWordIndex += 1;
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.DoWork += (s, o) =>
                {
                    IsShowWord = false;
                    OnPropertyChanged("AgainEnabled");
                    PubSub<object>.RaiseEvent(Parameter.CloseExamWordEventKey, this, null);
                    System.Threading.Thread.Sleep(500);
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        ShowExamWord();
                    });
                };
                backgroundWorker.RunWorkerAsync();
            }
        }

        private void ChangeToResult()
        {
            if (ExamWordIndex < _examWords.Count)
            {
                int removeIndex = ExamWordIndex;
                int examWordCount = _examWords.Count;
                int removeCount = examWordCount - ExamWordIndex;
                if (_examSetting.Random)
                {
                    _randomWordList.RemoveRange(removeIndex, removeCount);
                    for (int i = examWordCount - 1; i >= 0; i--)
                    {
                        if (!_randomWordList.Contains(i))
                        {
                            _examWords.RemoveAt(i);
                        }
                    }
                }
                else
                {
                    _examWords.RemoveRange(removeIndex, removeCount);
                }

            }
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                object param = new
                {
                    failureWords = _failureWordCount,
                    wordbookSelIds = WordBookSelIds,
                    words = _examWords,
                    examType = _examType.Value
                };
                App.Instance.ChangeContent(Parameter.ExamResultRootKey, param);
            });
        }
        #endregion
    }
}
