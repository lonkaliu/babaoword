﻿using BaBaoWord.DataAccess;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Globalization;

namespace BaBaoWord.ViewModel
{
    internal class DesktopExamWordViewModel : EditViewModelBase
    {
        public DesktopExamWordViewModel(WordModel word, ExamSettingModel examSetting, int currentIndex, int total)
        {
            Word = word;
            SetShortSentence();
            if (string.IsNullOrEmpty(Word.Explanation[0].ShortSentence))
            {
                Word.Explanation[0].ShortSentence = Word.Explanation[0].ShortExplanation;
            }
            Setting = examSetting;
            OpacityValue = Setting.Opacity;
            WordNum = currentIndex + " / " + total;
            PubSub<object>.AddEvent(Parameter.CloseExamWordEventKey, CloseExecute);
            _sound = new MediaElement();
            _sound.Volume = 1;
            _sound.LoadedBehavior = MediaState.Manual;
            _sound.MediaEnded += _sound_MediaEnded;
        }

        private void SetShortSentence()
        {
            string[] words = Word.Word.Split(' ');
            string explan = Word.Explanation[0].Sentence.Replace("\r\n", " lonkaRN ");
            string[] explans = explan.Split(' ');
            string[] explanBits = new string[explans.Length];
            foreach (string word in words)
            {
                string titleCaseWord = new System.Globalization.CultureInfo("en-US", false).TextInfo.ToTitleCase(word);
                for (int i = 0; i < explans.Length; i++)
                {
                    string explanSection = explans[i];
                    if ((explanSection.StartsWith(word) || explanSection.StartsWith(titleCaseWord))
                        && explanSection.Length - word.Length < 5)
                    {
                        explanBits[i] = word;
                    }
                }
            }
            Dictionary<int, string> replaceIndex = new Dictionary<int, string>();
            Dictionary<int, string> replaceTemp = new Dictionary<int, string>();
            for (int i = 0; i < explans.Length; i++)
            {
                if (string.IsNullOrEmpty(explanBits[i]) && replaceTemp.Count > words.Length - 1)
                {
                    foreach (var item in replaceTemp)
                    {
                        replaceIndex.Add(item.Key, item.Value);
                    }
                    replaceTemp = new Dictionary<int, string>();
                }
                else if (string.IsNullOrEmpty(explanBits[i]) && replaceTemp.Count <= words.Length - 1)
                {
                    replaceTemp = new Dictionary<int, string>();
                }
                else
                {
                    replaceTemp[i] = explanBits[i];
                }
            }
            if (replaceTemp.Count > 1)
            {
                foreach (var item in replaceTemp)
                {
                    replaceIndex.Add(item.Key, item.Value);
                }
            }
            foreach (var item in replaceIndex)
            {
                string word = item.Value;
                int wordLength = word.Length;
                string titleCaseWord = new System.Globalization.CultureInfo("en-US", false).TextInfo.ToTitleCase(word);
                explans[item.Key] = explans[item.Key].Replace(word, string.Empty.PadLeft(wordLength, '_')).Replace(new System.Globalization.CultureInfo("en-US", false).TextInfo.ToTitleCase(word), string.Empty.PadLeft(wordLength, '_'));
            }

            Word.Explanation[0].ShortSentence = string.Join(" ", explans).Replace(" lonkaRN ", "\r\n");
        }



        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("DetailStr");
            OnPropertyChanged("RememberStr");
            OnPropertyChanged("EnhanceMemoryStr");
            OnPropertyChanged("SubmitStr");
            OnPropertyChanged("AnswerStr");
            OnPropertyChanged("SupportStr");
            OnPropertyChanged("PlaySoundStr");

            base.LanguageChangedEvent(sender, e);
        }

        #region Binding

        #region DataContent
        public double OpacityValue { get; set; }

        private double _windowLeft = 0;
        public double WindowLeft
        {
            get
            {
                return _windowLeft;
            }
            set
            {
                _windowLeft = value;
                OnPropertyChanged("WindowLeft");
            }
        }

        private double _windowTop = 0;
        public double WindowTop
        {
            get
            {
                return _windowTop;
            }
            set
            {
                _windowTop = value;
                OnPropertyChanged("WindowTop");
            }
        }

        private double _wordInputWidth = 155;
        public double WordInputWidth
        {
            get
            {
                return _wordInputWidth;
            }
            set
            {
                _wordInputWidth = value;
                OnPropertyChanged("WordInputWidth");
            }
        }

        private double _wordOpacity = 0.5;
        public double WordOpacity
        {
            get
            {
                return _wordOpacity;
            }
            set
            {
                _wordOpacity = value;
                OnPropertyChanged("WordOpacity");
            }
        }
        private bool _show = false;
        public bool Show
        {
            get
            {
                return _show;
            }
            set
            {
                _show = value;
                OnPropertyChanged("Show");
            }
        }

        private bool _showWindow = true;
        public bool ShowWindow
        {
            get
            {
                return _showWindow;
            }
            set
            {
                _showWindow = value;
                OnPropertyChanged("ShowWindow");
            }
        }


        private Visibility _showDetail = Visibility.Hidden;
        public Visibility ShowDetail
        {
            get
            {
                return _showDetail;
            }
            set
            {
                _showDetail = value;
                OnPropertyChanged("ShowDetail");
            }
        }

        private Visibility _isShowAnswer = Visibility.Hidden;
        public Visibility IsShowAnswer
        {
            get
            {
                return _isShowAnswer;
            }
            set
            {
                _isShowAnswer = value;
                OnPropertyChanged("IsShowAnswer");
            }
        }

        private Visibility _isShowWord = Visibility.Hidden;
        public Visibility IsShowWord
        {
            get
            {
                return _isShowWord;
            }
            set
            {
                _isShowWord = value;
                OnPropertyChanged("IsShowWord");
            }
        }

        private bool _showSentence = false;
        public bool ShowSentence
        {
            get
            {
                return _showSentence;
            }
            set
            {
                _showSentence = value;
                OnPropertyChanged("ShowSentence");
            }
        }


        private string _answer = string.Empty;
        public string Answer
        {
            get
            {
                return _answer;
            }
            set
            {
                _answer = value;
                OnPropertyChanged("Answer");
            }
        }

        public string WordNum { get; set; }
        private WordModel _word;
        public WordModel Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value;
                OnPropertyChanged("Word");
            }
        }

        public ExamSettingModel Setting { get; set; }

        private Uri _showWordSoundPath;
        public Uri ShowWordSoundPath
        {
            get
            {
                return _showWordSoundPath;
            }
            set
            {
                _showWordSoundPath = value;
                OnPropertyChanged("ShowWordSoundPath");
            }
        }
        private readonly MediaElement _sound;
        public MediaElement Sound
        {
            get { return _sound; }
        }
        public string EnhanceMemoryStr
        {
            get
            {
                return CommonUtil.GetResourceString("EnhanceMemory");
            }
        }

        public string PlaySoundStr
        {
            get
            {
                return CommonUtil.GetResourceString("PlaySound");
            }
        }
        public string RememberStr
        {
            get
            {
                return CommonUtil.GetResourceString("Remember");
            }
        }

        public string DetailStr
        {
            get
            {
                return CommonUtil.GetResourceString("Detail");
            }
        }

        public string SubmitStr
        {
            get
            {
                return CommonUtil.GetResourceString("Submit");
            }
        }
        public string AnswerStr
        {
            get
            {
                return CommonUtil.GetResourceString("Answer");
            }
        }
        public string TipStr
        {
            get
            {
                return CommonUtil.GetResourceString("Tip");
            }
        }


        public string EditEnhanceImg
        {
            get
            {
                return Parameter.EditEnhanceImg;
            }
        }

        public string CancelImg
        {
            get
            {
                return Parameter.IconPath + "ic_action_cancel.png";
            }
        }

        public string SubmitImg
        {
            get
            {
                return Parameter.SubmitImg;
            }
        }
        public string QuestionImg
        {
            get
            {
                return Parameter.QuestionImg;
            }
        }
        public string TipImg
        {
            get
            {
                return Parameter.TipImg;
            }
        }

        public string SupportStr
        {
            get
            {
                return CommonUtil.GetResourceString("HotKey") + Environment.NewLine +
                    "F1:" + CommonUtil.GetResourceString("View") + Environment.NewLine +
                    "F3:" + CommonUtil.GetResourceString("Tip") + Environment.NewLine +
                    "F4:" + CommonUtil.GetResourceString("Answer") + Environment.NewLine +
                    "F5:" + CommonUtil.GetResourceString("PlaySound") + Environment.NewLine +
                    "ESC:" + CommonUtil.GetResourceString("Close") + Environment.NewLine +
                    "Enter:" + CommonUtil.GetResourceString("Submit")
                    ;
            }
        }
        private int FailureCount = 0;
        private string _firstWrongAnswer = string.Empty;
        #endregion

        #region Command
        public ICommand DesktopExamWord_Loaded
        {
            get
            {
                return new RelayCommand(param =>
                {
                    Rect workAreaRectangle = System.Windows.SystemParameters.WorkArea;
                    WindowLeft = workAreaRectangle.Right - 205 - 2;
                    WindowTop = workAreaRectangle.Bottom - 490 - 2;

                    Show = true;
                    BackgroundWorker backgroundWorker = new BackgroundWorker();
                    backgroundWorker.DoWork += (s, o) =>
                    {
                        System.Threading.Thread.Sleep(200);
                        if (Show)
                        {
                            PlaySoundExecute();
                        }
                    };
                    backgroundWorker.RunWorkerAsync();
                    ShowSentence = false;
                    if (Setting.ExamType == Category.ExamType.Spell)
                    {
                        if (Setting.DelayShowExplanation > 0)
                        {
                            BackgroundWorker explanationBackgroundWorker = new BackgroundWorker();
                            explanationBackgroundWorker.DoWork += (s, o) =>
                            {
                                System.Threading.Thread.Sleep(Setting.DelayShowExplanation * 1000);
                            };
                            explanationBackgroundWorker.RunWorkerAsync();
                            explanationBackgroundWorker.RunWorkerCompleted += (s, o) =>
                            {
                                ShowSentence = true;
                            };
                        }
                        else
                        {
                            ShowSentence = true;
                        }
                    }
                    else if(Setting.ExamType == Category.ExamType.Recognize)
                    {
                        WordInputWidth = 0;
                        WordOpacity = 1;
                        IsShowWord = Visibility.Visible;
                        Answer = Word.Word;
                    }


                });
            }
        }
        public ICommand Close
        {
            get
            {
                return new RelayCommand(param =>
                {
                    Closing();

                });
            }
        }

        private void Closing()
        {
            Show = false;
            ShowWordSoundPath = null;
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                System.Threading.Thread.Sleep(200);
                ShowWindow = false;
            };
            backgroundWorker.RunWorkerAsync();
        }

        private bool playEndFlag = true;
        public ICommand PlaySound
        {
            get
            {
                return new RelayCommand(param =>
                {
                    PlaySoundExecute();
                });
            }
        }

        private void PlaySoundExecute()
        {
            if (!string.IsNullOrEmpty(Word.Sound))
            {
                string[] structure = Word.Sound.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if (structure.Length != 2)
                {
                    return;
                }
                string path = string.Empty;
                if (structure[1].Equals("Temp"))
                {
                    path = Parameter.GetInstance().Mp3TempDirectory;
                }
                else if (structure[1].Equals("Real"))
                {
                    path = Parameter.GetInstance().Mp3Directory;
                    CommonUtil.GetSound(Word.Sound.ToString());
                }
                else
                {
                    return;
                }
                string[] words = structure[0].Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.DoWork += (s, o) =>
                {
                    int i = 0;
                    while (i < words.Length)
                    {
                        if (playEndFlag)
                        {
                            if (i != 0)
                            {
                                //System.Threading.Thread.Sleep(50);
                            }
                            playEndFlag = false;
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {

                                Sound.Source = new Uri(System.IO.Path.Combine(path, words[i] + ".mp3"), UriKind.Absolute);
                                Sound.Play();
                            });
                            i++;
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                };
                backgroundWorker.RunWorkerAsync();

            }
        }

        void _sound_MediaEnded(object sender, RoutedEventArgs e)
        {
            playEndFlag = true;
            Sound.Source = null;
        }

        public ICommand ForgetWord
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.IncreaseForget(Word.Id.Value);
                    Word.ShowForget = false;
                });
            }
        }

        public ICommand ReturnForget
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.ReturnForgetZero(Word.Id.Value);
                });
            }
        }

        public ICommand View
        {
            get
            {
                return new RelayCommand(param =>
                {
                    WordRespository wordDao = new WordRespository();
                    WordModel tempWord = null;
                    if (wordDao.GetWord(Word.Id.Value, out tempWord))
                    {
                        tempWord.ShowForget = Word.ShowForget;
                        Word = tempWord;
                    }
                    if (ShowDetail == Visibility.Hidden)
                    {
                        ShowDetail = Visibility.Visible;
                    }
                    else if (ShowDetail == Visibility.Visible)
                    {
                        ShowDetail = Visibility.Hidden;
                    }
                });
            }
        }

        public ICommand CloseDetail
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowDetail = Visibility.Hidden;
                });
            }
        }

        public ICommand CheckAnswer
        {
            get
            {
                return new RelayCommand(param =>
                {
                    if (param != null)
                    {
                        Answer = param.ToString();
                    }

                    if (Answer.ToLower().Trim().Equals(Word.Word))
                    {
                        Seccess();
                    }
                    else
                    {
                        Failure();
                    }

                });
            }
        }

        public ICommand ShowAnswer
        {
            get
            {
                return new RelayCommand(param =>
                {
                    BaseDao.IncreaseForget(Word.Id.Value);
                    Word.ShowForget = false;
                    FailureCount++;
                    IsShowAnswer = Visibility.Visible;
                    IsShowWord = Visibility.Visible;
                });
            }
        }

        public ICommand ShowTip
        {
            get
            {
                return new RelayCommand(param =>
                {
                    ShowSentence = true;
                });
            }
        }
        private void Seccess()
        {
            Sound.Source = new Uri(Parameter.SuccessSound, UriKind.Relative);
            Sound.Play();

            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += (s, o) =>
            {
                System.Threading.Thread.Sleep(900);

                FailureModel failureModel = new FailureModel()
                {
                    FailureCount = FailureCount,
                    FirstErrorAnswer = _firstWrongAnswer,
                    ShowAnswer = IsShowAnswer == Visibility.Hidden ? false : true
                };

                PubSub<object>.RaiseEvent(Parameter.NextExamWordEventKey, this, new PubSubEventArgs<object>(failureModel));

            };
            backgroundWorker.RunWorkerAsync();

        }

        private void Failure()
        {
            if (FailureCount == 0)
            {
                BaseDao.IncreaseForget(Word.Id.Value);
                Word.ShowForget = false;
                _firstWrongAnswer = Answer.ToLower();
            }
            FailureCount++;
            Answer = string.Empty;
            Sound.Source = new Uri(Parameter.FailureSound, UriKind.Relative);
            Sound.Play();

        }

        #endregion

        #endregion

        #region Publish-Subscribe
        private void CloseExecute(object sender, PubSubEventArgs<object> args)
        {
            Closing();
        }
        #endregion
    }
}
