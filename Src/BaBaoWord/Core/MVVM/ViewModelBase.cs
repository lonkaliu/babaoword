﻿using System;
using System.ComponentModel;

namespace BaBaoWord
{
    //https://www.youtube.com/watch?v=s-goeb2u3oA
    //https://www.youtube.com/watch?v=PUvZ5wVrZ4s
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        protected ViewModelBase()
        {
            if (null != App.Instance)
            {
                App.Instance.LanguageChangedEvent += LanguageChangedEvent;
            }
        }

        protected abstract void LanguageChangedEvent(object sender, EventArgs e);

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public void Dispose()
        {
            this.OnDispose();
        }

        protected virtual void OnDispose()
        {
            if (null != App.Instance)
            {
                App.Instance.LanguageChangedEvent -= LanguageChangedEvent;
            }
        }

    }
}
