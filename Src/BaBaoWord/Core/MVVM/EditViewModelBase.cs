﻿using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.MVVM
{
    internal class EditViewModelBase : ViewModelBase
    {
        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("SaveStr");
            OnPropertyChanged("StartStr");
            OnPropertyChanged("ClearStr");
            OnPropertyChanged("AddStr");
            OnPropertyChanged("EditStr");
            OnPropertyChanged("CancelStr");
            OnPropertyChanged("OkStr");
            OnPropertyChanged("RemoveStr");
            OnPropertyChanged("StopStr");
            OnPropertyChanged("SearchStr");
            OnPropertyChanged("SearchNotFoundStr");
            OnPropertyChanged("SettingStr");
            OnPropertyChanged("ViewStr");
            OnPropertyChanged("UpdateStr");
            OnPropertyChanged("CloseStr");
            OnPropertyChanged("ExpandStr");
            OnPropertyChanged("CollapseStr");
        }


        public string ExpandStr
        {
            get
            {
                return CommonUtil.GetResourceString("Expand");
            }
        }
        public string CollapseStr
        {
            get
            {
                return CommonUtil.GetResourceString("Collapse");
            }
        }
        public string SaveStr
        {
            get
            {
                return CommonUtil.GetResourceString("Save");
            }
        }
        public string ClearStr
        {
            get
            {
                return CommonUtil.GetResourceString("Clear");
            }
        }
        public string AddStr
        {
            get
            {
                return CommonUtil.GetResourceString("Add");
            }
        }
        public string EditStr
        {
            get
            {
                return CommonUtil.GetResourceString("Edit");
            }
        }
        public string CancelStr
        {
            get
            {
                return CommonUtil.GetResourceString("Cancel");
            }
        }
        public string OkStr
        {
            get
            {
                return CommonUtil.GetResourceString("OK");
            }
        }
        public string RemoveStr
        {
            get
            {
                return CommonUtil.GetResourceString("Remove");
            }
        }
        public string StopStr
        {
            get
            {
                return CommonUtil.GetResourceString("Stop");
            }
        }
        public string StartStr
        {
            get
            {
                return CommonUtil.GetResourceString("Start");
            }
        }

        public string ViewStr
        {
            get
            {
                return CommonUtil.GetResourceString("View");
            }
        }


        public string SearchStr
        {
            get
            {
                return CommonUtil.GetResourceString("Search");
            }
        }

        public string SearchNotFoundStr
        {
            get
            {
                return CommonUtil.GetResourceString("SearchNotFound");
            }
        }

        public string SettingStr
        {
            get
            {
                return CommonUtil.GetResourceString("Setting");
            }
        }

        public string ExitStr
        {
            get
            {
                return CommonUtil.GetResourceString("Exit");
            }
        }
        public string CloseStr
        {
            get
            {
                return CommonUtil.GetResourceString("Close");
            }
        }

        public string UpdateStr
        {
            get
            {
                return CommonUtil.GetResourceString("Update");
            }
        }

        public string EditAddImg
        {
            get
            {
                return Parameter.EditAddImg;
            }
        }

        public string EditOkImg
        {
            get
            {
                return Parameter.EditOkImg;
            }
        }
        public string EditCancelImg
        {
            get
            {
                return Parameter.EditCancelImg;
            }
        }
        public string EditClearImg
        {
            get
            {
                return Parameter.EditClearImg;
            }
        }
        public string EditSaveImg
        {
            get
            {
                return Parameter.EditSaveImg;
            }
        }
        public string EditEditImg
        {
            get
            {
                return Parameter.EditEditImg;
            }
        }
        public string EditRemoveImg
        {
            get
            {
                return Parameter.EditRemoveImg;
            }
        }
        public string EditStopImg
        {
            get
            {
                return Parameter.EditStopImg;
            }
        }
        public string EditStartImg
        {
            get
            {
                return Parameter.EditStartImg;
            }
        }
        public string EditPauseImg
        {
            get
            {
                return Parameter.EditPauseImg;
            }
        }
        public string EditStop2Img
        {
            get
            {
                return Parameter.EditStop2Img;
            }
        }

        public string EditSearchImg
        {
            get
            {
                return Parameter.EditSearchImg;
            }
        }
        public string EditSearch2Img
        {
            get
            {
                return Parameter.EditSearch2Img;
            }
        }
        public string EditSettingImg
        {
            get
            {
                return Parameter.EditSettingImg;
            }
        }
        public string EditTrashImg
        {
            get
            {
                return Parameter.EditTrashImg;
            }
        }

        public string EditRefreshImg
        {
            get
            {
                return Parameter.EditRefreshImg;
            }
        }

        public string EditQuestion2Img
        {
            get
            {
                return Parameter.Question2Img;
            }
        }
        public string ExpandImg
        {
            get
            {
                return Parameter.ExpandImg;
            }
        }
        public string CollapseImg
        {
            get
            {
                return Parameter.CollapseImg;
            }
        }

        public string SoundImg
        {
            get
            {
                return Parameter.SoundImg;
            }
        }
        public string BookImg
        {
            get
            {
                return Parameter.BookImg;
            }
        }
        public string ExcelImg
        {
            get
            {
                return Parameter.ExcelImg;
            }
        }
        public string ExcelDownloadImg
        {
            get
            {
                return Parameter.ExcelDownloadImg;
            }
        }
    }
}
