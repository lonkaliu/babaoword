﻿using BaBaoWord.Model;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.MVVM
{
    internal class MessageViewModelBase : EditViewModelBase
    {
        public string CancelImg
        {
            get
            {
                return Parameter.IconPath + "ic_action_cancel.png";
            }
        }

        protected override void LanguageChangedEvent(object sender, EventArgs e)
        {
            OnPropertyChanged("Title");
            base.LanguageChangedEvent(sender, e);
        }

        private string _title = string.Empty;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }


        protected ConsoleModel _consoleModel = new Model.ConsoleModel();
        public ConsoleModel ConsoleModel
        {
            get
            {
                return _consoleModel;
            }
            set
            {
                _consoleModel = value;
                OnPropertyChanged("ConsoleModel");
            }
        }

    }
}
