﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Model;
using BaBaoWord.Util;
using System.Reflection;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace BaBaoWord.DataAccess
{
    internal class SettingRespository
    {
        public bool SaveRotateSetting(RotateSettingModel rotateSettingModel)
        {
            string category = "Rotate";
            List<SettingModel> settings = new List<SettingModel>();
            foreach (PropertyInfo property in rotateSettingModel.GetType().GetProperties())
            {
                object value = property.GetValue(rotateSettingModel, null);

                settings.Add(new SettingModel()
                {
                    Category = category,
                    Key = property.Name,
                    Value = value == null ? null : value.ToString()
                });
            }
            return BaseDao.SaveSetting(settings);
        }

        public bool SaveModelSetting<T>(string category, T settingModel)
        {
            List<SettingModel> settings = new List<SettingModel>();
            foreach (PropertyInfo property in settingModel.GetType().GetProperties())
            {
                object value = property.GetValue(settingModel, null);
                if (value.GetType() == typeof(ObservableCollection<WordFilterModel>))
                {
                    value = value.Serialize<WordFilterModel>();
                }

                settings.Add(new SettingModel()
                {
                    Category = category,
                    Key = property.Name,
                    Value = value == null ? null : value.ToString()
                });
            }
            return BaseDao.SaveSetting(settings);
        }

        public bool SaveWordbookSetting(WordBookSettingModel settingModel)
        {
            string category = "WordBook";

            List<SettingModel> settings = new List<SettingModel>();
            foreach (PropertyInfo property in settingModel.GetType().GetProperties())
            {
                object value = property.GetValue(settingModel, null);
                if (value.GetType() == typeof(ObservableCollection<WordFilterModel>))
                {
                    value = value.Serialize<WordFilterModel>();
                }

                settings.Add(new SettingModel()
                {
                    Category = category,
                    Key = property.Name,
                    Value = value == null ? null : value.ToString()
                });
            }

            return BaseDao.SaveSetting(settings);
        }

        public bool SaveSetting(List<SettingModel> settings)
        {
            return BaseDao.SaveSetting(settings);
        }

        public bool GetSetting(string category, string key, out List<SettingModel> settings)
        {
            return BaseDao.GetSetting(category, key, out settings);
        }

        public bool GetRatateSetting(out RotateSettingModel rotateSettingModel)
        {
            rotateSettingModel = new RotateSettingModel();
            bool result = false;
            List<SettingModel> settings;
            if (BaseDao.GetSetting("Rotate", null, out settings))
            {
                foreach (SettingModel settingModel in settings)
                {
                    PropertyInfo property = rotateSettingModel.GetType().GetProperty(settingModel.Key);
                    var converter = TypeDescriptor.GetConverter(property.PropertyType);
                    property.SetValue(rotateSettingModel, converter.ConvertFrom(settingModel.Value), null);
                }
                result = true;
            }
            return result;
        }


        public bool GetWordbookSetting(out WordBookSettingModel wordBookSettingModel)
        {
            wordBookSettingModel = new WordBookSettingModel();
            bool result = false;
            List<SettingModel> settings;
            if (BaseDao.GetSetting("WordBook", null, out settings))
            {
                foreach (SettingModel settingModel in settings)
                {
                    PropertyInfo property = wordBookSettingModel.GetType().GetProperty(settingModel.Key);
                    if (settingModel.Key.Equals("WordFilters"))
                    {
                        System.Collections.ObjectModel.ObservableCollection<WordFilterModel> temp = new ObservableCollection<Model.WordFilterModel>();
                        property.SetValue(wordBookSettingModel, temp.Deserialize<WordFilterModel>(settingModel.Value), null);
                    }
                    else
                    {
                        var converter = TypeDescriptor.GetConverter(property.PropertyType);
                        property.SetValue(wordBookSettingModel, converter.ConvertFrom(settingModel.Value), null);
                    }
                }
                result = true;
            }
            return result;
        }
    }
}
