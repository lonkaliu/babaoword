﻿using BaBaoWord.Model;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.DataAccess
{
    internal class WordReadRespository
    {
        public bool GetWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return BaseDao.GetWords(wordbookIds, out words);
        }

        public bool GetExportWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return BaseDao.GetExportWords(wordbookIds, out words);
        }

        public bool GetWords(List<long> wordbookIds, out int count)
        {
            return BaseDao.GetWord(wordbookIds, out count);
        }
        public bool GetUnReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return BaseDao.GetUnReadWords(wordbookIds, out words);
        }
        public bool GetReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return BaseDao.GetReadWords(wordbookIds, out words);
        }

        public bool SaveReadWord(List<long> wordbookIds, long wId)
        {
            return BaseDao.SaveReadWord(wordbookIds, wId);
        }

        public bool ReturnAllUnRead(List<long> wordbookIds)
        {
            return BaseDao.ReturnAllUnRead(wordbookIds);
        }

    }
}
