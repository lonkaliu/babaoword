﻿using BaBaoWord.Category;
using BaBaoWord.Model;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.DataAccess
{
    internal class ExamRespository
    {
        public bool GetChoiceItems(long id, out List<ExamChoiceModel> examChoices)
        {
            return BaseDao.GetChoiceItems(id, out examChoices);
        }

        public bool AddExamResult(ExamType type, List<long> wordBookSelIds, List<WordModel> examWords, Dictionary<WordModel, FailureModel> failureWords)
        {
            return BaseDao.AddExamResult(type, wordBookSelIds, examWords, failureWords);
        }
    }
}
