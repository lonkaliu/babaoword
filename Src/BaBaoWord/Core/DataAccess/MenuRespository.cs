﻿using System.Collections.Generic;
using BaBaoWord.Model;
using BaBaoWord.Util;

namespace BaBaoWord.DataAccess
{
    internal class MenuRespository
    {
        private List<MenuModel> _menuList;
        public MenuRespository()
        {
            if (_menuList == null)
            {
                _menuList = new List<MenuModel>();
            }
            _menuList.Add(new MenuModel(0, CommonUtil.GetResourceString("Home"), Parameter.HomeRootKey));
            if (Parameter.GetInstance().UserInfo == null)
            {
                _menuList.Add(new MenuModel(1, CommonUtil.GetResourceString("Login"), Parameter.LoginRootKey));
            }
            else
            {
                _menuList.Add(new MenuModel(1, CommonUtil.GetResourceString("UserProfile"), Parameter.UserProfileRootKey));
            }
            _menuList.Add(new MenuModel(2, CommonUtil.GetResourceString("SyncWord"), "SyncWord")
            {
                IsEnabled = OnlyForAccount,
                Foreground = OnlyForAccountForeground
            });
            _menuList.Add(new MenuModel(101, "中文", "zh-TW"));
            _menuList.Add(new MenuModel(102, "English", "en-US"));
            _menuList.Add(new MenuModel(98, CommonUtil.GetResourceString("About"), Parameter.AboutRootKey));
            if (Parameter.GetInstance().UserInfo != null)
            {
                _menuList.Add(new MenuModel(100, CommonUtil.GetResourceString("Logout"), "Logout"));
            }
            _menuList.Add(new MenuModel(99, CommonUtil.GetResourceString("Back"), "Back"));

        }
        public bool OnlyForAccount
        {
            get
            {
                if (Parameter.GetInstance().UserInfo == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public string OnlyForAccountForeground
        {
            get
            {
                if (Parameter.GetInstance().UserInfo == null)
                {
                    return "#FFCBCBCB";
                }
                else
                {
                    return "#FF333333";
                }
            }
        }
        public List<MenuModel> GetMenuList()
        {
            return _menuList;
        }
    }
}
