﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Model;
using BaBaoWord.Util;

namespace BaBaoWord.DataAccess
{
    internal class WordBookRespository
    {
        private static List<WordBookModel> _wordBooks;
        private static List<long> _checkIds;

        public WordBookRespository()
        {
            if (_wordBooks == null)
            {
                BaseDao.GetWordBooks(null, out _wordBooks);
            }
            if (_checkIds == null)
            {
                _checkIds = new List<long>();
            }
        }

        public List<WordBookModel> GetDbItems()
        {
            //如果要等到commit才進db的話，這時後不能夠直接取出來丟進static
            //，要用另一個物件接，而且在PreOk or PreCancel的事件中要把static物件清空，這樣才可以判斷要不要去db取值
            if (BaseDao.GetWordBooks(null, out _wordBooks))
            {
                foreach (WordBookModel wordBook in _wordBooks)
                {
                    if (_checkIds.Contains(wordBook.Id.Value))
                    {
                        wordBook.IsCheck = true;
                    }
                    else
                    {
                        wordBook.IsCheck = false;
                    }
                }
            }
            return _wordBooks;
        }
        public List<WordBookModel> GetItems()
        {
            return _wordBooks;
        }


        public bool RemoveItem(WordBookModel wordBook)
        {
            bool result = false;
            try
            {
                if (BaseDao.RemoveWordBook(wordBook.Id.Value))
                {
                    Commit();
                    //_wordBooks.Remove(wordBook);
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool EditItem(WordBookModel wordBook)
        {
            bool result = false;
            try
            {
                //var originalWordBookIndex = _wordBooks.FindIndex(item => item.Id.Equals(wordBook.Id));
                //if (originalWordBookIndex != -1)
                //{
                //    _wordBooks[originalWordBookIndex] = wordBook;
                //    result = true;
                //}
                if (BaseDao.EditWordBook(wordBook))
                {
                    Commit();
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool AddItem(WordBookModel wordBook)
        {
            bool result = false;
            try
            {
                long id;
                if (BaseDao.AddWordBook(wordBook, out id))
                {
                    Commit();
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public void Commit()
        {
            //如果要最後點擊ok後才更變的話就要寫在這進db，並且在add、edit、remove時記錄下所有變動
            _checkIds = new List<long>();
            foreach (WordBookModel wordBook in _wordBooks)
            {
                if (wordBook.IsCheck)
                {
                    _checkIds.Add(wordBook.Id.Value);
                }
            }
            GetDbItems();
        }

        public bool GetCheckIds(out List<long> Ids)
        {
            Ids = _checkIds;
            return true;
        }

        public void SetCheckIds(List<long> Ids)
        {
            _checkIds = Ids;
            GetDbItems();
        }

        public bool GetWordBookIds(long wordId, out List<long> wordBookIds)
        {
            return BaseDao.GetWordBookIds(wordId, out wordBookIds);
        }

        public bool GetWordBooks(string word, out List<SearchWordInWordBookModel> wordBook)
        {
            return BaseDao.GetWordBooks(word, out wordBook);
        }

        public bool ReloadWordBooks()
        {
            _checkIds = new List<long>();
            return BaseDao.GetWordBooks(null, out _wordBooks);
        }

        public bool GetWordBooks(List<long> wordBookIds,out List<WordBookModel> wordBooks)
        {
            return BaseDao.GetWordBooks(wordBookIds, out wordBooks);
        }

    }
}
