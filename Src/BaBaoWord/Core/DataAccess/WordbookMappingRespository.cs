﻿using BaBaoWord.Model;
using BaBaoWord.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.DataAccess
{
    internal class WordBookMappingRespository
    {
        public bool GetWordbookMapping(long? wordbookId, long? wordId, out List<WordBookMapping> wordbookMapping)
        {
            return BaseDao.GetWordbookMapping(wordbookId, wordId, out wordbookMapping);
        }

        public bool RemoveWordbookMapping(long? wordbookId, long? wordId)
        {
            return BaseDao.RemoveWordbookMapping(wordbookId, wordId);
        }
    }
}
