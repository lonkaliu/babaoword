﻿using BaBaoWord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaBaoWord.Util;
using System.Data;

namespace BaBaoWord.DataAccess
{
    internal class WordRespository
    {
        public bool Commit(WordModel word, List<long> wordBookSelId)
        {
            bool result = false;
            if (word.Id == null)
            {
                long id;
                result = BaseDao.AddWord(word, wordBookSelId,out id);
            }
            else
            {
                result = BaseDao.EditWord(word, wordBookSelId);
            }
            return result;
        }

        //public bool GetWord(List<long> Id,out List<WordModel> words)
        //{

        //}

        public bool GetWord(long id, out WordModel resultWord)
        {
            return BaseDao.GetWord(id, out resultWord);
        }

        public bool GetWords(List<long> ids, string word, out List<WordModel> resultWords, bool onlyWord = false,bool isAll = true)
        {
            return BaseDao.GetWords(ids, word, out resultWords, onlyWord, isAll);
        }

        public bool CheckDuplicate(string word, List<long> wordBookSelId, out DataTable duplicateData)
        {
            bool result = false;
            if (BaseDao.CheckWordDuplicate(word, wordBookSelId, out duplicateData))
            {
                if (duplicateData != null && duplicateData.Rows.Count > 0)
                {
                    return true;
                }
            }
            return result;
        }

        public bool Remove(long Id)
        {
            return BaseDao.RemoveWord(Id);
        }

    }
}
