﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using BaBaoWord.Model;
using BaBaoWord.Category;

namespace BaBaoWord.Converter
{
    internal class SpeechSelectedToValueConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value is SpeechModel)
            {
                return ((SpeechModel)value).Value;
            }
            return null;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                return new SpeechModel((SpeechType)Enum.Parse(typeof(SpeechType), value.ToString()));
            }
            else
            {
                return new SpeechModel();
            }
        }
    }
}
