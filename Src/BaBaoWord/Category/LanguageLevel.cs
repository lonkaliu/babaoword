﻿namespace BaBaoWord.Category
{
    enum LanguageLevel
    {
        Beginner,
        Elementary,
        PreIntermediate,
        Intermediate,
        UpperIntermediate,
        Advanced,
        Proficiency
    }
}
