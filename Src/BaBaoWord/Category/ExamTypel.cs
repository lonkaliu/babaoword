﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Category
{
    enum ExamType
    {
        Spell,
        Recognize,
        ChoiceExplan,
        ChoiceWord
    }
}
