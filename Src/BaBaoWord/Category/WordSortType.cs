﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Category
{
    public enum WordSortType
    {
        ByWeight,
        ByWrongRate,
        ByForgetCount,
        ByImportance,
        ByWord,
        ById
    }
}
