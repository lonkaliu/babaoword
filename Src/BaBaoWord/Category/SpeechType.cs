﻿
namespace BaBaoWord.Category
{
    //字多的排在上面
    /// <summary>
    /// 詞性
    /// </summary>
    enum SpeechType
    {
        _empty,
        _abbr,//  abbreviation (abbr.)n. 縮寫
        _conj,//  conjunction (conj.) 連接詞
        _prep,//  preposition (prep.) 介繫詞
        _pron,//  pronoun (pron.) 代名詞
        _art,//  article (art.) 冠詞
        _aux,//  auxiliary verb (aux.) 助動詞
        _int,//  interjection (int.) 感嘆詞
        _spe,//  spelling (spe.) 拼字
        _ad,//  adverb (ad.) 副詞
        _hp,//  homophonic(hp.) 諧音
        _vt,//  transitive verb (vt.) 及物動詞
        _vi,//  intransitive verb (vi.) 不及物動詞
        _pl,//  plural (pl.) 複數
        _pp,//  past perfect (pp.) 過去完成式
        _pt,//  past tense 過去式
        _ph,//  phrase (phr.) 片語
        _a,//  adjective (a.) 形容詞
        _c,//  countable noun (c.) 可數名詞
        _n,//  noun (n.) 名詞
        _s,//  subject (s.) 主詞
        _u,//  uncountable noun (u.) 不可數名詞
        _v,//  verb (v.) 動詞
        
    }
}
