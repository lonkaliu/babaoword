﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using BaBaoWord.Model;
using System.IO;

namespace BaBaoWord.Util
{
    internal class WebServiceDao
    {
        //https://social.msdn.microsoft.com/Forums/zh-TW/8d14e335-b094-414e-abeb-851ef635ffe8/winformclasslibrarywcf?forum=802
        private static readonly object ms_lockObject = new object();

        #region Base
        private volatile static BaseService.IBaBaoWord _daoFactory;

        public static BaseService.IBaBaoWord DaoFactory
        {
            get
            {
                lock (ms_lockObject)
                {
                    if (_daoFactory == null)
                    {
                        //WSHttpBinding wsHttpBinding = new WSHttpBinding();
                        //wsHttpBinding.MessageEncoding = WSMessageEncoding.Text;
                        //wsHttpBinding.TextEncoding = Encoding.UTF8;
                        //wsHttpBinding.AllowCookies = true;
                        //EndpointAddress myEndpoint = new EndpointAddress(new Uri("http://lonkaliu.somee.com/service/BaBaoWordFileService.svc"));
                        //ChannelFactory<BaseService.IBaBaoWord> myChannelFactory = new ChannelFactory<BaseService.IBaBaoWord>(wsHttpBinding, myEndpoint);
                        //wsHttpBinding.UseDefaultWebProxy = false ;
                        //_daoFactory = myChannelFactory.CreateChannel();
                        _daoFactory = new BaseService.BaBaoWordClient();
                    }
                }
                return _daoFactory;
            }
            set
            {
                _daoFactory = value;
            }
        }

        public static BaseService.ResultData Login(UserInfoModel userInfo)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                result = DaoFactory.Login(new BaseService.UserInfo() { Email = userInfo.Email, Password = userInfo.Password });
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.ResultData SendVerificationCode(string email, string code)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                result = DaoFactory.SendValidationCode(email, code);
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.ResultData NewAccount(UserInfoModel userInfo)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                List<BaseService.LanguageLevel> lang = new List<BaseService.LanguageLevel>();
                foreach (UserLanguageModel item in userInfo.Language)
                {
                    lang.Add(new BaseService.LanguageLevel() { Name = item.Language_Name, Level = (int)item.Language_Level });
                }
                result = DaoFactory.NewAccount(new BaseService.UserInfo() { Email = userInfo.Email, Password = userInfo.Password, Name = userInfo.Name, LanguageLevel = lang.ToArray() });
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.ResultData EditAccount(UserInfoModel userInfo)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                List<BaseService.LanguageLevel> lang = new List<BaseService.LanguageLevel>();
                foreach (UserLanguageModel item in userInfo.Language)
                {
                    lang.Add(new BaseService.LanguageLevel() { Name = item.Language_Name, Level = (int)item.Language_Level });
                }
                result = DaoFactory.EditAccount(new BaseService.UserInfo() { Password = userInfo.Password, Name = userInfo.Name, LanguageLevel = lang.ToArray(), ID = userInfo.Id.Value });
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.ResultData ChangePassword(string email, string password)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                result = DaoFactory.ChangePassword(email, password);
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.LanguageLevel[] GetLanguage(long id)
        {
            return DaoFactory.GetLanguage(id);
        }

        public static BaseService.ResultData CheckAccount(string email)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                result = DaoFactory.CheckAccount(email);
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.ResultData GetVersion(string token, long id)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                result = DaoFactory.GetVersion(token, id);
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.ResultData SetVersion(string token, long id, int version)
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                result = DaoFactory.SetVersion(token, id, version);
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        public static BaseService.ResultData GetBaBaoWordVersion()
        {
            BaseService.ResultData result = new BaseService.ResultData();
            try
            {
                result = DaoFactory.GetBaBaoWordVersion();
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
                result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
            }
            return result;
        }

        #endregion

        #region File

        private volatile static FileService.IBaBaoWordFile _daoFileFactory;

        public static FileService.IBaBaoWordFile DaoFileFactory
        {
            get
            {
                lock (ms_lockObject)
                {
                    if (_daoFileFactory == null)
                    {
                        _daoFileFactory = new FileService.BaBaoWordFileClient();
                    }
                }
                return _daoFileFactory;
            }
            set
            {
                _daoFileFactory = value;
            }
        }

        public static FileService.ResultMessage UploadFile(string token, string userGuid)
        {
            FileService.ResultMessage result = new FileService.ResultMessage();
            try
            {
                string filePath = Path.Combine(App.Directory, "data.sqlite");
                string copyFilePath = Path.Combine(App.Directory, "data_bk.sqlite");
                File.Copy(filePath, copyFilePath,true);

                FileService.UploadFileData uploadFileData = new FileService.UploadFileData();
                using (FileStream stream = new FileStream(copyFilePath, FileMode.Open, FileAccess.Read))
                {
                    uploadFileData.Token = token;
                    uploadFileData.UserGuid = userGuid;
                    uploadFileData.FileByte = stream;
                    try
                    {
                        result = DaoFileFactory.UploadUserData(uploadFileData);
                    }
                    catch (Exception e)
                    {
                        Log.WriteLog(LogLevel.Error, e);
                        result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
                    }
                }
                //File.Delete(copyFilePath);
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public static FileService.DownloadResultMessage DownloadFile(string token, string userGuid)
        {
            FileService.DownloadResultMessage result = new FileService.DownloadResultMessage();
            try
            {
                FileService.DownloadFileData data = new FileService.DownloadFileData()
                {
                    Token = token,
                    UserGuid = userGuid
                };
                try
                {
                    result = DaoFileFactory.DownloadUserData(data);
                }
                catch (Exception e)
                {
                    Log.WriteLog(LogLevel.Error, e);
                    result.Exception = CommonUtil.GetResourceString("UnableConnectServer");
                }
                if (result.Result)
                {
                    string filePath = Path.Combine(App.Directory, "data.sqlite");
                    string fileBkPath = Path.Combine(App.Directory, "data_bk.sqlite");
                    File.Copy(filePath, fileBkPath, true);
                    File.Delete(filePath);
                    using (FileStream targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        const int bufferLen = 65000;
                        byte[] buffer = new byte[bufferLen];
                        int count = 0;
                        while ((count = result.FileByte.Read(buffer, 0, bufferLen)) > 0)
                        {
                            targetStream.Write(buffer, 0, count);
                        }
                        targetStream.Close();
                        targetStream.Close();
                    }
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }
        #endregion
    }
}
