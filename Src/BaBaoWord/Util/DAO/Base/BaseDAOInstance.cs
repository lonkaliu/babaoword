﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BaBaoWord.Model;
using BaBaoWord.Category;

namespace BaBaoWord.Util
{
    internal class BaseDaoInstance
    {
        private IApi _daoInstance = null;
        public BaseDaoInstance(string dataSource)
        {
#if DEBUG
            _daoInstance = new SQLiteApi(string.Format("Data Source={0};Version=3;", CommonUtil.GetFilePath(dataSource)));
#else
            _daoInstance = new SQLiteApi(string.Format("Data Source={0};Version=3;Password=!QAZ2wsx;", CommonUtil.GetFilePath(dataSource)));
#endif
        }

        #region Wordbook
        public bool GetWordBooks(List<long> wordBookIds, out List<WordBookModel> wordBooks)
        {
            return _daoInstance.GetWordBooks(wordBookIds, out wordBooks);
        }

        public bool AddWordBook(WordBookModel wordBook, out long id)
        {
            return _daoInstance.AddWordBook(wordBook, out id);
        }

        public bool EditWordBook(WordBookModel wordBook)
        {
            return _daoInstance.EditWordBook(wordBook);
        }

        public bool RemoveWordBook(long Id)
        {
            return _daoInstance.RemoveWordBook(Id);
        }

        public bool GetWordBookIds(long wordId, out List<long> wordBookIds)
        {
            return _daoInstance.GetWordBookIds(wordId, out wordBookIds);
        }

        public bool GetWordBooks(string word, out List<SearchWordInWordBookModel> wordBook)
        {
            return _daoInstance.GetWordBooks(word, out wordBook);
        }
        public bool GetWord(List<long> Ids, out int count)
        {
            return _daoInstance.GetWords(Ids, out count);
        }
        #endregion

        #region Word

        public bool AddWord(WordModel word, List<long> wordBookSelId, out long id)
        {
            return _daoInstance.AddWord(word, wordBookSelId, out id);
        }

        public bool EditWord(WordModel word, List<long> wordBookSelId)
        {
            return _daoInstance.EditWord(word, wordBookSelId);
        }

        public bool RemoveWord(long Id)
        {
            return _daoInstance.RemoveWord(Id);
        }
        public bool GetWord(long Id, out WordModel resultWord)
        {
            return _daoInstance.GetWord(Id, out resultWord);
        }

        public bool GetWords(List<long> Ids, string word, out List<WordModel> resultWords, bool onlyWord = false)
        {
            return _daoInstance.GetWords(Ids, word, out resultWords, onlyWord);
        }

        public bool IncreaseForget(long id)
        {
            return _daoInstance.IncreaseForget(id);
        }
        public bool ReturnForgetZero(long id)
        {
            return _daoInstance.ReturnForgetZero(id);
        }
        public bool GetForget(long id, out int count)
        {
            return _daoInstance.GetForget(id, out count);
        }
        public bool SetForget(long id, int count)
        {
            return _daoInstance.SetForget(id, count);
        }

         public bool CheckWordDuplicate(string word, List<long> wordBookSelId, out DataTable duplicateData)
        {
            return _daoInstance.CheckWordDuplicate(word, wordBookSelId, out duplicateData);
        }
       #endregion

        #region WordRead
        public bool GetWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return _daoInstance.GetWords(wordbookIds, out words);
        }
        public bool GetUnReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return _daoInstance.GetUnReadWords(wordbookIds, out words);
        }
        public bool GetReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return _daoInstance.GetReadWords(wordbookIds, out words);
        }

        public bool SaveReadWord(List<long> wordbookIds, long wId)
        {
            return _daoInstance.SaveReadWord(wordbookIds, wId);
        }

        public bool ReturnAllUnRead(List<long> wordbookIds)
        {
            return _daoInstance.ReturnAllUnRead(wordbookIds);
        }

        public bool GetAllReadData(out DataTable data)
        {
            return _daoInstance.GetAllReadData(out data);
        }
        public bool SaveAllReadData(DataTable data)
        {
            return _daoInstance.SaveAllReadData(data);
        }

        #endregion

        #region Setting
        public bool SaveSetting(List<SettingModel> settings)
        {
            return _daoInstance.SaveSetting(settings);
        }

        public bool GetSetting(string category, string key, out List<SettingModel> settings)
        {
            return _daoInstance.GetSetting(category, key, out settings);
        }
        #endregion

        #region Exam

        public bool GetChoiceItems(long id,out List<ExamChoiceModel> examChoices)
        {
            return _daoInstance.GetChoiceItems(id, out examChoices);
        }

        public bool AddExamResult(ExamType type, List<long> wordBookSelIds, List<WordModel> examWords, Dictionary<WordModel, FailureModel> failureWords)
        {
            return _daoInstance.AddExamResult(type, wordBookSelIds, examWords, failureWords);
        }
        #endregion

        #region Wordbook Mapping
        public bool AddWordbookMapping(long wordbookId, long wordId)
        {
            return _daoInstance.AddWordbookMapping(wordbookId, wordId);
        }

        public bool RemoveWordbookMapping(long? wordbookId, long? wordId)
        {
            return _daoInstance.RemoveWordbookMapping(wordbookId, wordId);
        }

        public bool GetWordbookMapping(long? wordbookId, long? wordId, out List<WordBookMapping> wordbookMapping)
        {
            return _daoInstance.GetWordbookMapping(wordbookId, wordId, out wordbookMapping);
        }
        #endregion

        public bool Dispose()
        {
            return _daoInstance.Dispose();
        }

    }
}
