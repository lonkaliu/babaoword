﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Category;

namespace BaBaoWord.Util
{
    internal class SQLiteApi : IApi
    {
        private DapperFactory dapper;
        private string _connectionString = string.Empty;
        public SQLiteApi(string connectionString)
        {
            _connectionString = connectionString;
            CreateDB(connectionString);
            //dapper.ExecuteSQL("PRAGMA auto_vacuum = 1;");
        }

        private void CreateDB(string connectionString)
        {
            bool dbExist;
            dapper = new DapperFactory(connectionString, out dbExist);
            if (!dbExist)
            {
                CreateWordBookTable();
                CreateWordTable();
                CreateWordBookMappingWordTable();
                CreatePronunciationTable();
                CreateExplanationTable();
                CreateWordReadTable();
                CreateSettingTable();
                CreateReadDiminishingTable();
                CreateExamMainTable();
                CreateExamWordbookTable();
                CreateExamWrongWordTable();
                //
            }
            else
            {
                CheckReadDiminishingTable();
                CreateExamMainTable();
                CreateExamWordbookTable();
                CreateExamWrongWordTable();
                AlterWordTable();
            }
        }

        public bool CreateDB()
        {
            bool result = false;
            try
            {
                this.CreateDB(this._connectionString);
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        private void CheckReadDiminishingTable()
        {
            object value;
            if (dapper.ExecuteScalarSQL("PRAGMA table_info(READ_DIMINISHING);", out value) && value == null)
            {
                CreateReadDiminishingTable();
            }
        }

        private bool CreateReadDiminishingTable()
        {
            if (dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS READ_DIMINISHING (READ_COUNT INTEGER NOT NULL DEFAULT 0, DIMINISHING_DAY INTEGER NOT NULL DEFAULT 0 , UNIQUE(READ_COUNT, DIMINISHING_DAY));"))
            {
                dapper.ExecuteSQL(@"INSERT INTO READ_DIMINISHING VALUES(0,1);
                                    INSERT INTO READ_DIMINISHING VALUES(1,1);
                                    INSERT INTO READ_DIMINISHING VALUES(2,1);
                                    INSERT INTO READ_DIMINISHING VALUES(3,1);
                                    INSERT INTO READ_DIMINISHING VALUES(4,1);
                                    INSERT INTO READ_DIMINISHING VALUES(5,1);
                                    INSERT INTO READ_DIMINISHING VALUES(6,1);
                                    INSERT INTO READ_DIMINISHING VALUES(7,1);
                                    INSERT INTO READ_DIMINISHING VALUES(8,1);
                                    INSERT INTO READ_DIMINISHING VALUES(9,1);
                                    INSERT INTO READ_DIMINISHING VALUES(10,1);
                                    INSERT INTO READ_DIMINISHING VALUES(11,1);
                                    INSERT INTO READ_DIMINISHING VALUES(12,1);
                                    INSERT INTO READ_DIMINISHING VALUES(13,1);
                                    INSERT INTO READ_DIMINISHING VALUES(14,1);
                                    INSERT INTO READ_DIMINISHING VALUES(15,2);
                                    INSERT INTO READ_DIMINISHING VALUES(16,2);
                                    INSERT INTO READ_DIMINISHING VALUES(17,3);
                                    INSERT INTO READ_DIMINISHING VALUES(18,3);
                                    INSERT INTO READ_DIMINISHING VALUES(19,4);
                                    INSERT INTO READ_DIMINISHING VALUES(20,4);
                                    INSERT INTO READ_DIMINISHING VALUES(21,8);
                                    INSERT INTO READ_DIMINISHING VALUES(22,9);
                                    INSERT INTO READ_DIMINISHING VALUES(23,10);
                                    INSERT INTO READ_DIMINISHING VALUES(24,11);
                                    INSERT INTO READ_DIMINISHING VALUES(25,12);
                                    INSERT INTO READ_DIMINISHING VALUES(26,13);
                                    INSERT INTO READ_DIMINISHING VALUES(27,14);
                                    INSERT INTO READ_DIMINISHING VALUES(28,15);
                                    INSERT INTO READ_DIMINISHING VALUES(29,16);
                                    INSERT INTO READ_DIMINISHING VALUES(30,17);
                                    INSERT INTO READ_DIMINISHING VALUES(31,18);");
                return true;
            }
            return false;
        }

        private bool CreateWordBookMappingWordTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [WORDBOOK_WORD] ([WBID] INTEGER NOT NULL, [WID] INTEGER NOT NULL, UNIQUE ([WBID], [WID]));");
        }

        private bool CreateWordReadTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS WORD_READ (WBKEY TEXT NOT NULL, WID INTEGER NOT NULL,UNIQUE(WBKEY,WID ));");
        }

        #region WordBook

        private bool CreateWordBookTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [WORDBOOK] ([ID] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [NAME] TEXT NOT NULL UNIQUE COLLATE NOCASE);");
        }
        public bool GetWordBooks(List<long> wordBookIds, out List<WordBookModel> wordBooks)
        {
            bool result = false;
            wordBooks = new List<WordBookModel>();
            object value;
            List<string> whereList = new List<string>();

            if (wordBookIds != null)
            {
                whereList.Add(" A.ID IN @WBID ");
            }
            string whereStr = string.Empty;
            if (whereList.Count > 0)
            {
                whereStr = " WHERE " + string.Join("AND", whereList.ToArray());
            }
            if (dapper.GetModel<WordBookModel>(string.Format("SELECT A.ID,A.NAME,COUNT(B.WID) AS WORDCOUNT FROM [WORDBOOK] A LEFT JOIN WORDBOOK_WORD B ON A.ID = B.WBID {0} GROUP BY A.ID,A.NAME", whereStr), out value, new
            {
                WBID = wordBookIds == null ? new long[] { } : wordBookIds.ToArray()
            }))
            {
                wordBooks = (value as List<WordBookModel>);
                result = true;
            }
            return result;
        }

        public bool GetWordBookIds(long wordId, out List<long> wordBookIds)
        {
            bool result = false;
            wordBookIds = new List<long>();
            object value;
            if (dapper.GetModel<long>("SELECT WBID FROM WORDBOOK_WORD WHERE WID = @WID", out value, new { WID = wordId }))
            {
                wordBookIds = new List<long>(value as IEnumerable<long>);
                result = true;
            }
            return result;
        }


        public bool RemoveWordBook(long id)
        {
            bool result = false;
            try
            {
                dapper.ExecuteSQL(@"DELETE FROM EXPLANATION WHERE WID IN (SELECT WID FROM WORDBOOK_WORD WHERE WBID = @ID);
                                    DELETE FROM PRONUNCIATION WHERE WID IN (SELECT WID FROM WORDBOOK_WORD WHERE WBID = @ID);
                                    DELETE FROM WORD WHERE ID IN (SELECT WID FROM WORDBOOK_WORD WHERE WBID = @ID);
                                    DELETE FROM WORD_READ WHERE WBKEY LIKE @LikeID;
                                    DELETE FROM WORDBOOK_WORD WHERE WBID = @ID;
                                    DELETE FROM WORDBOOK WHERE ID = @ID; ", new { ID = id, LikeID = "%-" + id + "-%" });
                dapper.ExecuteSQL("VACUUM;");
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool AddWordBook(WordBookModel wordBook, out long id)
        {
            id = long.MinValue;
            object value;
            bool result = false;
            if (dapper.ExecuteScalarSQL("INSERT INTO [WORDBOOK] (NAME) VALUES (@NAME);SELECT LAST_INSERT_ROWID();", out value, wordBook))
            {
                if (value is long)
                {
                    id = (long)value;
                    result = true;
                }
            }
            return result;
        }

        public bool EditWordBook(WordBookModel wordBook)
        {
            bool result = false;
            if (dapper.ExecuteSQL("UPDATE [WORDBOOK] SET NAME=@NAME WHERE ID=@ID", wordBook))
            {
                result = true;
            }
            return result;
        }

        public bool GetWordBooks(string word, out List<SearchWordInWordBookModel> wordBook)
        {
            bool result = false;
            object value;
            wordBook = new List<SearchWordInWordBookModel>();
            if (dapper.GetModel<SearchWordInWordBookModel>("SELECT B.ID AS WORDID,GROUP_CONCAT(C.NAME) AS WORDBOOKNAME FROM WORDBOOK_WORD A LEFT JOIN WORD B ON A.WID = B.ID LEFT JOIN WORDBOOK C ON A.WBID = C.ID WHERE B.WORD = @WORD COLLATE NOCASE GROUP BY B.ID ", out value, new { WORD = word }))
            {
                result = true;
                wordBook = new List<SearchWordInWordBookModel>(value as IEnumerable<SearchWordInWordBookModel>);
            }
            return result;
        }

        private bool GetBookWordCount(long wbId, List<long> wordIds, out List<long> bWordIds)
        {
            bool result = false;
            object value;
            bWordIds = new List<long>();
            if (dapper.GetModel<long>(@"SELECT WID FROM WORDBOOK_WORD WHERE WBID = @WBID AND WID IN @WID ", out value, new { WBID = wbId, @WID = wordIds.ToArray() }))
            {
                bWordIds = (List<long>)value;
                result = true;
            }
            return result;
        }


        #endregion

        #region WordRead
        public bool GetWords(List<long> wordbookIds, out List<WordModel> words)
        {
            words = new List<WordModel>();
            bool result = false;
            try
            {
                if (wordbookIds.Count > 0)
                {
                    object value;
                    if (dapper.GetModel<long>("SELECT DISTINCT WID FROM WORDBOOK_WORD A LEFT JOIN WORD B ON A.WID = B.ID WHERE WBID IN @ID ", out value, new { ID = wordbookIds.ToArray() }))
                    {
                        List<long> wIds = new List<long>(value as IEnumerable<long>);
                        if (GetWords(wIds, string.Empty, out words, true))
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }


        public bool GetWords(List<long> wordbookIds, out int count)
        {
            count = 0;
            bool result = false;
            try
            {
                if (wordbookIds.Count > 0)
                {
                    string value;
                    if (dapper.GetValue("SELECT COUNT(DISTINCT WID) FROM WORDBOOK_WORD A LEFT JOIN WORD B ON A.WID = B.ID WHERE WBID IN @ID ", out value, new { ID = wordbookIds.ToArray() }))
                    {
                        if (int.TryParse(value, out count))
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }


        public bool GetReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            words = new List<WordModel>();
            bool result = false;
            try
            {
                if (wordbookIds != null && wordbookIds.Count > 0)
                {
                    string wbkey = GetWordReadKey(wordbookIds);
                    object value;
                    if (dapper.GetModel<long>("SELECT WID FROM WORD_READ WHERE WBKEY = @WBKEY", out value, new { WBKEY = wbkey }))
                    {
                        List<long> wIds = new List<long>(value as IEnumerable<long>);
                        if (GetWords(wIds, string.Empty, out words, true, false))
                        {
                            result = true;
                        }
                    }
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool GetUnReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            words = new List<WordModel>();
            bool result = false;
            try
            {
                if (wordbookIds != null && wordbookIds.Count > 0)
                {
                    string wbkey = GetWordReadKey(wordbookIds);
                    object value;
                    if (dapper.GetModel<long>("SELECT WID FROM WORD_READ WHERE WBKEY = @WBKEY", out value, new { WBKEY = wbkey }))
                    {
                        List<long> wIds = new List<long>(value as IEnumerable<long>);
                        if (dapper.GetModel<long>("SELECT DISTINCT WID FROM WORDBOOK_WORD A LEFT JOIN WORD B ON A.WID = B.ID WHERE WBID IN @ID AND WID NOT IN @WID", out value, new { ID = wordbookIds.ToArray(), WID = wIds.ToArray() }))
                        {
                            wIds = new List<long>(value as IEnumerable<long>);
                            if (GetWords(wIds, string.Empty, out words, true, false))
                            {
                                result = true;
                            }
                        }
                    }
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool SaveReadWord(List<long> wordbookIds, long wId)
        {
            bool result = false;
            try
            {
                string wbkey = GetWordReadKey(wordbookIds);

                if (dapper.ExecuteSQL("DELETE FROM WORD_READ WHERE WBKEY = @WBKEY AND WID = @WID;INSERT INTO WORD_READ VALUES(@WBKEY,@WID);", new { WBKEY = wbkey, WID = wId }))
                {
                    dapper.ExecuteSQL("VACUUM;");
                    result = true;
                    object checkValueStr;
                    double checkValue;
                    /*
                    讀次的計算
                    
                    該讀次的加總遞減天 - (現在時間 - 建單字時間 + 1)
                    ex 3/25建的單字、現在3/30日 讀次4次 遞減加總為 5 (還可以再讀)
                      = 5 - (30 - 25 + 1) = 5 - 6 = - 1
                    ex 3/25建的單字、現在3/30日 讀次6次 遞減加總為 7 (已讀到上限)
                      = 7 - (30 - 25 + 1) = 7 - 6 =  1
                    算出來的值小於等於0就表示還可以再讀
                    */
                    if (dapper.ExecuteScalarSQL(@"SELECT SUM(DIMINISHING_DAY)-(JULIANDAY(DATETIME('NOW'))-JULIANDAY((SELECT CREATE_DATE FROM WORD WHERE ID = @ID)) + 1)
                                            FROM READ_DIMINISHING WHERE READ_COUNT <= (SELECT READ_COUNT FROM WORD WHERE ID = @ID)", out checkValueStr, new { ID = wId })
                        && checkValueStr != null && double.TryParse(checkValueStr.ToString(), out checkValue) && checkValue <= 0)
                    {
                        result = dapper.ExecuteSQL("UPDATE WORD SET READ_COUNT = READ_COUNT +1,READ_DATE = CURRENT_TIMESTAMP WHERE ID = @ID", new { ID = wId });
                    }

                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool ReturnAllUnRead(List<long> wordbookIds)
        {
            bool result = false;
            try
            {
                string whereStr = string.Empty;
                string wbkey = string.Empty;
                if (wordbookIds != null)
                {
                    whereStr = " WHERE WBKEY = @WBKEY";
                    wbkey = GetWordReadKey(wordbookIds);
                }


                if (dapper.ExecuteSQL("DELETE FROM WORD_READ " + whereStr, new { WBKEY = wbkey }))
                {
                    dapper.ExecuteSQL("VACUUM;");
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        private string GetWordReadKey(List<long> wordbookIds)
        {
            return "-" + string.Join("-", wordbookIds.ToArray()) + "-";
        }

        public bool GetAllReadData(out DataTable data)
        {
            data = new DataTable();
            bool result = false;
            try
            {
                result = dapper.GetDataTable("SELECT * FROM WORD_READ", out data);
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool SaveAllReadData(DataTable data)
        {
            bool result = false;
            try
            {
                var param = data.AsEnumerable().Select(item => new { WBKEY = item["WBKEY"].ToString(), WID = item["WID"].ToString() });
                result = dapper.ExecuteSQL("INSERT INTO WORD_READ VALUES (@WBKEY,@WID);", param);
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        #endregion

        #region Word
        private bool CreateWordTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [WORD] ([ID] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,[SOUND] TEXT, [WORD] TEXT NOT NULL, [IMPORTANCE] TEXT NOT NULL,FORGET_COUNT INTEGER NOT NULL DEFAULT 0,READ_COUNT INTEGER NOT NULL DEFAULT 0,EXAM_COUNT INTEGER NOT NULL DEFAULT 0,CREATE_DATE DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,READ_DATE DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);");
        }
        private void AlterWordTable()
        {
            DataTable value;
            if (dapper.GetDataTable("PRAGMA table_info(WORD);", out value))
            {
                var forgetCountCol = value.AsEnumerable().Where(item => item["name"].ToString().Equals("FORGET_COUNT"));
                if (!forgetCountCol.Any())
                {
                    dapper.ExecuteSQL("ALTER TABLE WORD ADD COLUMN FORGET_COUNT INTEGER NOT NULL DEFAULT 0");
                }

                var readCountCol = value.AsEnumerable().Where(item => item["name"].ToString().Equals("READ_COUNT"));
                if (!readCountCol.Any())
                {
                    dapper.ExecuteSQL("ALTER TABLE WORD ADD COLUMN READ_COUNT INTEGER NOT NULL DEFAULT 0");
                }

                var wrongCountCol = value.AsEnumerable().Where(item => item["name"].ToString().Equals("EXAM_COUNT"));
                if (!wrongCountCol.Any())
                {
                    dapper.ExecuteSQL("ALTER TABLE WORD ADD COLUMN EXAM_COUNT INTEGER NOT NULL DEFAULT 0");
                }

                var dateCountCol = value.AsEnumerable().Where(item => item["name"].ToString().Equals("READ_DATE"));
                if (!dateCountCol.Any())
                {
                    dapper.ExecuteSQL(@"ALTER TABLE WORD ADD COLUMN CREATE_DATE DATETIME;
                                        ALTER TABLE WORD ADD COLUMN READ_DATE DATETIME;
                                        UPDATE WORD SET CREATE_DATE=CURRENT_TIMESTAMP,READ_DATE=CURRENT_TIMESTAMP;
                                        ALTER TABLE WORD RENAME TO oXHFcGcd04oXHFcGcd04_WORD;
                                        CREATE TABLE WORD (ID INTEGER PRIMARY KEY  NOT NULL ,SOUND TEXT,WORD TEXT NOT NULL ,IMPORTANCE TEXT NOT NULL ,FORGET_COUNT INTEGER NOT NULL DEFAULT (0) ,READ_COUNT INTEGER NOT NULL  DEFAULT (0) ,CREATE_DATE DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP) ,READ_DATE DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP));
                                        INSERT INTO WORD SELECT ID,SOUND,WORD,IMPORTANCE,FORGET_COUNT,READ_COUNT,CREATE_DATE,READ_DATE FROM oXHFcGcd04oXHFcGcd04_WORD;
                                        DROP TABLE oXHFcGcd04oXHFcGcd04_WORD;");
                }

            }
        }

        public bool AddWord(WordModel word, List<long> wordBookSelId, out long id)
        {
            bool result = false;
            id = long.MinValue;
            try
            {

                object value;
                string path;
                if (CopyMp3(word.Sound, out path))
                {
                    word.Sound = path;
                }
                else
                {
                    word.Sound = string.Empty;
                    word.IsRemoteSound = false;
                }

                if (dapper.ExecuteScalarSQL("INSERT INTO WORD (WORD,IMPORTANCE,SOUND) VALUES (@WORD,@IMPORTANCE,@SOUND); SELECT LAST_INSERT_ROWID();", out value, word))
                {
                    if (value is long)
                    {
                        id = (long)value;
                        word.Id = id;
                        if (wordBookSelId != null)
                        {
                            if (!SyncWordBookMappingWord(id, wordBookSelId, true))
                            {
                                return result;
                            }
                        }

                        if (!SyncExplanation(id, word))
                        {
                            return result;
                        }

                        if (!SyncPronunciation(id, word))
                        {
                            return result;
                        }
                        result = true;

                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool GetWord(long Id, out WordModel resultWord)
        {
            bool result = false;
            resultWord = new WordModel();
            try
            {
                object value;
                if (dapper.GetModel<WordModel>("SELECT * FROM WORD WHERE ID = @ID", out value, new { ID = Id }))
                {
                    IEnumerable<WordModel> valueList = value as IEnumerable<WordModel>;
                    if (valueList.Count() > 0)
                    {
                        resultWord = valueList.FirstOrDefault();
                        TrulyObservableCollection<PronunciationModel> pronunciation;
                        if (GetPronunciation(Id, out pronunciation))
                        {
                            resultWord.Pronunciation = pronunciation;
                        }
                        TrulyObservableCollection<ExplanationModel> explanation;
                        if (GetExplanation(Id, out explanation))
                        {
                            resultWord.Explanation = explanation;
                        }
                        result = true;
                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool GetWords(List<long> Ids, string word, out List<WordModel> resultWords, bool onlyWord = false, bool isAll = true)
        {
            bool result = false;
            resultWords = new List<WordModel>();
            try
            {

                List<string> whereList = new List<string>();
                whereList.Add(" 1=1 ");
                if (Ids != null)
                {
                    whereList.Add(" A.ID IN @ID ");
                }
                if (!string.IsNullOrEmpty(word))
                {
                    whereList.Add(" A.WORD = @WORD COLLATE NOCASE ");
                }
                if (!isAll)
                {
                    whereList.Add(" REAL_UNREAD >= 0 ");
                    whereList.Add(" A.READ_COUNT <= (SELECT MAX(READ_COUNT) FROM READ_DIMINISHING) ");
                }
                string whereStr = string.Empty;
                if (whereList.Count > 0)
                {
                    whereStr = " WHERE " + string.Join("AND", whereList.ToArray());
                }
                string orderStr = "REAL_WEIGHT,FORGET_COUNT DESC,IMPORTANCE DESC,WORD";
                WordSortType sortBy =CommonUtil. GetSortByModel();
                switch (sortBy)
                {
                    case WordSortType.ByWeight:
                        break;
                    case WordSortType.ByWrongRate:
                        orderStr = "WRONG_RATE DESC," + orderStr;
                        break;
                    case WordSortType.ByForgetCount:
                        orderStr = "FORGET_COUNT DESC,REAL_WEIGHT,IMPORTANCE DESC,WORD";
                        break;
                    case WordSortType.ByImportance:
                        orderStr = "IMPORTANCE DESC,REAL_WEIGHT,FORGET_COUNT DESC,WORD";
                        break;
                    case WordSortType.ByWord:
                        orderStr = "WORD";
                        break;
                    case WordSortType.ById:
                        orderStr = "WID";
                        break;
                    default:
                        break;
                }

                string wordFilterWhereStr = " 1=1 ";
                List<WordFilterModel> wordFilters = CommonUtil.GetFilterModel();
                foreach(var item in wordFilters)
                {
                    wordFilterWhereStr += " AND ";
                    switch (item.WordSort)
                    {
                        case WordSortType.ByWeight:
                            wordFilterWhereStr += " REAL_WEIGHT ";
                            break;
                        case WordSortType.ByWrongRate:
                            wordFilterWhereStr += " WRONG_RATE ";
                            break;
                        case WordSortType.ByForgetCount:
                            wordFilterWhereStr += " FORGET_COUNT ";
                            break;
                    }
                    switch (item.Compare)
                    {
                        case CompareType.GreaterThenEqual:
                            wordFilterWhereStr += " >= ";
                            break;
                        case CompareType.LessThenEqual:
                            wordFilterWhereStr += " <= ";
                            break;
                        case CompareType.Equal:
                            wordFilterWhereStr += " = ";
                            break;
                    }
                    wordFilterWhereStr += item.Value;
                }
                DataTable value;
                /*
                計算單字的權重
                    未讀天公式：如果讀次等於0 或 (現在時間 - 上次讀的時間) <1 時未讀天就設為0，不然未讀天為(現在時間 - 上次讀的時間) - 該讀次的遞減天
                    weight公式：如果讀次等於0 就設為-999999，不然weight為(讀次-未讀天) * 讀次
                    加強weight公式：如果weight>0 就讓weight / 單字重要性+1 / 單字加強次數+1，不然加強weight為weight * 單字重要性+1 * 單字加強次數+1
                加強weight的值越小越值得讀
                */

                /*
                計算單字是否出現
                    1.讀次大於READ_DIMINISHING的最大值就不要再出現了
                    2.未讀天 < 0 就不出現 
                */
                //TODO 小數問題
                if (dapper.GetDataTable(string.Format(@"
                    SELECT *,CASE WHEN WEIGHT > 0 THEN ROUND(WEIGHT / (IMPORTANCE+1.0) / (FORGET_COUNT+1.0),2) ELSE WEIGHT * (IMPORTANCE+1.0) *(FORGET_COUNT+1.0) END AS REAL_WEIGHT  FROM
                    (
                        SELECT *,CASE WHEN READ_COUNT = 0 THEN -999999 ELSE (READ_COUNT - CAST(REAL_UNREAD AS INT)) * READ_COUNT END AS WEIGHT
                        FROM 
                        (
                            SELECT 
                                A.ID AS WID,A.SOUND,A.WORD,A.IMPORTANCE
                                ,B.EXPLANATION,B.SPEECH,B.SENTENCE
                                ,case when E.EXPLANATION_O is null then '' else E.EXPLANATION_O end as EXPLANATION_O
                                ,case when E.SPEECH_O is null then '' else E.SPEECH_O end as SPEECH_O
                                ,case when E.SENTENCE_O is null then '' else E.SENTENCE_O end as SENTENCE_O
                                ,C.PRONUNCIATION,C.SOUND AS PSOUND
                                ,A.READ_COUNT
                                ,A.EXAM_COUNT
                                ,CASE WHEN F.WRONG_COUNT IS NULL THEN 0 ELSE F.WRONG_COUNT END AS WRONG_COUNT
                                ,A.FORGET_COUNT
                                ,CASE WHEN  A.READ_COUNT = 0 THEN 0
                                WHEN (JULIANDAY(DATETIME('NOW')) - JULIANDAY(A.READ_DATE)) < 1 THEN 0
                                 ELSE  (JULIANDAY(DATETIME('NOW')) - JULIANDAY(A.READ_DATE))  - D.DIMINISHING_DAY END AS REAL_UNREAD
                                ,CASE WHEN F.WRONG_COUNT IS NULL THEN 0 ELSE ROUND(((F.WRONG_COUNT*1.0)/(A.EXAM_COUNT*1.0))*100,2) END AS WRONG_RATE
                            FROM WORD A 
                                LEFT JOIN EXPLANATION B ON A.ID = B.WID 
                                LEFT JOIN PRONUNCIATION C ON A.ID = C.WID 
                                LEFT JOIN READ_DIMINISHING D ON A.READ_COUNT = D.READ_COUNT
                                LEFT JOIN (SELECT WID,EXPLANATION AS EXPLANATION_O,SPEECH AS SPEECH_O,SENTENCE AS SENTENCE_O FROM EXPLANATION WHERE SORT = 1) E ON A.ID = E.WID
                                LEFT JOIN (SELECT WID,COUNT(*) WRONG_COUNT FROM EXAM_WRONG_WORD GROUP BY WID) F ON A.ID=F.WID
                            {0} 
                                AND B.SORT = 0 AND C.SORT = 0 
                        )
                    )
                     Where {2}
                     ORDER BY {1}
                    ", whereStr, orderStr, wordFilterWhereStr)
                , out value, new
                {
                    ID = Ids == null ? new long[] { } : Ids.ToArray(),
                    WORD = word
                }))
                {
                    if (value != null)
                    {
                        string tempWid = string.Empty;
                        foreach (DataRow dr in value.Rows)
                        {
                            if (tempWid.Equals(dr["WID"].ToString()))
                            {
                                continue;
                            }
                            tempWid = dr["WID"].ToString();
                            WordModel wordModel = new WordModel();
                            wordModel.Id = long.Parse(dr["WID"].ToString());
                            wordModel.Sound = dr["SOUND"].ToString();
                            wordModel.Word = dr["WORD"].ToString();
                            wordModel.Importance = (Category.ImportanceType)(Enum.Parse(typeof(Category.ImportanceType), dr["IMPORTANCE"].ToString()));
                            wordModel.FORGET_COUNT = long.Parse(dr["FORGET_COUNT"].ToString());
                            wordModel.Weight = double.Parse(dr["REAL_WEIGHT"].ToString());
                            wordModel.WrongRate = double.Parse(dr["WRONG_RATE"].ToString());
                            wordModel.WrongCount = int.Parse(dr["WRONG_COUNT"].ToString());
                            wordModel.ExamCount = int.Parse(dr["EXAM_COUNT"].ToString());

                            if (onlyWord)
                            {

                                wordModel.Explanation = new TrulyObservableCollection<ExplanationModel>();
                                ExplanationModel explanationModel = new ExplanationModel();
                                explanationModel.Explanation = dr["EXPLANATION"].ToString();
                                try
                                {
                                    explanationModel.Speech = (Category.SpeechType)(Enum.Parse(typeof(Category.SpeechType), dr["SPEECH"].ToString()));
                                }
                                catch
                                { }
                                explanationModel.Sentence = dr["SENTENCE"].ToString();
                                wordModel.Explanation.Add(explanationModel);

                                if (!string.IsNullOrEmpty(dr["EXPLANATION_O"].ToString()))
                                {
                                    ExplanationModel explanationOModel = new ExplanationModel();
                                    explanationOModel.Explanation = dr["EXPLANATION_O"].ToString();
                                    try
                                    {
                                        explanationOModel.Speech = (Category.SpeechType)(Enum.Parse(typeof(Category.SpeechType), dr["SPEECH_O"].ToString()));
                                    }
                                    catch
                                    { }
                                    explanationOModel.Sentence = dr["SENTENCE_O"].ToString();
                                    wordModel.Explanation.Add(explanationOModel);
                                }

                                wordModel.Pronunciation = new TrulyObservableCollection<PronunciationModel>();
                                PronunciationModel pronunciationModel = new PronunciationModel();
                                pronunciationModel.Pronunciation = dr["PRONUNCIATION"].ToString();
                                pronunciationModel.Sound = dr["PSOUND"].ToString();
                                wordModel.Pronunciation.Add(pronunciationModel);
                            }
                            else
                            {
                                TrulyObservableCollection<PronunciationModel> pronunciation;
                                if (GetPronunciation(wordModel.Id.Value, out pronunciation))
                                {
                                    wordModel.Pronunciation = pronunciation;
                                }
                                TrulyObservableCollection<ExplanationModel> explanation;
                                if (GetExplanation(wordModel.Id.Value, out explanation))
                                {
                                    wordModel.Explanation = explanation;
                                }
                            }
                            resultWords.Add(wordModel);
                        }

                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }


        public bool EditWord(WordModel word, List<long> wordbookSelId)
        {
            bool result = false;
            try
            {
                string path;
                if (CopyMp3(word.Sound, out path))
                {
                    word.Sound = path;
                }
                else
                {
                    word.Sound = string.Empty;
                    word.IsRemoteSound = false;
                }

                if (dapper.ExecuteSQL("UPDATE WORD SET WORD=@WORD,IMPORTANCE=@IMPORTANCE,SOUND=@SOUND WHERE ID=@ID;", word))
                {
                    long wId = word.Id.Value;
                    if (wordbookSelId != null)
                    {
                        if (!SyncWordBookMappingWord(wId, wordbookSelId, false))
                        {
                            return result;
                        }
                    }
                    if (!SyncExplanation(wId, word))
                    {
                        return result;
                    }

                    if (!SyncPronunciation(wId, word))
                    {
                        return result;
                    }
                    result = true;

                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool RemoveWord(long id)
        {
            bool result = false;
            try
            {
                string strSql = @" DELETE FROM WORDBOOK_WORD WHERE WID=@ID;
                                   DELETE FROM EXPLANATION WHERE WID=@ID;
                                   DELETE FROM PRONUNCIATION WHERE WID=@ID;
                                   DELETE FROM WORD WHERE ID=@ID;
                                   DELETE FROM WORD_READ WHERE WID=@ID;";
                if (dapper.ExecuteSQL(strSql, new { ID = id }))
                {
                    dapper.ExecuteSQL("VACUUM;");
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool CheckWordDuplicate(string word, List<long> wordBookSelId, out DataTable duplicateData)
        {
            bool result = false;
            duplicateData = new DataTable();
            try
            {
                List<string> whereList = new List<string>();
                if (wordBookSelId != null)
                {
                    whereList.Add(" WBID IN @WBID ");
                }
                if (!string.IsNullOrEmpty(word))
                {
                    whereList.Add(" WORD = @WORD COLLATE NOCASE ");
                }
                string whereStr = string.Empty;
                if (whereList.Count > 0)
                {
                    whereStr = " WHERE " + string.Join("AND", whereList.ToArray());
                }
                if (dapper.GetDataTable(" SELECT C.NAME,B.WORD,B.ID FROM WORDBOOK_WORD A LEFT JOIN WORD B ON A.WID = B.ID LEFT JOIN WORDBOOK C ON A.WBID = C.ID " + whereStr, out duplicateData, new { WBID = wordBookSelId.ToArray(), WORD = word }))
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool IncreaseForget(long id)
        {
            bool result = false;
            try
            {
                result = dapper.ExecuteSQL("UPDATE WORD SET FORGET_COUNT = FORGET_COUNT+1 WHERE ID = @ID", new { ID = id });
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool ReturnForgetZero(long id)
        {
            bool result = false;
            try
            {
                result = dapper.ExecuteSQL("UPDATE WORD SET FORGET_COUNT = 0 WHERE ID = @ID", new { ID = id });
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool GetForget(long id, out int count)
        {
            bool result = false;
            count = 0;
            try
            {
                string value;
                if (dapper.GetValue("SELECT FORGET_COUNT FROM WORD WHERE ID = @ID", out value, new { ID = id }))
                {
                    result = int.TryParse(value, out count);
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool SetForget(long id, int count)
        {
            bool result = false;
            try
            {
                result = dapper.ExecuteSQL("UPDATE WORD SET FORGET_COUNT = @ForgetCount WHERE ID = @ID", new { ID = id, ForgetCount = count });
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool GetExportWords(List<long> wordbookIds, out List<WordModel> words)
        {
            words = new List<WordModel>();
            bool result = false;
            try
            {
                if (wordbookIds.Count > 0)
                {
                    object value;
                    if (dapper.GetModel<long>("SELECT DISTINCT WID FROM WORDBOOK_WORD A LEFT JOIN WORD B ON A.WID = B.ID WHERE WBID IN @ID ", out value, new { ID = wordbookIds.ToArray() }))
                    {
                        List<long> wIds = new List<long>(value as IEnumerable<long>);
                        if (GetWords(wIds, string.Empty, out words, false))
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        private bool UpdateExamCount(List<long> wIds)
        {
            bool result = false;
            try
            {
                result = dapper.ExecuteSQL("UPDATE WORD SET EXAM_COUNT = EXAM_COUNT +1  WHERE ID IN @ID", new { ID = wIds.ToArray() });
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;

        }

        #region Pronunciation
        private bool CreatePronunciationTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [PRONUNCIATION] ([ID] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [WID] INTEGER NOT NULL , [PRONUNCIATION] TEXT NOT NULL , [SOUND] TEXT, [SORT] INTEGER NOT NULL , UNIQUE([WID], [PRONUNCIATION] COLLATE NOCASE), UNIQUE([WID], [SORT]));");
        }

        private bool SyncPronunciation(long wId, WordModel word)
        {
            bool result = false;
            try
            {
                if (dapper.ExecuteSQL("DELETE FROM PRONUNCIATION WHERE WID=@WID", new { WID = wId }))
                {
                    dapper.ExecuteSQL("VACUUM;");
                    string path;
                    foreach (PronunciationModel pronunciation in word.Pronunciation)
                    {
                        if (CopyMp3(pronunciation.Sound, out path))
                        {
                            pronunciation.Sound = path;
                        }
                    }
                    var newData = word.Pronunciation.Select((item, index) => new
                    {
                        ID = Convert.DBNull,
                        WID = wId,
                        PRONUNCIATION = item.Pronunciation.Trim(),
                        SOUND = item.Sound,
                        SORT = index
                    });
                    if (newData.Any())
                    {
                        result = dapper.ExecuteSQL("INSERT INTO PRONUNCIATION VALUES (@ID,@WID,@PRONUNCIATION,@SOUND,@SORT)", newData);
                    }
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }
        private bool GetPronunciation(long Id, out TrulyObservableCollection<PronunciationModel> pronunciation)
        {
            pronunciation = new TrulyObservableCollection<PronunciationModel>();
            object value;
            if (dapper.GetModel<PronunciationModel>("SELECT * FROM PRONUNCIATION WHERE WID = @WID ORDER BY SORT", out value, new { WID = Id }))
            {
                IEnumerable<PronunciationModel> valueList = value as IEnumerable<PronunciationModel>;
                pronunciation = new TrulyObservableCollection<PronunciationModel>(valueList);
            }
            return true;
        }

        private bool CopyMp3(string mp3Path, out string path)
        {
            path = string.Empty;
            bool result = false;
            try
            {
                if (!string.IsNullOrEmpty(mp3Path))
                {
                    string[] structure = mp3Path.ToString().Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (structure.Length != 2)
                    {
                        return result;
                    }
                    if (structure[1].Equals("Temp"))
                    {
                        string[] words = structure[0].Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                        foreach (string word in words)
                        {
                            System.IO.File.Copy(
                                System.IO.Path.Combine(Parameter.GetInstance().Mp3TempDirectory, word + ".mp3"),
                                System.IO.Path.Combine(Parameter.GetInstance().Mp3Directory, word + ".mp3"), true);
                        }
                        path = structure[0] + ",Real";
                        result = true;
                    }
                    else if (structure[1].Equals("Real"))
                    {
                        path = mp3Path;
                        result = true;
                    }
                    else
                    {
                        return result; ;
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        #endregion

        #region Explanation
        private bool CreateExplanationTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [EXPLANATION] ([ID] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [WID] INTEGER NOT NULL, [EXPLANATION] TEXT NOT NULL, [SPEECH] TEXT NOT NULL, [SENTENCE] TEXT, [SORT] TEXT NOT NULL,UNIQUE([WID], [EXPLANATION] COLLATE NOCASE, [SPEECH]) ,UNIQUE([WID], [SORT]));");
        }

        private bool SyncExplanation(long wId, WordModel word)
        {
            bool result = false;
            try
            {
                if (dapper.ExecuteSQL("DELETE FROM EXPLANATION WHERE WID=@WID", new { WID = wId }))
                {
                    dapper.ExecuteSQL("VACUUM;");
                    var newData = word.Explanation.Select((item, index) => new
                    {
                        ID = Convert.DBNull,
                        WID = wId,
                        EXPLANATION = item.Explanation.Trim(),
                        SPEECH = item.Speech,
                        SENTENCE = item.Sentence.Trim(),
                        SORT = index
                    });
                    if (newData.Any())
                    {
                        result = dapper.ExecuteSQL("INSERT INTO EXPLANATION VALUES (@ID,@WID,@EXPLANATION,@SPEECH,@SENTENCE,@SORT)", newData);
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        private bool GetExplanation(long Id, out TrulyObservableCollection<ExplanationModel> explanation)
        {
            explanation = new TrulyObservableCollection<ExplanationModel>();
            object value;
            if (dapper.GetModel<ExplanationModel>("SELECT * FROM EXPLANATION WHERE WID = @WID ORDER BY SORT", out value, new { WID = Id }))
            {
                IEnumerable<ExplanationModel> valueList = value as IEnumerable<ExplanationModel>;
                explanation = new TrulyObservableCollection<ExplanationModel>(valueList);
            }
            return true;
        }

        #endregion

        #endregion

        #region Setting

        private bool CreateSettingTable()
        {
            bool result = false;
            try
            {
                dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS SETTING (CATEGORY TEXT NOT NULL, KEY TEXT NOT NULL, VALUE TEXT, DESCRIPTION TEXT,UNIQUE(CATEGORY,KEY));");
                List<SettingModel> settings = new List<SettingModel>();
                settings.Add(new SettingModel() { Category = "Rotate", Key = "BroadcastSound", Value = "True" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "Opacity", Value = "0.8" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "Interval", Value = "3" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "LunchTimeBegin", Value = "1200" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "LunchTimeEnd", Value = "1300" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "OffWorkTime", Value = "1700" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "WorkTime", Value = "0800" });
                settings.Add(new SettingModel() { Category = "WordBook", Key = "SortBy", Value = "ByWeight" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "AllWord", Value = "True" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "DelayShowExplanation", Value = "3" });
                settings.Add(new SettingModel() { Category = "Rotate", Key = "Random", Value = "True" });
                settings.Add(new SettingModel() { Category = "System", Key = "Version", Value = "1" });
                SaveSetting(settings);
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool SaveSetting(List<SettingModel> settings)
        {
            return dapper.ExecuteSQL("INSERT OR REPLACE INTO SETTING VALUES (@CATEGORY,@KEY,@VALUE,@DESCRIPTION)", settings);
        }

        public bool GetSetting(string category, string key, out List<SettingModel> settings)
        {
            settings = null;
            bool result = false;
            try
            {
                List<string> whereList = new List<string>();
                if (!string.IsNullOrEmpty(category))
                {
                    whereList.Add(" CATEGORY = @CATEGORY ");
                }
                if (!string.IsNullOrEmpty(key))
                {
                    whereList.Add(" KEY = @KEY ");
                }
                string whereStr = string.Empty;
                if (whereList.Count > 0)
                {
                    whereStr = " WHERE " + string.Join("AND", whereList.ToArray());
                }
                object value;
                if (dapper.GetModel<SettingModel>("SELECT * FROM SETTING" + whereStr, out value, new
                {
                    CATEGORY = category,
                    KEY = key
                }))
                {
                    settings = new List<SettingModel>(value as IEnumerable<SettingModel>);
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        #endregion

        #region Exam
        private bool CreateExamMainTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [EXAM_MAIN] ([ID] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,[TYPE] TEXT NOT NULL,[TOTAL] INTEGER NOT NULL, [WRONG_COUNT] INTEGER NOT NULL, [CORRECT_RATE] REAL NOT NULL,EXAM_DATE DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);");
        }

        private bool CreateExamWordbookTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [EXAM_WORDBOOK] ([EMID] INTEGER NOT NULL,[WBID] INTEGER NOT NULL,[WORDS] TEXT NOT NULL,[TOTAL] INTEGER NOT NULL, [WRONG_COUNT] INTEGER NOT NULL, [CORRECT_RATE] REAL NOT NULL ,UNIQUE([EMID],[WBID],[WORDS]));");
        }

        private bool CreateExamWrongWordTable()
        {
            return dapper.ExecuteSQL("CREATE TABLE IF NOT EXISTS [EXAM_WRONG_WORD] ([EMID] INTEGER NOT NULL,[WID] INTEGER NOT NULL, [FIRST_ERROR] TEXT NOT NULL,[ERROR_COUNT] INTEGER NOT NULL, [IS_SHOW_ANSWER] INTEGER NOT NULL,  UNIQUE([EMID], [WID]));");
        }

        public bool AddExamResult(ExamType type, List<long> wordBookSelIds, List<WordModel> examWords, Dictionary<WordModel, FailureModel> failureWords)
        {
            long id = long.MinValue;
            object value;
            bool result = false;
            if (dapper.ExecuteScalarSQL("INSERT INTO [EXAM_MAIN] (TYPE,TOTAL,WRONG_COUNT,CORRECT_RATE) VALUES (@TYPE,@TOTAL,@WRONG_COUNT,@CORRECT_RATE);SELECT LAST_INSERT_ROWID();"
                , out value
                , new
                {
                    TYPE = type.ToString(),
                    TOTAL = examWords.Count,
                    WRONG_COUNT = failureWords.Count,
                    CORRECT_RATE = CommonUtil.GetCorrectRate(examWords.Count, failureWords.Count)
                }))
            {
                if (value is long)
                {
                    id = (long)value;
                    List<long> examIds = examWords.Select(item => item.Id.Value).ToList();
                    foreach (long wbId in wordBookSelIds)
                    {
                        AddExamWordbook(id, wbId, examIds, failureWords);
                    }

                    foreach (var failureWord in failureWords)
                    {
                        AddExamWrongWord(id, failureWord.Key, failureWord.Value);
                    }
                    UpdateExamCount(examIds);
                    result = true;
                }
            }
            return result;
        }
        private bool AddExamWordbook(long emId, long wbId, List<long> wordIds, Dictionary<WordModel, FailureModel> wrongWords)
        {
            object value;
            bool result = false;
            List<long> bWordIds = new List<long>();
            GetBookWordCount(wbId, wordIds, out bWordIds);
            int bookTotal = bWordIds.Count;
            int bookWrongCount = 0;
            foreach (var wrongWord in wrongWords)
            {
                if (bWordIds.Contains(wrongWord.Key.Id.Value))
                {
                    bookWrongCount++;
                }
            }
            if (dapper.ExecuteScalarSQL("INSERT INTO [EXAM_WORDBOOK] (EMID,WBID,WORDS,TOTAL,WRONG_COUNT,CORRECT_RATE) VALUES (@EMID,@WBID,@WORDS,@TOTAL,@WRONG_COUNT,@CORRECT_RATE);"
                , out value
                , new
                {
                    EMID = emId,
                    WBID = wbId,
                    WORDS = string.Join(",", bWordIds.ToArray()),
                    TOTAL = bookTotal,
                    WRONG_COUNT = bookWrongCount,
                    CORRECT_RATE = CommonUtil.GetCorrectRate(bookTotal, bookWrongCount)
                }))
            {
                result = true;
            }
            return result;
        }

        private bool AddExamWrongWord(long emId, WordModel failureWord, FailureModel failureModel)
        {
            bool result = false;
            object value;

            if (dapper.ExecuteScalarSQL("INSERT INTO [EXAM_WRONG_WORD] (EMID,WID,FIRST_ERROR,ERROR_COUNT,IS_SHOW_ANSWER) VALUES (@EMID,@WID,@FIRST_ERROR,@ERROR_COUNT,@IS_SHOW_ANSWER);"
                , out value
                , new { EMID = emId, WID = failureWord.Id.Value, FIRST_ERROR = failureModel.FirstErrorAnswer, ERROR_COUNT = failureModel.FailureCount, IS_SHOW_ANSWER = failureModel.ShowAnswer }))
            {
                result = true;
            }
            return result;
        }

        public bool GetChoiceItems(long id, out List<ExamChoiceModel> examChoices)
        {
            examChoices = new List<Model.ExamChoiceModel>();
            bool result = false;
            try
            {
                object value;
                if (dapper.GetModel<ExamChoiceModel>(@"SELECT B.ID AS WID,B.WORD,A.ID,A.EXPLANATION FROM 
                    (SELECT ID,WID,EXPLANATION FROM EXPLANATION WHERE  SORT = 0 AND WID = @ID
                    UNION ALL 
                    SELECT * FROM (SELECT ID,WID,EXPLANATION FROM EXPLANATION WHERE SORT = 0 AND WID != @ID ORDER BY RANDOM() LIMIT 3))
                     A LEFT JOIN WORD B 
                    ON A.WID = B.ID
                    ORDER BY RANDOM()", out value, new { ID = id }))
                {
                    examChoices = value as List<ExamChoiceModel>;
                    int totla = examChoices.Count;
                    for (int i = 4; i > totla; i--)
                    {
                        examChoices.Add(new ExamChoiceModel() { WId = -1, Word = "[Shortage]", Id = -1, Explanation = "[Shortage]" });
                    }
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }


        #endregion

        #region WordbookMapping

        private bool SyncWordBookMappingWord(long wId, List<long> wordBookSelId, bool isAdd)
        {
            bool result = false;
            try
            {
                if (dapper.ExecuteSQL("DELETE FROM WORDBOOK_WORD WHERE WID = @WID;", new { WID = wId }))
                {
                    dapper.ExecuteSQL("VACUUM;");

                    var data = wordBookSelId.Select(item => new { WID = wId, WBID = item });
                    if (dapper.ExecuteSQL("INSERT INTO WORDBOOK_WORD (WBID,WID) VALUES (@WBID,@WID);", data))
                    {
                        var record = wordBookSelId.Select(item => new
                        {
                            TableName = ChangeTableName.Wordbook_Word,
                            ChangeType = ChangeType.New,
                            Id1 = item,
                            Id2 = wId
                        });
                        dapper.ExecuteSQL("INSERT INTO CHANGE_RECORD VALUES (@TableName,@ChangeType,@Id1,@Id2)", record);
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool AddWordbookMapping(long wordbookId, long wordId)
        {
            return dapper.ExecuteSQL("INSERT INTO WORDBOOK_WORD (WBID,WID) VALUES (@WBID,@WID);", new
            {
                WBID = wordbookId,
                WID = wordId
            });
        }

        public bool RemoveWordbookMapping(long? wordbookId, long? wordId)
        {
            List<string> whereList = new List<string>();
            if (wordbookId != null)
            {
                whereList.Add(" WBID=@WBID ");
            }
            if (wordId != null)
            {
                whereList.Add("  WID=@WID ");
            }
            string whereStr = string.Empty;
            if (whereList.Count > 0)
            {
                whereStr = " WHERE " + string.Join("AND", whereList.ToArray());
            }
            if (dapper.ExecuteSQL("DELETE FROM WORDBOOK_WORD " + whereStr, new
            {
                WBID = wordbookId == null ? string.Empty : wordbookId.Value.ToString(),
                WID = wordId == null ? string.Empty : wordId.Value.ToString()
            }))
            {
                dapper.ExecuteSQL("VACUUM;");
                return true;
            }
            return false;
        }


        public bool GetWordbookMapping(long? wordbookId, long? wordId, out List<WordBookMapping> wordbookMapping)
        {
            bool result = false;
            wordbookMapping = new List<WordBookMapping>();
            try
            {
                List<string> whereList = new List<string>();
                if (wordbookId != null)
                {
                    whereList.Add(" WBID=@WBID ");
                }
                if (wordId != null)
                {
                    whereList.Add("  WID=@WID ");
                }
                string whereStr = string.Empty;
                if (whereList.Count > 0)
                {
                    whereStr = " WHERE " + string.Join("AND", whereList.ToArray());
                }
                object value;
                if (dapper.GetModel<WordBookMapping>("SELECT WBID AS WordbookId,WID AS WordId FROM WORDBOOK_WORD " + whereStr, out value, new
                {
                    WBID = wordbookId == null ? string.Empty : wordbookId.Value.ToString(),
                    WID = wordId == null ? string.Empty : wordId.Value.ToString()
                }))
                {
                    wordbookMapping = value as List<WordBookMapping>;
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        #endregion

       


        public bool Dispose()
        {
            return dapper.Dispose();
        }

    }
}
