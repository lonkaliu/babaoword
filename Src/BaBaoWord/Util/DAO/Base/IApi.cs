﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BaBaoWord.Model;
using BaBaoWord.Category;

namespace BaBaoWord.Util
{
    internal interface IApi
    {
        bool CreateDB();

        #region WordBook
        bool GetWordBooks(List<long> wordBookIds, out List<WordBookModel> wordBooks);

        bool RemoveWordBook(long Id);

        bool AddWordBook(WordBookModel wordBook, out long id);

        bool EditWordBook(WordBookModel wordBook);

        bool GetWordBookIds(long wordId, out List<long> wordBookIds);

        bool GetWordBooks(string word, out List<SearchWordInWordBookModel> wordBook);


        #endregion

        #region WordRead

        bool GetWords(List<long> wordbookIds, out List<WordModel> words);

        bool GetWords(List<long> wordbookIds, out int count);

        bool GetReadWords(List<long> wordbookIds, out List<WordModel> words);

        bool GetUnReadWords(List<long> wordbookIds, out List<WordModel> words);

        bool SaveReadWord(List<long> wordbookIds, long wId);

        bool ReturnAllUnRead(List<long> wordbookIds);

        bool GetAllReadData(out DataTable data);
        bool SaveAllReadData(DataTable data);

        #endregion

        #region Word
        bool GetWord(long Id, out WordModel resultWord);

        bool GetWords(List<long> Ids, string word, out List<WordModel> resultWords, bool onlyWord = false,bool isAll = true);

        bool AddWord(WordModel word, List<long> wordBookSelId, out long id);

        bool EditWord(WordModel word, List<long> wordBookSelId);

        bool RemoveWord(long Id);

        bool IncreaseForget(long id);
        bool ReturnForgetZero(long id);
        bool GetForget(long id, out int count);
        bool SetForget(long id, int count);

        bool GetExportWords(List<long> wordbookIds, out List<WordModel> words);

        bool CheckWordDuplicate(string word, List<long> wordBookSelId, out DataTable duplicateData);
       #endregion

        #region Wordbook Mapping
        bool AddWordbookMapping(long wordbookId, long wordId);

        bool RemoveWordbookMapping(long? wordbookId, long? wordId);

        bool GetWordbookMapping(long? wordbookId, long? wordId, out List<WordBookMapping> wordbookMapping);

        #endregion

        #region Exam

        bool GetChoiceItems(long id,out List<ExamChoiceModel> examChoices);
        bool AddExamResult(ExamType type, List<long> wordBookSelIds, List<WordModel> examWords, Dictionary<WordModel, FailureModel> failureWords);
        #endregion

        #region Setting
        bool SaveSetting(List<SettingModel> settings);

        bool GetSetting(string category, string key, out List<SettingModel> settings);

        #endregion

        bool Dispose();


        
    }
}
