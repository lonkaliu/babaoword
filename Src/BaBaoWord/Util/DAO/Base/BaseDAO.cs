﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BaBaoWord.Model;
using BaBaoWord.Category;

namespace BaBaoWord.Util
{
    internal class BaseDao
    {
        private volatile static IApi _daoFactory;
        private static IApi DaoFactory
        {
            get
            {
                lock (ms_lockObject)
                {
                    if (_daoFactory == null)
                    {
                        switch (Parameter.GetInstance().DaoType)
                        {
                            case DaoType.DB:
                            case DaoType.Web:
                            case DaoType.Test:
                                break;
                            case DaoType.SQlite:
#if DEBUG
                                //TODO 密碼要拿掉
                                _daoFactory = new SQLiteApi(string.Format("Data Source={0};Version=3;", CommonUtil.GetFilePath("data.sqlite")));
#else
                                //_daoFactory = new SQLiteApi(string.Format("Data Source={0};Version=3;", CommonUtil.GetFilePath("data.sqlite")));
                                _daoFactory = new SQLiteApi(string.Format("Data Source={0};Version=3;Password=!QAZ2wsx;", CommonUtil.GetFilePath("data.sqlite")));
#endif
                                break;
                        }
                    }
                }
                return _daoFactory;
            }
            set
            {
                _daoFactory = value;
            }
        }

        private static readonly object ms_lockObject = new object();

        public static bool CreateDB()
        {
            return DaoFactory.CreateDB();
        }


        #region Wordbook
        public static bool GetWordBooks(List<long> wordBookIds, out List<WordBookModel> wordBooks)
        {
            return DaoFactory.GetWordBooks(wordBookIds, out wordBooks);
        }

        public static bool AddWordBook(WordBookModel wordBook, out long id)
        {
            return DaoFactory.AddWordBook(wordBook, out id);
        }

        public static bool EditWordBook(WordBookModel wordBook)
        {
            return DaoFactory.EditWordBook(wordBook);
        }

        public static bool RemoveWordBook(long Id)
        {
            return DaoFactory.RemoveWordBook(Id);
        }
        public static bool GetWordBookIds(long wordId, out List<long> wordBookIds)
        {
            return DaoFactory.GetWordBookIds(wordId, out wordBookIds);
        }

        public static bool GetWordBooks(string word, out List<SearchWordInWordBookModel> wordBook)
        {
            return DaoFactory.GetWordBooks(word, out wordBook);
        }

        public static bool GetWord(List<long> wordbookIds, out int count)
        {
            return DaoFactory.GetWords(wordbookIds, out count);
        }
        #endregion

        #region Word
        public static bool AddWord(WordModel word, List<long> wordBookSelId, out long id)
        {
            return DaoFactory.AddWord(word, wordBookSelId, out id);
        }

        public static bool EditWord(WordModel word, List<long> wordBookSelId)
        {
            return DaoFactory.EditWord(word, wordBookSelId);
        }

        public static bool RemoveWord(long Id)
        {
            return DaoFactory.RemoveWord(Id);
        }

        public static bool GetWord(long Id, out WordModel resultWord)
        {
            return DaoFactory.GetWord(Id, out resultWord);
        }

        public static bool GetWords(List<long> Ids, string word, out List<WordModel> resultWords, bool onlyWord = false, bool isAll = true)
        {
            return DaoFactory.GetWords(Ids, word, out resultWords, onlyWord, isAll);
        }

        public static bool IncreaseForget(long id)
        {
            return DaoFactory.IncreaseForget(id);
        }
        public static bool ReturnForgetZero(long id)
        {
            return DaoFactory.ReturnForgetZero(id);
        }
        public static bool GetForget(long id, out int count)
        {
            return DaoFactory.GetForget(id, out count);
        }
        public static bool SetForget(long id, int count)
        {
            return DaoFactory.SetForget(id, count);
        }

        public static bool GetExportWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return DaoFactory.GetExportWords(wordbookIds, out words);
        }

        public static bool CheckWordDuplicate(string word, List<long> wordBookSelId, out DataTable duplicateData)
        {
            return DaoFactory.CheckWordDuplicate(word, wordBookSelId, out duplicateData);
        }
        #endregion

        #region WordRead
        public static bool GetWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return DaoFactory.GetWords(wordbookIds, out words);
        }
        public static bool GetUnReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return DaoFactory.GetUnReadWords(wordbookIds, out words);
        }
        public static bool GetReadWords(List<long> wordbookIds, out List<WordModel> words)
        {
            return DaoFactory.GetReadWords(wordbookIds, out words);
        }

        public static bool SaveReadWord(List<long> wordbookIds, long wId)
        {
            return DaoFactory.SaveReadWord(wordbookIds, wId);
        }

        public static bool ReturnAllUnRead(List<long> wordbookIds)
        {
            return DaoFactory.ReturnAllUnRead(wordbookIds);
        }

        public static bool GetAllReadData(out DataTable data)
        {
            return DaoFactory.GetAllReadData(out data);
        }
        public static bool SaveAllReadData(DataTable data)
        {
            return DaoFactory.SaveAllReadData(data);
        }

        #endregion

        #region Exam

        public static bool GetChoiceItems(long id,out List<ExamChoiceModel> examChoices)
        {
            return DaoFactory.GetChoiceItems(id,out examChoices);
        }

        public static bool AddExamResult(ExamType type, List<long> wordBookSelIds, List<WordModel> examWords, Dictionary<WordModel, FailureModel> failureWords)
        {
            return DaoFactory.AddExamResult(type, wordBookSelIds, examWords, failureWords);
        }

        #endregion

        #region Setting
        public static bool SaveSetting(List<SettingModel> settings)
        {
            return DaoFactory.SaveSetting(settings);
        }

        public static bool GetSetting(string category, string key, out List<SettingModel> settings)
        {
            return DaoFactory.GetSetting(category, key, out settings);
        }
        #endregion

        #region Wordbook Mapping
        public static bool AddWordbookMapping(long wordbookId, long wordId)
        {
            return DaoFactory.AddWordbookMapping(wordbookId, wordId);
        }

        public static bool RemoveWordbookMapping(long? wordbookId, long? wordId)
        {
            return DaoFactory.RemoveWordbookMapping(wordbookId, wordId);
        }

        public static bool GetWordbookMapping(long? wordbookId, long? wordId, out List<WordBookMapping> wordbookMapping)
        {
            return DaoFactory.GetWordbookMapping(wordbookId, wordId, out wordbookMapping);
        }
        #endregion

        public static bool Dispose()
        {
            _daoFactory = null;
            return DaoFactory.Dispose();
        }
    }
}
