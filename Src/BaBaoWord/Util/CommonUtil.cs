﻿using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.View;
using BaBaoWord.ViewModel;
using System;
using System.IO;
using System.Windows;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Linq;
using BaBaoWord.Category;

namespace BaBaoWord.Util
{
    internal class CommonUtil
    {
        public static bool IsValidEmail(string strIn)
        {
            bool result = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None);
                result = true;
            }
            catch (Exception)
            {
                return false;
            }

            if (!result)
                return false;
            result = false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                result = Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase);
            }
            catch (Exception)
            {

            }
            return result;
        }

        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
            }
            return match.Groups[1].Value + domainName;
        }
        public static string ReplaceStr(string str)
        {
            return str.Replace("&quot;", string.Empty).Trim();
        }
        public static string GetFilePath(string fileName)
        {
            string filePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, fileName);
            string fileFullPath = Path.GetFullPath(filePath);
            return fileFullPath;
        }
        public static string GetResourceString(string str, bool isOriginal = false)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    result = (string)Application.Current.Resources[str];
                    if (result == null)
                    {
                        if (isOriginal)
                        {
                            result = str;
                        }
                        else
                        {
                            result = "Can't Find Resources Key 「" + str + "」";
                        }
                    }
                }
                catch
                {
                    result = "Can't Find Resources Key 「" + str + "」";
                }
            }
            return result;
        }

        public static object GetPageParam(string pageKey, string key)
        {
            object value = null;
            if (Parameter.GetInstance().PageParams.ContainsKey(pageKey)
                 && Parameter.GetInstance().PageParams[pageKey] != null
                && Parameter.GetInstance().PageParams[pageKey].ContainsKey(key))
            {
                value = Parameter.GetInstance().PageParams[pageKey][key];
            }
            return value;
        }
        public static bool SetPageParam(string pageKey, string key, object value)
        {
            bool result = false;
            if (!Parameter.GetInstance().PageParams.ContainsKey(pageKey))
            {
                Parameter.GetInstance().PageParams[pageKey] = new Dictionary<string, object>();
            }
            Parameter.GetInstance().PageParams[pageKey][key] = value;
            result = true;
            return result;
        }

        public static bool RemovePageParam(string pageKey, string key)
        {
            bool result = false;
            if (Parameter.GetInstance().PageParams.ContainsKey(pageKey)
                 && Parameter.GetInstance().PageParams[pageKey] != null
                && Parameter.GetInstance().PageParams[pageKey].ContainsKey(key))
            {

                Parameter.GetInstance().PageParams[pageKey].Remove(key);
            }
            result = true;
            return result;
        }

        public static bool RemoveAllPageParam()
        {
            bool result = false;
            try
            {
                RemovePageParam(Parameter.WordBookRootKey, "WordBookSelIds");
                RemovePageParam(Parameter.ForgetPasswordRootKey, "UserInfo");
                RemovePageParam(Parameter.NewAccountRootKey, "UserInfo");
                RemovePageParam(Parameter.EditWordRootKey, "WordId");
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public static DateTime CombineDateTime(DateTime date, string time)
        {
            DateTime result;
            if (!DateTime.TryParse(date.ToString("yyyy/MM/dd") + " " + time.Substring(0, 2) + ":" + time.Substring(2, 2) + ":00", out result))
            {
                result = DateTime.MinValue;
            }
            return result;
        }
        public static bool ValidationTime(string time)
        {
            if (DateTime.MinValue == CombineDateTime(DateTime.Now, time.PadLeft(4, '0')))
            {
                return false;
            }
            return true;
        }

        public static bool GetSound(string soundStr)
        {
            bool result = false;
            try
            {
                string[] tokens = soundStr.Replace(",Real", string.Empty).Split("-".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string token in tokens)
                {
                    if (!File.Exists(Path.Combine(Parameter.GetInstance().Mp3Directory, token + ".mp3")))
                    {
                        YahooTranslator.GetSound(token, true);
                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public static double GetCorrectRate(int total, int wrongCount)
        {
            if (total == 0)
            {
                return 0;
            }
            else
            {
                return (Math.Round((total - wrongCount) / double.Parse(total.ToString()), 2)) * 100;
            }
        }

        public static WordSortType GetSortByModel()
        {
            WordSortType result = WordSortType.ByWeight;
            if (Parameter.GetInstance().SortBy != null)
            {
                result = Parameter.GetInstance().SortBy.Value;
            }
            else
            {
                List<SettingModel> settings;
                if (new DataAccess.SettingRespository().GetSetting("WordBook", "SortBy", out settings) && settings.Count > 0)
                {
                    result = (WordSortType)Enum.Parse(typeof(WordSortType), settings[0].Value);
                    Parameter.GetInstance().SortBy = result;
                }
            }
            return result;
        }

        public static List<WordFilterModel> GetFilterModel()
        {
            System.Collections.ObjectModel.ObservableCollection<WordFilterModel> temp = new System.Collections.ObjectModel.ObservableCollection<Model.WordFilterModel>();
            if (Parameter.GetInstance().WordFilters != null)
            {
                temp = Parameter.GetInstance().WordFilters;
            }
            else
            {
                List<SettingModel> settings;
                if (new DataAccess.SettingRespository().GetSetting("WordBook", "WordFilters", out settings) && settings.Count > 0)
                {
                    temp = temp.Deserialize<WordFilterModel>(settings[0].Value);
                    Parameter.GetInstance().WordFilters = temp;
                }
            }
            List<WordFilterModel> result = new List<Model.WordFilterModel>();
            if (temp .Count > 0)
            {
                foreach(var item in temp)
                {
                    result.Add(item);
                }
            }
            return result;
        }
        #region Message
        public static void ShowProcessMainWindowShadow(string msg)
        {
            ProcessModel processModel = new ProcessModel();
            processModel.IsProcess = true;
            processModel.Message = msg;
            PubSub<object>.RaiseEvent(Parameter.ProcessControlEventKey, null, new PubSubEventArgs<object>(processModel));
        }

        public static void HighProcessMainWindowShadow()
        {
            ProcessModel processModel = new ProcessModel();
            processModel.IsProcess = false;
            PubSub<object>.RaiseEvent(Parameter.ProcessControlEventKey, null, new PubSubEventArgs<object>(processModel));
        }

        public static void HideMainWindowShadow()
        {
            PubSub<object>.RaiseEvent(Parameter.HighShadowEventKey, null, new PubSubEventArgs<object>(false));
        }

        public static void ShowMainWindowShadow()
        {
            PubSub<object>.RaiseEvent(Parameter.HighShadowEventKey, null, new PubSubEventArgs<object>(true));
        }


        public static void ShowMessageBox(string msg, string title = "")
        {
            MessageAlert ma = new MessageAlert();
            ma.DataContext = new MessageAlertViewModel()
            {
                Msg = msg
            };
            if (!string.IsNullOrEmpty(title))
            {
                ma.Title = title;
            }
            ma.ShowDialog();
        }

        public static void ShowMessageBoxWithShadow(string msg, string title = "")
        {
            ShowMainWindowShadow();
            MessageAlert ma = new MessageAlert();
            ma.DataContext = new MessageAlertViewModel()
            {
                Msg = msg
            };
            if (!string.IsNullOrEmpty(title))
            {
                ma.Title = title;
            }
            ma.ShowDialog();
            HideMainWindowShadow();
        }
        public static bool ShowConfirmMessageBox(string msg, string title = "")
        {
            bool result = false;
            try
            {
                MessageConfirm mc = new MessageConfirm();
                mc.DataContext = new MessageConfirmViewModel()
                {
                    Msg = msg
                };
                if (!string.IsNullOrEmpty(title))
                {
                    mc.Title = title;
                }
                if (mc.ShowDialog().Value)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }
        public static bool ShowConfirmMessageBoxWithShadow(string msg, string title = "")
        {
            bool result = false;
            try
            {
                ShowMainWindowShadow();
                MessageConfirm mc = new MessageConfirm();
                mc.DataContext = new MessageConfirmViewModel()
                {
                    Msg = msg
                };
                if (!string.IsNullOrEmpty(title))
                {
                    mc.Title = title;
                }
                if (mc.ShowDialog().Value)
                {
                    result = true;
                }
                HideMainWindowShadow();
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public static string ParseMsgStr(string msg)
        {
            string result = string.Empty;
            string tempResult = string.Empty;
            string[] lines = msg.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                string[] tokens = line.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length > 0)
                {
                    tempResult = GetResourceString(tokens[0]);
                }
                List<string> param = new List<string>();
                for (int j = 1; j < tokens.Length; j++)
                {
                    param.Add(GetResourceString(tokens[j], true));
                }
                tempResult = string.Format(tempResult, param.ToArray());
                result += tempResult;
                if (i < lines.Length - 1)
                {
                    result += Environment.NewLine;
                }
            }
            return result;
        }

        public static string MsgAlreadyInStr(string word, string wordBook)
        {
            return "WordAlreadyInBook," + word + "," + wordBook;
        }

        public static string MsgBlankStr(string field)
        {
            return "CannotBeBlank," + field;
        }
        public static string MsgAddItemStr(string field)
        {
            return "AddItemFirst," + field;
        }

        public static string MsgItemBlankStr(string field, int index)
        {
            return "ItemCannotBeBlank," + field + "," + index;
        }

        public static string MsgSuccessStr(string processType)
        {
            return "Success," + processType;
        }

        public static string MsgFailureStr(string processType)
        {
            return "Failure," + processType;
        }

        public static string MsgSettingErrStr(string time)
        {
            return "SettingError," + time;
        }

        public static string MsgIncorrectFieldStr(string field)
        {
            return "IncorrectField," + field;
        }
        #endregion

        #region File

        public static BaseService.ResultData SyncFile(bool onlySyncBack = false)
        {
            int selfVersion = int.MinValue;
            int dbVersion = int.MinValue;
            BaBaoWord.DataAccess.SettingRespository settingDao = new DataAccess.SettingRespository();
            System.Collections.Generic.List<SettingModel> settings;
            if (settingDao.GetSetting("System", "Version", out settings))
            {
                if (!int.TryParse(settings[0].Value, out selfVersion))
                {
                    selfVersion = int.MinValue;
                }
            }

            var result = WebServiceDao.GetVersion(Parameter.GetInstance().UserInfo.Token, Parameter.GetInstance().UserInfo.Id.Value);
            if (result.Result)
            {
                if (!int.TryParse(result.Values["Version"].ToString(), out dbVersion))
                {
                    dbVersion = int.MinValue;
                }
            }
            result = new BaseService.ResultData();

            string exception;
            if (selfVersion > 0 && dbVersion > 0)
            {
                //如果本機跟遠端的版號一樣就可以直接更新本機的上遠端，並把雙方的版號加1
                if (selfVersion == dbVersion && !onlySyncBack)
                {
                    if (!UploadFile(selfVersion, out exception))
                    {
                        result.Exception = exception;
                    }
                    else
                    {
                        SyncSetting();
                        SyncRead();
                        result.Result = true;
                    }
                }
                //如果本機的版號小於遠端
                else if (selfVersion < dbVersion)
                {
                    //先把本機的刪掉後下載遠端的資料
                    if (!DownloadFile(dbVersion, out exception))
                    {
                        result.Exception = exception;
                        return result;
                    }

                    SyncSetting();
                    SyncRead();
                    new DataAccess.SettingRespository().SaveSetting(new List<SettingModel>() { new SettingModel() { Category = "System", Key = "Version", Value = dbVersion.ToString() } });
                    result.Result = true;
                }
                try
                {
                    System.IO.File.Delete(CommonUtil.GetFilePath("data_bk.sqlite"));
                }
                catch (Exception e)
                {
                    Log.WriteLog(LogLevel.Error, e);
                }
            }
            return result;
        }


        private static void SyncRead()
        {
            var baseBkDao = new BaseDaoInstance("data_bk.sqlite");
            System.Data.DataTable data;
            if (baseBkDao.GetAllReadData(out data))
            {
                BaseDao.SaveAllReadData(data);
            }
            baseBkDao.Dispose();
        }

        private static void SyncSetting()
        {
            var baseBkDao = new BaseDaoInstance("data_bk.sqlite");
            List<SettingModel> settingValue;
            if (baseBkDao.GetSetting(string.Empty, string.Empty, out settingValue))
            {
                BaseDao.SaveSetting(settingValue);
            }
            baseBkDao.Dispose();
        }

        private static bool DownloadFile(int version, out string exception)
        {
            bool resultMethod = false;
            exception = string.Empty;
            try
            {
                var result = WebServiceDao.DownloadFile(Parameter.GetInstance().UserInfo.Token, Parameter.GetInstance().UserInfo.User_Guid);
                resultMethod = result.Result;
                if (!result.Result)
                {
                    exception = result.Exception;
                }


            }
            catch (Exception e)
            {
                exception = e.Message;
            }
            return resultMethod;
        }

        private static bool UploadFile(int version, out string exception)
        {
            bool resultMethod = false;
            exception = string.Empty;
            try
            {
                int newVersion = version + 1;
                var result = WebServiceDao.SetVersion(Parameter.GetInstance().UserInfo.Token, Parameter.GetInstance().UserInfo.Id.Value, newVersion);
                if (result.Result)
                {
                    BaBaoWord.DataAccess.SettingRespository settingDao = new DataAccess.SettingRespository();
                    if (settingDao.SaveSetting(new List<SettingModel>() { new SettingModel() { Category = "System", Key = "Version", Value = newVersion.ToString() } }))
                    {
                        //BaseDao.RemoveChangeRecord(new ChangeRecordModel());
                        BaseDao.ReturnAllUnRead(null);
                        var fileResult = WebServiceDao.UploadFile(Parameter.GetInstance().UserInfo.Token, Parameter.GetInstance().UserInfo.User_Guid);
                        resultMethod = fileResult.Result;
                        if (!fileResult.Result)
                        {
                            exception = fileResult.Exception;
                        }
                    }
                }
                else
                {
                    exception = result.Exception;
                }
            }
            catch (Exception e)
            {
                exception = e.Message;
            }
            return resultMethod;
        }

        public static bool DecryptData()
        {
            bool result = false;
            try
            {
                string path = string.Format("Data Source={0};Version=3;Password=!QAZ2wsx;", CommonUtil.GetFilePath("data.sqlite"));

                DapperFactory sqlite = new DapperFactory(path, out result);
                if (result)
                {
                    result = sqlite.DecryptData();
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e.Message);
            }
            return result;
        }
        #endregion

    }
}
