﻿using System.IO;
using System.Collections.Generic;
using BaBaoWord.Category;
using System.Collections.ObjectModel;
using BaBaoWord.Model;

namespace BaBaoWord.Util
{
    internal class Parameter
    {
        private Parameter()
        {
        }

        public BaBaoWord.Model.UserInfoModel UserInfo { get; set; }

        public string ExportPath { get; set; }

        #region Const Parameter

        #region ICON

        public const string IconPath = "/Resources/icon/";
        public const string AddImg = IconPath + "ic_action_add.png";
        public const string EditAddImg = IconPath + "ic_edit_add.png";
        public const string EditEditImg = IconPath + "ic_edit_edit.png";
        public const string EditEnhanceImg = IconPath + "ic_edit_enhance.png";
        public const string EditRemoveImg = IconPath + "ic_edit_remove.png";
        public const string EditStopImg = IconPath + "ic_edit_stop.png";
        public const string EditSearchImg = IconPath + "ic_edit_search.png";
        public const string EditSearch2Img = IconPath + "ic_edit_search2.png";
        public const string EditSettingImg = IconPath + "ic_edit_setting.png";
        public const string EditTrashImg = IconPath + "ic_edit_trash.png";
        public const string EditRefreshImg = IconPath + "ic_edit_refresh.png";
        public const string EditOkImg = IconPath + "ic_edit_ok.png";
        public const string EditCancelImg = IconPath + "ic_edit_cancel.png";
        public const string EditClearImg = IconPath + "ic_edit_clear.png";
        public const string EditSaveImg = IconPath + "ic_edit_save.png";
        public const string RemoveImg = IconPath + "ic_action_remove.png";
        public const string YahooImg = IconPath + "yahoo_button.png";
        public const string EditSoundImg = IconPath + "ic_edit_sound.png";
        public const string SoundImg = IconPath + "ic_action_sound.png";
        public const string FindSoundImg = IconPath + "ic_action_download_music.png";
        public const string BookImg = IconPath + "ic_action_book.png";
        public const string ExpandImg = IconPath + "ic_action_expand.png";
        public const string CollapseImg = IconPath + "ic_action_collapse.png";
        public const string ExpandAllImg = IconPath + "ic_action_expand_all.png";
        public const string CollapseAllImg = IconPath + "ic_action_collapse_all.png";
        public const string EditImg = IconPath + "ic_action_edit.png";
        public const string SubmitImg = IconPath + "ic_edit_submit.png";
        public const string QuestionImg = IconPath + "ic_edit_question.png";
        public const string Question2Img = IconPath + "ic_edit_question2.png";
        public const string EditPauseImg = IconPath + "ic_edit_pause.png";
        public const string EditStop2Img = IconPath + "ic_edit_stop2.png";
        public const string EditStartImg = IconPath + "ic_edit_start.png";
        public const string ExcelDownloadImg = IconPath + "ic_action_excel_download.png";
        public const string ExcelImg = IconPath + "ic_action_excel.png";
        public const string TipImg = IconPath + "ic_action_tip.png";

        #endregion

        #region Sound

        public const string SoundPath = "Resources/sound/";
        public const string RotateShowWordSound = SoundPath + "Word_Rotate_Show.mp3";
        public const string SuccessSound = SoundPath + "Success.wav";
        public const string FailureSound = SoundPath + "Failure.wav";

        #endregion
        public string Mp3TempDirectory { get; set; }


        public string Mp3Directory { get; set; }

        public DaoType DaoType
        {
            get
            {
                return DaoType.SQlite;
            }
        }

        public WordSortType? SortBy { get; set; }

        public ObservableCollection<WordFilterModel> WordFilters { get; set; }

        private Dictionary<string, Dictionary<string, object>> _pageParams = new Dictionary<string, Dictionary<string, object>>();
        public Dictionary<string, Dictionary<string, object>> PageParams
        {
            get
            {
                return _pageParams;
            }
            set
            {
                _pageParams = value;
            }
        }
        #endregion

        #region RootKey

        public const string NewWordRootKey = "NewWord";
        public const string EditWordRootKey = "EditWord";
        public const string HomeRootKey = "Home";
        public const string WordBookRootKey = "WordBook";
        public const string ExamRootKey = "Exam";
        public const string ExamResultRootKey = "ExamResult";
        public const string AboutRootKey = "About";
        public const string LoginRootKey = "Login";
        public const string UserProfileRootKey = "UserProfile";
        public const string NewAccountRootKey = "NewAccount";
        public const string ValidationCodeRootKey = "ValidationCode";
        public const string ForgetPasswordRootKey = "ForgetPassword";

        #endregion

        #region EventKey

        public const string RefreshNameEventKey = "RefreshName";
        public const string MenuControlEventKey = "MenuControl";
        public const string ProcessControlEventKey = "ProcessControl";
        public const string RefreshWordBookListEventKey = "RefreshWordBookList";
        public const string BannerControlEventKey = "BannerControl";
        public const string ChangeWordBookContentEventKey = "ChangeWordBookContent";
        public const string HighShadowEventKey = "HighShadow";
        public const string ChangeWordEventKey = "ChangeWord";
        public const string ReloadWordBookWordsEventKey = "ReloadWordBookWords";
        public const string CloseRotateWordEventKey = "CloseRotateWord";
        public const string RefreshMenuEventKey = "RefreshMenu";
        public const string CloseExamWordEventKey = "CloseExamWord";
        public const string NextExamWordEventKey = "NextExamWord";
        public const string NextRotateWordEventKey = "NextRotateWord";
        public const string PreviousRotateWordEventKey = "PreviousRotateWord";
        public const string PauseRotateEventKey = "PauseRotate";
        
        #endregion

        #region Singleton Patten
        private volatile static Parameter Instance;
        private static readonly object ms_lockObject = new object();
        public static Parameter GetInstance()
        {
            if (Instance == null)
            {
                lock (ms_lockObject)
                {
                    if (Instance == null)
                    {
                        Instance = new Parameter();
                    }
                }
            }
            return Instance;
        }
        #endregion

    }
}
