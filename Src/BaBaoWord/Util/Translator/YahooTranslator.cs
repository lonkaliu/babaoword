﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using BaBaoWord.Model;
using BaBaoWord.MVVM;
using BaBaoWord.Category;

namespace BaBaoWord.Util
{
    internal class YahooTranslator
    {
        private static string translatorUrl = "https://tw.dictionary.yahoo.com/dictionary?p=";
        /// <summary>
        /// 翻譯
        /// </summary>
        /// <param name="word">要翻的單字</param>
        /// <param name="tWrod">結果</param>
        /// <returns></returns>
        public static bool Translator(string word, out WordModel result)
        {
            result = new WordModel();
            bool bResult = false;
            try
            {
                HtmlDocument content;
                if (HtmlHelper.GetHtemlContent(translatorUrl + word.Replace(" ", "+"), out content))
                {
                    result.Word = word;

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(content.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/ol[1]/li[1]/div[1]/div[1]").InnerHtml);
                    string checkStr = doc.DocumentNode.InnerText;
                    if (checkStr.IndexOf("無法找到符合 " + word + " 的相關結果。請嘗試以下建議或輸入其它關鍵字。") == -1
                        //&& (checkStr.IndexOf("KK") > -1 || checkStr.IndexOf("DJ") > -1)
                        )
                    {
                        doc.LoadHtml(content.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/ol[1]").InnerHtml);
                        HtmlNode pronunciationDoc = doc.DocumentNode.SelectSingleNode("./li[1]/div[1]/div[1]/div[2]/span[1]");
                        result.Pronunciation = new TrulyObservableCollection<PronunciationModel>();
                        GetPronunciation(pronunciationDoc, result);

                        HtmlNode soundDoc = doc.DocumentNode.SelectSingleNode("./li[1]/div[1]/div[1]/div[2]/span[2]");
                        GetSound(soundDoc, result);
                        result.Explanation = new TrulyObservableCollection<ExplanationModel>();
                        GetExplanation(doc, result);
                        bResult = true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return bResult;
        }

        public static bool GetSound(string word, bool IsReal = false)
        {
            bool bResult = false;
            try
            {
                HtmlDocument content;
                if (HtmlHelper.GetHtemlContent(translatorUrl + word.Replace(" ", "+"), out content))
                {
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(content.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/ol[1]/li[1]/div[1]/div[1]").InnerHtml);
                    string checkStr = doc.DocumentNode.InnerText;
                    if (checkStr.IndexOf("無法找到符合 " + word + " 的相關結果。請嘗試以下建議或輸入其它關鍵字。") == -1 && (checkStr.IndexOf("KK") > -1 && checkStr.IndexOf("DJ") > -1))
                    {
                        doc.LoadHtml(content.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/ol[1]").InnerHtml);

                        HtmlNode soundDoc = doc.DocumentNode.SelectSingleNode("./li[1]/div[1]/div[1]/div[2]/span[2]");
                        bResult = GetSound(word, soundDoc, IsReal);
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return bResult;
        }

        private static bool GetSound(string word, HtmlNode soundDoc, bool IsReal = false)
        {
            bool result = false;
            try
            {
                string doc = CommonUtil.ReplaceStr(soundDoc.InnerText);
                string soundTag = doc.Substring(doc.IndexOf("sound_url_1"));
                string mp3Tag = soundTag.Substring(soundTag.IndexOf("mp3"));
                string mp3Http = mp3Tag.Substring(0, mp3Tag.IndexOf("mp3", 3) + 3).Replace("mp3:", string.Empty);
                string mp3LocalPath = string.Empty;
                if (IsReal)
                {
                    mp3LocalPath = Path.Combine(Parameter.GetInstance().Mp3Directory, word + ".mp3");
                }
                else
                {
                    mp3LocalPath = Path.Combine(Parameter.GetInstance().Mp3TempDirectory, word + ".mp3");
                }
                if (!File.Exists(mp3LocalPath))
                {
                    using (WebClient client = new WebClient())
                    {
                        WebProxy proxy = new WebProxy();
                        proxy.UseDefaultCredentials = true;
                        client.Proxy = proxy;
                        client.DownloadFile(mp3Http, mp3LocalPath);
                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        private static void GetExplanation(HtmlDocument doc, WordModel result)
        {
            HtmlNode explanations = doc.DocumentNode.SelectSingleNode("./li[2]/div[1]");
            bool checkApeech = false;
            ExplanationModel wordExplanation = new ExplanationModel();
            int j = 0;
            foreach (HtmlNode explanation in explanations.ChildNodes)
            {
                if (j % 2 == 0)
                {
                    checkApeech = CheckApeech(explanation.InnerText, out wordExplanation);
                    if (!checkApeech)
                    {
                        GetChildExplanation(explanation, wordExplanation, result);
                    }
                }
                else if (checkApeech)
                {
                    GetChildExplanation(explanation, wordExplanation, result);
                }
                j++;
            }
        }

        private static void GetChildExplanation(HtmlNode explanations, ExplanationModel wordExplanation, WordModel result)
        {
            try
            {
                foreach (HtmlNode node in explanations.ChildNodes)
                {
                    ExplanationModel explanationModel = new ExplanationModel();
                    explanationModel.Speech = wordExplanation.Speech;
                    explanationModel.SpeechStr = wordExplanation.SpeechStr;
                    HtmlNode explanation = node.SelectSingleNode("./h4[1]");
                    HtmlNode sentence = node.SelectSingleNode("./span[1]");
                    explanationModel.Explanation = explanation.InnerText;
                    explanationModel.Sentence = sentence == null ? string.Empty : sentence.InnerText;
                    result.Explanation.Add(explanationModel);
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
        }

        private static bool CheckApeech(string speechDoc, out ExplanationModel wordExplanation)
        {
            //TODO 可以拉到父向
            bool result = false;
            wordExplanation = new ExplanationModel();
            try
            {
                foreach (SpeechType speech in Enum.GetValues(typeof(SpeechType)))
                {
                    if (speechDoc.StartsWith(speech.ToString().TrimStart('_')))
                    {
                        result = true;
                        wordExplanation.Speech = speech;
                        wordExplanation.SpeechStr = speechDoc;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        private static void GetSound(HtmlNode soundDoc, WordModel result)
        {
            try
            {
                string doc = CommonUtil.ReplaceStr(soundDoc.InnerText);
                string soundTag = doc.Substring(doc.IndexOf("sound_url_1"));
                string mp3Tag = soundTag.Substring(soundTag.IndexOf("mp3"));
                string mp3Http = mp3Tag.Substring(0, mp3Tag.IndexOf("mp3", 3) + 3).Replace("mp3:", string.Empty);
                string mp3LocalPath = Path.Combine(Parameter.GetInstance().Mp3TempDirectory, result.Word + ".mp3");
                if (!File.Exists(mp3LocalPath))
                {
                    using (WebClient client = new WebClient())
                    {
                        WebProxy proxy = new WebProxy();
                        proxy.UseDefaultCredentials = true;
                        client.Proxy = proxy;
                        client.DownloadFile(mp3Http, mp3LocalPath);
                    }
                }
                result.Sound = result.Word + ",Temp";
                result.IsRemoteSound = true;
                //if (result.Pronunciation != null && result.Pronunciation.Count > 0)
                //{
                //    result.Pronunciation[0].Sound = result.Word+",Temp";
                //    result.Pronunciation[0].IsRemoteSound = true;
                //}
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
        }

        private static void GetPronunciation(HtmlNode pronunciationDoc, WordModel result)
        {
            try
            {
                string[] pronunciations = CommonUtil.ReplaceStr(pronunciationDoc.InnerText).Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < pronunciations.Length; i++)
                {
                    PronunciationModel pronunciation = new PronunciationModel();
                    pronunciation.Pronunciation = pronunciations[i];
                    result.Pronunciation.Add(pronunciation);
                }

            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
        }
    }
}
