﻿using System;
using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BaBaoWord.Control
{
    //http://www.thejoyofcode.com/Animating_when_data_changes.aspx
    //http://www.markodevcic.com/post/Animating_WPF_ContentControl_Part_1/
    //http://www.markodevcic.com/post/Animating_WPF_ContentControl_Part_3/

    public class AnimatedContentControl : ContentControl
    {
        private Storyboard _fadeOut;
        private Storyboard _fadeIn;
        //private ContentControl _old;
        //private ContentControl _current;
        Rectangle _oldContent;
        ContentPresenter _mainArea;

        public AnimatedContentControl()
        {
            InitStoryBoards(false);
            DefaultStyleKey = typeof(AnimatedContentControl);
        }
        private void InitStoryBoards(bool fadeVertical)
        {
            Contract.Ensures(_fadeIn != null, "Failed to init fade in storyboard");
            Contract.Ensures(_fadeOut != null, "Failed to init fade out storyboard");
            _fadeIn = fadeVertical ? (Storyboard)FindResource("fadeInVertical") : (Storyboard)FindResource("fadeIn");
            _fadeOut = fadeVertical ? (Storyboard)FindResource("fadeOutVertical") : (Storyboard)FindResource("fadeOut");
        }

        public override void OnApplyTemplate()
        {

            //_old = (ContentControl)GetTemplateChild("_old");
            //_current = (ContentControl)GetTemplateChild("_current");
            Contract.Ensures(_oldContent != null, "Failed to init paint area");
            Contract.Ensures(_mainArea != null, "Failed to init main area");
            base.OnApplyTemplate();
            _oldContent = (Rectangle)Template.FindName("paintArea", this);
            _mainArea = (ContentPresenter)Template.FindName("mainArea", this);
        }
        protected override void OnContentChanged(object oldContent, object newContent)
        {
            //if (_current != null && _old != null)
            //{
            //    _current.Content = newContent;
            //    _old.Content = oldContent;
            //    _current.Visibility = System.Windows.Visibility.Collapsed;
            //    _old.Visibility = System.Windows.Visibility.Visible;
            //    Storyboard fadeOutClone = _fadeOut.Clone();
            //    Storyboard fadeInClone = _fadeIn.Clone();
            //    //CreateFade(1, 0, _old);
            //    //CreateFade(0, 1, _current);
            //    fadeOutClone.Completed += (s, e) =>
            //        {
            //            fadeInClone.Begin(_current);
            //            _old.Visibility = System.Windows.Visibility.Collapsed;
            //            _current.Visibility = System.Windows.Visibility.Visible;
            //        };
            //    fadeOutClone.Begin(_old);
            //}
            if (oldContent != null && newContent != null)
            {
                _oldContent.Fill = GetVisualBrush(_mainArea);
                _oldContent.Visibility = System.Windows.Visibility.Visible;
                _mainArea.Visibility = System.Windows.Visibility.Collapsed;
                Storyboard fadeOutClone = _fadeOut.Clone();
                Storyboard fadeInClone = _fadeIn.Clone();
                fadeOutClone.Completed += (s, e) =>
                {
                    fadeInClone.Begin(_mainArea);
                    _oldContent.Visibility = System.Windows.Visibility.Collapsed;
                    _mainArea.Visibility = System.Windows.Visibility.Visible;
                };
                fadeOutClone.Begin(_oldContent);
            }

            base.OnContentChanged(oldContent, newContent);
            base.OnContentChanged(oldContent, newContent);
        }

        private Brush GetVisualBrush(Visual visual)
        {
            Contract.Requires(visual != null, "Visual is null");
            var target = new System.Windows.Media.Imaging.RenderTargetBitmap((int)this.ActualWidth, (int)this.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            target.Render(visual);
            var brush = new ImageBrush(target);
            brush.Freeze();
            return brush;
        }

        private void CreateFade(double from, double to, DependencyObject element)
        {
            DoubleAnimation da = new DoubleAnimation();
            da.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            da.From = from;
            da.To = to;
            Storyboard.SetTarget(da, element);
            Storyboard.SetTargetProperty(da, new PropertyPath("Opacity"));
            Storyboard sb = new Storyboard();
            sb.Children.Add(da);
            sb.Begin();
        }


    }
}
