﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BaBaoWord.Util
{
    internal interface IDapperProvide
    {
        bool GetValue(string strSQL, out string value, object param = null);
        bool GetDataTable(string strSQL, out DataTable value, object param = null);
        bool GetStoredProcedure(string spName, out DataTable value, object param = null);
        bool ExecuteSQL(string strSQL, object param = null);

        bool ExecuteScalarSQL(string strSQL, out object value, object param = null);

        bool GetModel<T>(string strSQL, out object value, object param = null);

        bool Dispose();

        bool DecryptData();
    }
}
