﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using Dapper;
using System.Data.SQLite;

namespace BaBaoWord.Util
{
    internal class DapperSqlite : IDapperProvide
    {
        private SQLiteConnection m_sqliteConnection;
        public DapperSqlite(string connectionString, out bool dbExist)
        {
            dbExist = true;
            if (!string.IsNullOrEmpty(connectionString))
            {
                string dbPath = string.Empty;
                string[] parts = connectionString.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string part in parts)
                {
                    if (part.ToLower().Contains("data source"))
                    {
                        string[] dataSourcePart = part.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        if (dataSourcePart.Length == 2)
                        {
                            dbPath = dataSourcePart[1];
                            if (!File.Exists(dbPath))
                            {
                                SQLiteConnection.CreateFile(dbPath);
                                dbExist = false;
                                break;
                            }
                        }
                    }
                }
                m_sqliteConnection = new SQLiteConnection(connectionString);
                m_sqliteConnection.Open();
                //if (!dbExist)
                //{
                //    m_sqliteConnection.ChangePassword("!QAZ2wsx");
                //}
            }
        }

        public bool GetValue(string strSQL, out string value, object param = null)
        {
            value = null;
            bool result = false;
            try
            {
                if (m_sqliteConnection == null)
                {
                    throw new NullReferenceException("未設置SQLite的連線字串");
                }
                if (m_sqliteConnection.State != ConnectionState.Open)
                {
                    m_sqliteConnection.Open();
                }
                var data = m_sqliteConnection.Query(strSQL, param);
                if (data.Any())
                {
                    var firstRow = data.FirstOrDefault();
                    value = ((IDictionary<string, object>)firstRow).Values.FirstOrDefault().ToString();
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            finally
            {
                m_sqliteConnection.Close();
            }
            return result;
        }

        public bool GetDataTable(string strSQL, out System.Data.DataTable value, object param = null)
        {
            value = null;
            bool result = false;
            try
            {
                if (m_sqliteConnection == null)
                {
                    throw new NullReferenceException("未設置SQLite的連線字串");
                }
                if (m_sqliteConnection.State != ConnectionState.Open)
                {
                    m_sqliteConnection.Open();
                }
                SQLiteCommand cmd = new SQLiteCommand(m_sqliteConnection);
                value = new DataTable();
                value.Load(m_sqliteConnection.ExecuteReader(strSQL, param,null,null,CommandType.Text));
                cmd.Dispose();
                if (value != null)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            finally
            {
                m_sqliteConnection.Close();
            }
            return result;
        }

        public bool GetStoredProcedure(string spName, out System.Data.DataTable value, object param = null)
        {
            value = new DataTable();
            bool result = false;
            try
            {
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public bool ExecuteSQL(string strSQL, object param = null)
        {
            bool result = false;
            try
            {
                if (m_sqliteConnection == null)
                {
                    throw new NullReferenceException("未設置SQLite的連線字串");
                }
                if (m_sqliteConnection.State != ConnectionState.Open)
                {
                    m_sqliteConnection.Open();
                }
                m_sqliteConnection.Execute(strSQL, param);
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            finally
            {
                m_sqliteConnection.Close();
            }
            return result;
        }

        public bool ExecuteScalarSQL(string strSQL, out object value, object param = null)
        {
            bool result = false;
            value = null;
            try
            {
                if (m_sqliteConnection == null)
                {
                    throw new NullReferenceException("未設置SQLite的連線字串");
                }
                if (m_sqliteConnection.State != ConnectionState.Open)
                {
                    m_sqliteConnection.Open();
                }
                value = m_sqliteConnection.ExecuteScalar(strSQL, param);
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            finally
            {
                m_sqliteConnection.Close();
            }
            return result;
        }


        public bool GetModel<T>(string strSQL, out object value, object param = null)
        {
            value = null;
            bool result = false;
            try
            {
                if (m_sqliteConnection == null)
                {
                    throw new NullReferenceException("未設置SQLite的連線字串");
                }
                if (m_sqliteConnection.State != ConnectionState.Open)
                {
                    m_sqliteConnection.Open();
                }
                value = m_sqliteConnection.Query<T>(strSQL, param);
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            finally
            {
                m_sqliteConnection.Close();
            }
            return result;
        }


        public bool Dispose()
        {
            m_sqliteConnection.Close();

            GC.Collect();
            return true;
        }

        public bool DecryptData()
        {
            bool result = false;
            try
            {
                m_sqliteConnection.ChangePassword(string.Empty);
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e.Message);
            }
            return result;
        }
    }
}
