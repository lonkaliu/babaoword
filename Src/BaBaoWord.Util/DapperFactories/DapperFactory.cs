﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BaBaoWord.Util
{
    public class DapperFactory
    {
        private IDapperProvide _dapperProvide;
        internal IDapperProvide DapperProvide
        {
            get
            {
                if (_dapperProvide == null && !string.IsNullOrEmpty(UtilParameter.GetInstance().DapperConnectionString))
                {
                    switch (UtilParameter.GetInstance().DapperType)
                    {
                        case DapperType.SQLite:
                            bool dbExist = false;
                            _dapperProvide = new DapperSqlite(UtilParameter.GetInstance().DapperConnectionString, out dbExist);
                            break;
                    }
                }
                return _dapperProvide;
            }
        }

        public DapperFactory(string connectionString, out bool dbExist)
        {
            dbExist = false;
            if (_dapperProvide == null)
            {
                switch (UtilParameter.GetInstance().DapperType)
                {
                    case DapperType.SQLite:
                        _dapperProvide = new DapperSqlite(connectionString, out dbExist);
                        break;
                }
            }
        }

        public bool GetValue(string strSQL, out string value, object param = null)
        {
            return DapperProvide.GetValue(strSQL, out value, param);
        }

        public bool GetDataTable(string strSQL, out DataTable value, object param = null)
        {
            return DapperProvide.GetDataTable(strSQL, out value, param);
        }
        public bool GetStoredProcedure(string spName, out DataTable value, object param = null)
        {
            return DapperProvide.GetStoredProcedure(spName, out value, param);
        }
        public bool ExecuteSQL(string strSQL, object param = null)
        {
            return DapperProvide.ExecuteSQL(strSQL, param);
        }

        public bool ExecuteScalarSQL(string strSQL, out object value, object param = null)
        {
            return DapperProvide.ExecuteScalarSQL(strSQL, out value, param);
        }

        public bool GetModel<T>(string strSQL, out object value, object param = null)
        {
            return DapperProvide.GetModel<T>(strSQL, out value, param);
        }

        public bool Dispose()
        {
            return DapperProvide.Dispose();
        }

        public bool DecryptData()
        {
            return  _dapperProvide.DecryptData();
        }
    }
}
