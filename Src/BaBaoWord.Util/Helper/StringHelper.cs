﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BaBaoWord.Util
{
    public static class StringHelper
    {
        public static string Substring(this string str, int startIndex, int length, bool useByte)
        {
            string result = string.Empty;
            if (str == null)
            {
                return result;
            }
            if (useByte)
            {
                byte[] byteSource = System.Text.Encoding.Unicode.GetBytes(str);
                int byteLength = byteSource.Length - startIndex * 2;
                IEnumerable<int> byteRange = Enumerable.Range(startIndex * 2,
                     (
                         length * 2 > byteLength
                             ? byteLength
                             : length * 2
                     )
                     );
                //要補的byte數
                double alphanumeric = (byteRange.Count(i => byteSource[i].Equals(0)) * 0.5);

                double tempAlphanumeric = 0;
                int tempAlphanumericCount = 0;
                bool checkAdd = true;
                //往後找總共要補的個數
                for (int i = (startIndex * 2 + length * 2); i < byteSource.Length; i++)
                {
                    if (tempAlphanumeric.Equals(alphanumeric))
                    {
                        if (tempAlphanumericCount % 2 == 0)
                        {
                            alphanumeric = tempAlphanumericCount;
                        }
                        else
                        {
                            alphanumeric = tempAlphanumericCount + 1;
                        }
                        checkAdd = false;
                        break;
                    }
                    if (byteSource[i] > 0)
                    {
                        tempAlphanumeric += 0.5;
                    }
                    tempAlphanumericCount++;
                }

                if (checkAdd)
                {
                    alphanumeric += tempAlphanumericCount;
                }
                int totalCount = (length * 2) + (int)
                    (
                        alphanumeric % 2 == 0
                            ? alphanumeric
                            : alphanumeric + 1
                    );

                int realCount =
                    (
                        totalCount >= byteLength
                            ? byteLength
                            : totalCount
                    );
                result = System.Text.Encoding.Unicode.GetString(byteSource, startIndex * 2, realCount);
                if (byteLength > realCount)
                {
                    result += "...";
                }
            }
            else
            {
                result = str.Substring(startIndex, length);
            }
            return result;
        }

        public static string ReplaceText(this string str, Dictionary<string, string> value)
        {
            string result = str;
            int startIndex = result.IndexOf("{");
            if (value != null && value.Count > 0 && startIndex > -1)
            {
                string keyStr = result.Substring(startIndex + 1);
                int endIndex = keyStr.IndexOf("}");
                if (endIndex > -1)
                {
                    keyStr = keyStr.Substring(0, endIndex);
                    string headStr = str.Substring(0, startIndex + endIndex + 2);
                    string endStr = str.Substring(startIndex + endIndex + 2);
                    if (value.ContainsKey(keyStr.ToUpper()))
                    {
                        headStr = headStr.Replace("{" + keyStr + "}", value[keyStr.ToUpper()]);
                    }
                    result = headStr + endStr.ReplaceText(value);
                }

            }



            return result;
        }

        public static string ToSHA256(this string str)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();
            byte[] source = Encoding.Default.GetBytes(str);
            byte[] crypto = sha256.ComputeHash(source);
            return Convert.ToBase64String(crypto);
        }

    }
}
