﻿using HtmlAgilityPack;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace BaBaoWord.Util
{
    public static class HtmlHelper
    {
        //http://htmlagilitypack.codeplex.com/releases/view/90925
        public static bool GetHtemlContent(string url, out HtmlDocument content)
        {
            //url = "https://tw.stock.yahoo.com/q/q?s=2308";
            content = new HtmlDocument();
            bool result = false;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                WebProxy proxy = new WebProxy();
                proxy.UseDefaultCredentials = true;
                request.Proxy = proxy;
                request.Timeout = 5000;
                using (HttpWebResponse reponse = (HttpWebResponse)request.GetResponse())
                {
                    string coder = ((HttpWebResponse)reponse).CharacterSet;
                    Stream streamReceive = reponse.GetResponseStream();
                    content.Load(streamReceive, Encoding.GetEncoding(coder));
                }
                result = true;
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }
    }
}
