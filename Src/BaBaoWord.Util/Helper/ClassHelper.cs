﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BaBaoWord.Util
{
    public static class ClassHelper
    {
        ///// <summary>
        ///// 將物件中字串屬性為null的值轉成string.Empty
        ///// </summary>
        ///// <param name="source"></param>
        //public static void StringProperty2Empty(this object source)
        //{
        //    //設定其他欄位為空字串
        //    foreach (PropertyInfo prop in source.GetType().GetProperties())
        //    {
        //        if (prop.PropertyType == typeof(string))
        //        {
        //            string propValue = prop.GetValue(source, null) as string;
        //            if (propValue == null)
        //                prop.SetValue(source, string.Empty, null);
        //        }
        //    }
        //}

        /// <summary>
        /// 將物件中字串屬性為null的值轉成string.Empty
        /// </summary>
        /// <param name="source"></param>
        public static void StringProperty2Empty(object source)
        {
            //設定其他欄位為空字串
            foreach (PropertyInfo prop in source.GetType().GetProperties())
            {
                if (prop.PropertyType == typeof(string))
                {
                    string propValue = prop.GetValue(source, null) as string;
                    if (propValue == null)
                        prop.SetValue(source, string.Empty, null);
                }
            }
        }

        public static void Copy(this object source, object target)
        {
            PropertyInfo[] targetPropertys = target.GetType().GetProperties();
            foreach (PropertyInfo prop in source.GetType().GetProperties())
            {
                var targetProperty = targetPropertys.Where(item => item.Name.Equals(prop.Name)).FirstOrDefault();
                if (targetProperty != null)
                {
                    try
                    {
                        prop.SetValue(source, targetProperty.GetValue(target, null), null);
                    }
                    catch
                    {

                    }
                }
            }
        }
    }
}
