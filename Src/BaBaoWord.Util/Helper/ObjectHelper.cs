﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BaBaoWord.Util
{
    public static class ObjectHelper
    {
        public static T GetPropertyValue<T>(this object obj, string propertyName)
        {
            Type type = obj.GetType();
            T result = default(T);
            try
            {
                result = (T)obj.GetType().InvokeMember(propertyName, System.Reflection.BindingFlags.GetProperty, null, obj, new object[] { });
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }

        private static object GetDefaultValue(Type type)
        {
            if (type.IsValueType)
                return Activator.CreateInstance(type);

            return null;
        }


        public static string Serialize<T>(this object obj)
        {
            string result = string.Empty;
            try
            {
                if (obj is ObservableCollection<T>)
                {
                    obj = (ObservableCollection<T>)obj;
                    string fileName = "serializeTemp" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xml";
                    TextWriter writer = new StreamWriter(fileName);
                    XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<T>));
                    serializer.Serialize(writer, obj);
                    writer.Close();
                    result = File.ReadAllText(fileName);
                    File.Delete(fileName);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }

        public static ObservableCollection<T> Deserialize<T>(this object obj, string serializeStr)
        {
            ObservableCollection<T> result = null;
            try
            {
                if (obj is ObservableCollection<T>)
                {
                    string fileName = "serializeTemp" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xml";
                    File.WriteAllText(fileName, serializeStr);

                    TextReader reader = new StreamReader(fileName);
                    XmlSerializer deserializer = new XmlSerializer(typeof(ObservableCollection<T>));
                    result = (ObservableCollection<T>)deserializer.Deserialize(reader);
                    reader.Close();
                    File.Delete(fileName);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
    }
}
