﻿using System.Windows;
using System.Windows.Media;

namespace BaBaoWord.Util
{
    internal static class UIHelper
    {
        public static   T FindChild<T>(DependencyObject reference,string childName) where T : DependencyObject
        {
            if(reference == null)
            {
                return null;
            }

            T foundChild = null;
            if(reference != null)
            {
                int childrenCount = VisualTreeHelper.GetChildrenCount(reference);
                for(int i= 0;i< childrenCount;i++)
                {
                    var child = VisualTreeHelper.GetChild(reference, i);
                    T childType = child as T;
                    if (childType == null)
                    {
                        foundChild = FindChild<T>(child, childName);
                        if(foundChild != null)
                        {
                            break;
                        }
                    }
                    else if(!string.IsNullOrEmpty(childName))
                    {
                        FrameworkElement frameworkElement = child as FrameworkElement;
                        if(frameworkElement != null && frameworkElement.Name ==childName)
                        {
                            foundChild = (T)child;
                            break;
                        }
                    }
                    else
                    {
                        foundChild = (T)child;
                        break;
                    }
                }
            }
            return foundChild;

        }
    }
}
