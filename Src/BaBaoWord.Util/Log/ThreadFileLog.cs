﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BaBaoWord.Util
{
    internal class ThreadFileLog : FileLog
    {
        private Queue<Message> _messageQueue = new Queue<Message>();
        public ThreadFileLog()
        {
            Thread commandThread = new Thread(DoCommand);
            commandThread.IsBackground = true;
            commandThread.Start();
        }

        protected override void DoWriteLog(string fileName, string msg)
        {
            _messageQueue.Enqueue(new Message() { FileName = fileName, Msg = msg });
        }

        private void DoCommand()
        {
            while (true)
            {
                Thread.Sleep(1);
                if (_messageQueue.Count > 0)
                {
                    Message message = _messageQueue.Dequeue();
                    base.DoWriteLog(message.FileName, message.Msg);
                }
            }
        }
    }
}
