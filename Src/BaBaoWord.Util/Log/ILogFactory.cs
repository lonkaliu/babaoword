﻿namespace BaBaoWord.Util
{
    internal interface ILogFactory
    {
        LogProvide CreatorProvide();
    }
}