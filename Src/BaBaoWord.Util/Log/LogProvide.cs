﻿using System;

namespace BaBaoWord.Util
{
    internal abstract class LogProvide
    {
        /// <summary>
        /// 最後一筆的fail Log msg
        /// </summary>
        protected string _failLastMessage = string.Empty;

        /// <summary>
        /// 最後一筆的info Log msg
        /// </summary>
        protected string _infoLastMessage = string.Empty;

        /// <summary>
        /// Log的呈現等級
        /// </summary>
        protected LogLevel _limitLogLevel
        {
            get
            {
                return UtilParameter.GetInstance().LogLevel;
            }
        }

        /// <summary>
        /// Log的字數最大值
        /// </summary>
        protected int _maxLogSize
        {
            get
            {
                return UtilParameter.GetInstance().MaxLogSize;
            }
        }

        public abstract void WriteLog(LogLevel logLevel, string function, string description);

        public abstract void WriteLog(LogLevel logLevel, string function, Exception e);

        public abstract void WriteLog(LogLevel logLevel, string function, string description, Exception e);

        /// <summary>
        /// 確認使用者設定的Log呈現等級來決定是否要寫Log
        /// </summary>
        /// <param name="logLevel">寫Log的等級</param>
        /// <returns></returns>
        protected bool CheckLevel(LogLevel logLevel)
        {
            bool result = false;
            if ((int)logLevel <= (int)_limitLogLevel)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// 取得Log訊息的實際內容
        /// </summary>
        /// <param name="logLevel">寫Log的等級</param>
        /// <param name="function">Function Name</param>
        /// <param name="msg">來源訊息</param>
        /// <returns></returns>
        protected virtual string GetLogMsg(LogLevel logLevel, string function, string msg)
        {
            string result = msg;
            if (_maxLogSize > 0 && msg.Length > _maxLogSize)
            {
                result = msg.Substring(0, _maxLogSize, true);
            }
            if ((int)logLevel > 2)
            {
                lock (_infoLastMessage)
                {
                    if (result.Length == _infoLastMessage.Length && result.Equals(_infoLastMessage))
                    {
                        result = "same...";
                    }
                    else
                    {
                        _infoLastMessage = result;
                    }
                }
            }
            else
            {
                lock (_failLastMessage)
                {
                    if (result.Length == _failLastMessage.Length && result.Equals(_failLastMessage))
                    {
                        result = "same...";
                    }
                    else
                    {
                        _failLastMessage = result;
                    }
                }
            }
            return GetPrefix(logLevel, function) + result;
        }

        /// <summary>
        /// 取得Log訊息的前置詞
        /// </summary>
        /// <param name="logLevel">寫Log的等級</param>
        /// <param name="function">Function Name</param>
        /// <returns></returns>
        protected virtual string GetPrefix(LogLevel logLevel, string function)
        {
            return DateTime.Now.ToString("HH:mm:ss") + "[" + logLevel.ToString().PadLeft(10) + "]" + "[" + function.PadLeft(30) + "]";
        }
    }
}