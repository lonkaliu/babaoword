﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace BaBaoWord.Util
{
    public class BoolToGridRowHeightConverter : IValueConverter
    {
        //http://stackoverflow.com/questions/2502178/wpf-hide-grid-row
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double height = 0;
            if (value is bool && ((bool)value) && double.TryParse(parameter.ToString(), out height))
            {
                return new GridLength(height);
            }
            return new GridLength(height);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
