﻿using System;
using System.Windows;
using System.Windows.Data;

namespace BaBaoWord.Util
{
    public class NullOrEmptyConverter<T> : IValueConverter
    {
        public NullOrEmptyConverter(T trueValue,T falseValue)
        {
            True = trueValue;
            False = falseValue;
        }

        public T True { get; set; }
        public T False { get; set; }
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            
            bool result = false;

            if (value == null)
            {
                result = true;
            }
            else
            {
                result = string.IsNullOrEmpty(value.ToString());
            }
            return result ? True : False;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException("IsNullOrEmptyConverter can only be used OneWay.");
            //return true;
        }
    }
}
