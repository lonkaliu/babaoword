﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWord.Util
{
    public class NullOrEmptyToBooleanConverter : NullOrEmptyConverter<Boolean>
    {
        public NullOrEmptyToBooleanConverter():
            base(true,false)
        {

        }
    }
}
