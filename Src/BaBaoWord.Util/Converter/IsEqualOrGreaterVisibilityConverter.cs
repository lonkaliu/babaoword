﻿using System;
using System.Windows;
using System.Windows.Data;

namespace BaBaoWord.Util
{
    public class IsEqualOrGreaterVisibilityConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            bool result = false;
            int param;
            if (value != null && value is int
                && parameter != null && int.TryParse(parameter.ToString(), out param))
            {
                result = (int)value >= param ? true : false;
            }
            return result ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException("IsNullOrEmptyConverter can only be used OneWay.");
            //return true;
        }
    }
}
