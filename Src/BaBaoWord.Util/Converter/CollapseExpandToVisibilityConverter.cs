﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;

namespace BaBaoWord.Util
{
    public class CollapseExpandToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 2 && values[0] != null && values[0] is  Visibility && values[1] != null && values[1] is int)
            {
                return ((Visibility)values[0] == Visibility.Visible && ((int)values[1] == 0)) ? Visibility.Visible : Visibility.Collapsed;
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
