﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace BaBaoWord.Util
{
    public class MutilValueToBooleanConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Count() != 2)
                throw new InvalidOperationException("This converter expects only two bound values, the first is your comparer object and the second is the bound value. "
                + "Please check your Multibinding object.");

            if (values[0] != null && values[1] != null)
            {
                //we are dynamically casting our System.Object to its apparent type passed in as the parameter
                string val1 = values[0].ToString();
                string val2 = values[1].ToString();

                if (val1 == val2)
                    return true;
            }

            //If our parameter is null, we can assume that no type is required, and we are doing a primitive compare
            if (values[0] == values[1])
                return true;

            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
