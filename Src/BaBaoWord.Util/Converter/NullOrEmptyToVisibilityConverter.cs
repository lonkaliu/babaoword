﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace BaBaoWord.Util
{
    public class NullOrEmptyToVisibilityConverter : NullOrEmptyConverter<Visibility>
    {
        public NullOrEmptyToVisibilityConverter():
            base(Visibility.Visible,Visibility.Collapsed)
        {

        }
    }
}
