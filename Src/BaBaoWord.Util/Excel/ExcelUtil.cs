﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Data;

namespace BaBaoWord.Util
{
    public class ExcelUtil
    {
        public static bool Export(List<string> columns, Dictionary<string, int> value, string excelPath)
        {
            bool result = false;
            try
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    // 新增worksheet
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("Result");


                    //// 將DataTable資料塞到sheet中
                    //ws.Cells["A1"].LoadFromDataTable(dt, true);

                    for (int i = 0; i < columns.Count; i++)
                    {
                        ws.Cells[1, i + 1].Value = columns[i];
                    }

                    int rowIndex = 2;
                    foreach (string key in value.Keys)
                    {
                        ws.Cells[rowIndex, 1].Value = key;
                        ws.Cells[rowIndex, 2].Value = value[key];
                        rowIndex++;
                    }

                    // 設定Excel Header 樣式
                    using (ExcelRange rng = ws.Cells["A1:B1"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    package.SaveAs(new FileInfo(excelPath));
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }



        /// <summary>
        /// Table的欄位要自己對到col的順序
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="value"></param>
        /// <param name="excelPath"></param>
        /// <returns></returns>
        public static bool Export(List<string> columns, DataTable value, string excelPath)
        {
            bool result = false;
            try
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    // 新增worksheet
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("Result");

                    ws.Column(1).Hidden = true;

                    for (int i = 0; i < columns.Count; i++)
                    {
                        ws.Cells[1, i + 1].Value = columns[i];
                        switch(columns[i])
                        {
                            case "權重":
                            case "Weight":
                                ws.Column(i + 1).Width = 6;
                                break;
                            case "重要性":
                            case "Importance":
                                ws.Column(i + 1).Width = 8;
                                break;
                            case "單字":
                            case "Word":
                                ws.Column(i + 1).Width = 18;
                                break;
                            case "發音":
                            case "Pronunciation":
                                ws.Column(i + 1).Width = 18;
                                break;
                            case "詞性":
                            case "Speech":
                                ws.Column(i + 1).Width = 13;
                                break;
                            case "說明":
                            case "Explanation":
                                ws.Column(i + 1).Width = 22;
                                break;
                            case "例句":
                            case "Sentence":
                                ws.Column(i + 1).Width = 22;
                                break;
                        }

                    }

                    int rowIndex = 2;
                    foreach (DataRow dr in value.Rows)
                    {
                        for (int i = 0; i < columns.Count; i++)
                        {
                            ws.Cells[rowIndex, i + 1].Value = dr[i];
                        }
                        rowIndex++;
                    }

                    // 設定Excel Header 樣式
                    using (ExcelRange rng = ws.Cells["A1:" + (char)(64 + columns.Count) + "1"])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    package.SaveAs(new FileInfo(excelPath));
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }

        public static bool Import(string excelPath, out DataTable value, bool hasHeader = true)
        {
            bool result = false;
            value = new DataTable();
            try
            {
                if (File.Exists(excelPath))
                {
                    using (var pck = new OfficeOpenXml.ExcelPackage())
                    {
                        using (var stream = File.OpenRead(excelPath))
                        {
                            pck.Load(stream);
                        }
                        var ws = pck.Workbook.Worksheets.First();
                        foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                        {
                            value.Columns.Add(hasHeader
                                ? value.Columns.Contains(firstRowCell.Text) ? firstRowCell.Text + "_" + firstRowCell.Start.Column : firstRowCell.Text
                                : string.Format("Column {0}", firstRowCell.Start.Column));
                        }
                        int startRow = hasHeader ? 2 : 1;
                        for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                        {
                            var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                            DataRow dr = value.Rows.Add();
                            foreach (var cell in wsRow)
                            {
                                dr[cell.Start.Column - 1] = cell.Text;
                            }
                        }
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }
    }
}
