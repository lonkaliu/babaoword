﻿using System.IO;
using System;

namespace BaBaoWord.Util
{
    internal class UtilParameter
    {
        private UtilParameter()
        {
        }

        #region Const Parameter


        #endregion


        #region Log Parameter

        public string LogFile
        {
            get
            {
                return System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "LogFiles");
            }
        }

        public LogType LogType
        {
            get
            {
                return Util.LogType.File;
            }
        }

        public LogLevel LogLevel
        {
            get
            {
                return Util.LogLevel.Verbose;
            }
        }

        public int MaxLogSize
        {
            get
            {
                return -1;
            }
        }
        #endregion

        private string _dapperConnectionString;
        public string DapperConnectionString
        {
            get
            {
                return _dapperConnectionString;
            }
            set
            {
                _dapperConnectionString = value;
            }
        }

        private DapperType _dapperType = DapperType.SQLite;
        /// <summary>
        /// Dapper要實作的類別
        /// </summary>
        public DapperType DapperType
        {
            get
            {
                return _dapperType;
            }
            set
            {
                _dapperType = value;
            }
        }
        #region Singleton Patten
        private volatile static UtilParameter Instance;
        private static readonly object ms_lockObject = new object();
        public static UtilParameter GetInstance()
        {
            if (Instance == null)
            {
                lock (ms_lockObject)
                {
                    if (Instance == null)
                    {
                        Instance = new UtilParameter();
                    }
                }
            }
            return Instance;
        }
        #endregion
    }
}
