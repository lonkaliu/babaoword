﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Ionic.Zip;
using System.IO;
using System.ComponentModel;
using System.Threading;

namespace AutoUpdate
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                WindowStyle = WindowStyle.None;
                WindowStartupLocation = WindowStartupLocation.CenterScreen;
                AllowsTransparency = true;
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.DoWork += (s, o) =>
                {
                    Thread.Sleep(500);
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        processBar.Value = 5;
                    });

                    var result = DownloadPatch();
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        processBar.Value = 20;
                    });
                    Thread.Sleep(500);
                    if (result.Result)
                    {
                        using (ZipFile zips = ZipFile.Read("update.zip"))
                        {
                            double processValue = Math.Ceiling((double)((95 - 20) / zips.Count));

                            foreach (ZipEntry entity in zips)
                            {
                                entity.Extract(AppDomain.CurrentDomain.BaseDirectory, ExtractExistingFileAction.OverwriteSilently);
                                App.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    processBar.Value = processBar.Value + processValue;
                                });
                            }
                        }
                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            processBar.Value = 95;
                        });
                        Thread.Sleep(500);

                        File.Delete("update.zip");
                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            processBar.Value = 100;
                        });
                        try
                        {
                            System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BaBaoWord.exe"));
                        }
                        catch
                        {

                        }
                    }

                };
                backgroundWorker.RunWorkerAsync();
                backgroundWorker.RunWorkerCompleted += (s, o) =>
                {

                    App.Current.Shutdown();
                };
            }
            catch
            {

            }
        }

        public static FileService.DownloadResultMessage DownloadPatch()
        {
            FileService.DownloadResultMessage result = new FileService.DownloadResultMessage();
            try
            {
                FileService.DownloadPath data = new FileService.DownloadPath()
                {
                    Path = @"patch/update.zip"
                };
                try
                {
                    result = new FileService.BaBaoWordFileClient().DownloadFile(data);
                }
                catch
                {
                }
                if (result.Result)
                {
                    string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "update.zip");
                    using (FileStream targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        const int bufferLen = 65000;
                        byte[] buffer = new byte[bufferLen];
                        int count = 0;
                        while ((count = result.FileByte.Read(buffer, 0, bufferLen)) > 0)
                        {
                            targetStream.Write(buffer, 0, count);
                        }
                        targetStream.Close();
                        targetStream.Close();
                    }
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }
    }
}
