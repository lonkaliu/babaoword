﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;
using System.Runtime.InteropServices;

//http://blog.magnusmontin.net/2013/03/16/how-to-create-a-custom-window-in-wpf/
//http://nickeandersson.blogs.com/blog/2007/12/a-wpf-desktop-a.html
namespace AlertBaseLib
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:DesktopAlert"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:DesktopAlert;assembly=DesktopAlert"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:DesktopAlertBase/>
    ///
    /// </summary>
    public abstract class AlertBase : Window
    {
        private DoubleAnimation _fadeInAnimation;
        private DoubleAnimation _showAnimation;

        private DoubleAnimation _fadeOutAnimation;
        private DoubleAnimation _hideAnimation;

        private DispatcherTimer _activeTimer;

        private bool _result = false;

        static AlertBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AlertBase), new FrameworkPropertyMetadata(typeof(AlertBase)));
        }

        // We need to add a public constructor so that we can set up our
        // new window properly
        public AlertBase()
        {
            Visibility = Visibility.Visible;

            // Let's set up a dummy window size
            Width = 350;
            Height = 75;

            // Set some default properties for the alerts.
            // These can be changed by the derived alerts.
            Owner = Application.Current.MainWindow;
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            ShowInTaskbar = false;
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            //Topmost = true;
            AllowsTransparency = true;
            Opacity = 1;
            BorderThickness = new Thickness(1);
            BorderBrush = Brushes.Black;
            Background = Brushes.White;


            Loaded += new RoutedEventHandler(DesktopAlertBase_Loaded);
        }

        protected override void OnInitialized(EventArgs e)
        {
            SourceInitialized += AlertBase_SourceInitialized;
            base.OnInitialized(e);
        }

        #region Drag
        private HwndSource _hwndSource;
        void AlertBase_SourceInitialized(object sender, EventArgs e)
        {
            _hwndSource = (HwndSource)PresentationSource.FromVisual(this);
            _hwndSource.AddHook(DragHook);
        }
        [StructLayout(LayoutKind.Sequential)]
        //Struct for Marshalling the lParam
        public struct MoveRectangle
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

        }

        //http://stackoverflow.com/questions/4926201/make-a-window-draggable-within-a-certain-boundary-wpf
        private IntPtr DragHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_MOVING = 0x0216;
            const int WM_MOVE = 0x0003;

            switch (msg)
            {
                case WM_MOVING:
                    {
                        //read the lparm ino a struct

                        MoveRectangle rectangle = (MoveRectangle)Marshal.PtrToStructure(
                          lParam, typeof(MoveRectangle));


                        //

                        int limitLeft = (int)this.Owner.Left + 7;
                        int limitRight = (int)this.Owner.Left + (int)this.Owner.Width - 7;
                        int limitTop = (int)this.Owner.Top + 28;
                        int limitBottom = (int)this.Owner.Top + (int)this.Owner.Height - 7;


                        if (rectangle.Left < limitLeft)
                        {
                            rectangle.Left = limitLeft;
                            rectangle.Right = rectangle.Left + (int)this.Width;
                        }

                        if (rectangle.Top < limitTop)
                        {
                            rectangle.Top = limitTop;
                            rectangle.Bottom = rectangle.Top + (int)this.Height;
                        }

                        if (rectangle.Right > limitRight)
                        {
                            rectangle.Right = limitRight;
                            rectangle.Left = rectangle.Right - (int)this.Width;
                        }

                        if (rectangle.Bottom > limitBottom)
                        {
                            rectangle.Bottom = limitBottom;
                            rectangle.Top = rectangle.Bottom - (int)this.Height;
                        }

                        _hideAnimation.From = rectangle.Top;
                        _hideAnimation.To = rectangle.Top + 50;

                        Marshal.StructureToPtr(rectangle, lParam, true);

                        break;
                    }
                case WM_MOVE:
                    {
                        //Do the same thing as WM_MOVING You should probably enacapsulate that stuff so don'tn just copy and paste

                        break;
                    }


            }

            return IntPtr.Zero;
        }
        #endregion

        // When the template is applied to the control, look for a button called "PART_CloseButton".
        // If such a button exists, hook up an event handler so that the alert can be closed when
        // the user clicks the button.
        public override void OnApplyTemplate()
        {
            // Set up the fade in and fade out animations
            _fadeInAnimation = new DoubleAnimation();
            _fadeInAnimation.From = 0;
            _fadeInAnimation.To = 1;
            _fadeInAnimation.Duration = new Duration(TimeSpan.Parse("0:0:0.3"));
            _showAnimation = new DoubleAnimation();
            _showAnimation.From = this.Top + 50;
            _showAnimation.To = this.Top;
            _showAnimation.Duration = new Duration(TimeSpan.Parse("0:0:0.3"));


            // For the fade out we omit the from, so that it can be smoothly initiated
            // from a fade in that gets interrupted when the user wants to close the window
            _fadeOutAnimation = new DoubleAnimation();
            _fadeOutAnimation.To = 0;
            _fadeOutAnimation.Duration = new Duration(TimeSpan.Parse("0:0:0.3"));
            _hideAnimation = new DoubleAnimation();
            _hideAnimation.From = this.Top;
            _hideAnimation.To = this.Top + 50;
            _hideAnimation.Duration = new Duration(TimeSpan.Parse("0:0:0.3"));

            Rectangle rec_MoveRectangle = GetTemplateChild("rec_MoveRectangle") as Rectangle;
            if (rec_MoveRectangle != null)
            {
                rec_MoveRectangle.PreviewMouseDown += rec_MoveRectangle_PreviewMouseDown;
            }

            ButtonBase cancelButton = Template.FindName("btn_Cancel", this) as ButtonBase;
            if (cancelButton != null)
            {
                cancelButton.Click += new RoutedEventHandler(closeButton_Click);
            }

            ButtonBase cancelButtonHide = Template.FindName("btn_CloseHide", this) as ButtonBase;
            if (cancelButtonHide != null)
            {
                cancelButtonHide.Click += new RoutedEventHandler(closeButton_Click);
            }

            Image closeButton = Template.FindName("btn_Close", this) as Image;
            if (closeButton != null)
            {
                closeButton.MouseUp += closeButton_MouseUp;
            }

            ButtonBase okButton = Template.FindName("btn_OK", this) as ButtonBase;
            if (okButton != null)
            {
                okButton.Click += okButton_Click;
            }


        }


        void closeButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            closeButton_Click(null, null);
        }

        void okButton_Click(object sender, RoutedEventArgs e)
        {
            RaisePreOkEvent();
            _activeTimer.Stop();
            FadeOut();
            _result = true;
        }
        // If the user clicks the close button, stop the timer that counts how long the alert
        // window has been open, and start the fading out of the window.
        void closeButton_Click(object sender, RoutedEventArgs e)
        {
            RaisePreCancelEvent();
            _activeTimer.Stop();
            FadeOut();
        }


        void rec_MoveRectangle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        // In the Loaded-event handler we need to figure out where to place the alert window.
        // Currently they are all placed in the bottom right corner, and can not take into
        // account other currently open alert windows.
        void DesktopAlertBase_Loaded(object sender, RoutedEventArgs e)
        {
            // Figure out where to place the window based on the current screen resolution
            //Rect workAreaRectangle = System.Windows.SystemParameters.WorkArea;
            //Left = workAreaRectangle.Right - Width - BorderThickness.Right;
            //Top = workAreaRectangle.Bottom - Height - BorderThickness.Bottom;
            _fadeInAnimation.Completed += new EventHandler(_fadeInAnimation_Completed);

            // Start the fade in animation
            BeginAnimation(AlertBase.OpacityProperty, _fadeInAnimation);
            BeginAnimation(AlertBase.TopProperty, _showAnimation);
        }

        // When the fade in animation is completed, start another timer that fires an event when the
        // window has been visible for 10 seconds
        void _fadeInAnimation_Completed(object sender, EventArgs e)
        {
            _activeTimer = new DispatcherTimer();
            _activeTimer.Interval = TimeSpan.Parse("0:0:10");
            // Attach an anonymous method to the timer so that we can start fading out the alert
            // when the timer is done.
            _activeTimer.Tick += delegate(object obj, EventArgs ea) { FadeOut(); };

            TextBlock txt = Template.FindName("hidden_EnabledCountdown", this) as TextBlock;
            if (txt.Text.Equals("True"))
            {
                _activeTimer.Start();
            }
        }

        // Set up the fade out animation, and hook up an event handler to fire when it is completed.
        private void FadeOut()
        {
            // Attach an anonymous method to the Completed-event of the fade out animation
            // so that we can close the alert window when the animation is done.
            //_fadeOutAnimation.Completed += delegate(object sender, EventArgs e) { Close(); };
            _fadeOutAnimation.Completed += new EventHandler(_fadeOutAnimation_Completed);

            BeginAnimation(AlertBase.OpacityProperty, _fadeOutAnimation, HandoffBehavior.SnapshotAndReplace);
            BeginAnimation(AlertBase.TopProperty, _hideAnimation, HandoffBehavior.SnapshotAndReplace);
        }

        private void _fadeOutAnimation_Completed(object sender, EventArgs e)
        {
            this.DialogResult = _result;
            _activeTimer.IsEnabled = false;
            Close();
        }


        public string GetResourceString(string str)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    result = (string)Application.Current.Resources[str];
                    if (result == null)
                    {
                        result = "Can't Find Resources Key 「" + str + "」";
                    }
                }
                catch
                {
                    result = "Can't Find Resources Key 「" + str + "」";
                }
            }
            return result;
        }


        public static readonly RoutedEvent PreCancelEvent = EventManager.RegisterRoutedEvent(
        "PreCancel", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AlertBase));

        public event RoutedEventHandler PreCancel
        {
            add { AddHandler(PreCancelEvent, value); }
            remove { RemoveHandler(PreCancelEvent, value); }
        }

        private void RaisePreCancelEvent()
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(PreCancelEvent);
            RaiseEvent(eventArgs);
        }

        public static readonly RoutedEvent PreOkEvent = EventManager.RegisterRoutedEvent(
            "PreOk", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AlertBase));

        public event RoutedEventHandler PreOk
        {
            add { AddHandler(PreOkEvent, value); }
            remove { RemoveHandler(PreOkEvent, value); }
        }

        private void RaisePreOkEvent()
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(PreOkEvent);
            RaiseEvent(eventArgs);
        }

    }
}
