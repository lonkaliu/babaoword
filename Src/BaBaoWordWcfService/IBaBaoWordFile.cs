﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BaBaoWordWcfService
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼和組態檔中的介面名稱 "IBaBaoWordFileService"。
    [ServiceContract]
    public interface IBaBaoWordFile
    {
        [OperationContract]
        ResultMessage UploadUserData(UploadFileData fileMessage);

        [OperationContract]
        DownloadResultMessage DownloadUserData(DownloadFileData fileMessage);

        [OperationContract]
        DownloadResultMessage DownloadFile(DownloadPath path);

    }

    [MessageContract]
    public class UploadFileData : IDisposable
    {
        [MessageHeader]
        public string Token;
        [MessageHeader]
        public string UserGuid;
        [MessageBodyMember]
        public Stream FileByte;
        public void Dispose()
        {
            if (FileByte != null)
            {
                FileByte.Close();
                FileByte = null;
            }
        }
    }


    [MessageContract]
    public class DownloadFileData
    {
        [MessageHeader]
        public string Token { get; set; }

        [MessageHeader]
        public string UserGuid { get; set; }
    }


    [MessageContract]
    public class DownloadPath
    {
        [MessageHeader]
        public string Path { get; set; }
    }
    [MessageContract]
    public class DownloadResultMessage : IDisposable
    {
        public DownloadResultMessage()
        {
            FileByte = new MemoryStream(new byte[0]);
        }
        [MessageHeader(MustUnderstand = true)]
        public bool Result;
        [MessageHeader(MustUnderstand = true)]
        public string Exception;
        [MessageHeader(MustUnderstand = true)]
        public long Length;
        [MessageBodyMember(Order = 1)]
        public Stream FileByte;


        public void Dispose()
        {
            if (FileByte != null)
            {
                FileByte.Close();
                FileByte = null;
            }
        }
    }

    [MessageContract]
    public class ResultMessage
    {
        [MessageHeader]
        public bool Result;
        [MessageHeader]
        public string Exception;
    }
}
