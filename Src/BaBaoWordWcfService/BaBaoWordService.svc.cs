﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Web.Hosting;
using System.Data.Common;
using System.Data.SqlClient;
using BaBaoWordWcfService.Util;
using System.Net.Mail;

namespace BaBaoWordWcfService
{
    public class BaBaoWordService : IBaBaoWord
    {
        private CommonDb _dao = new CommonDb();

        //delete current_token;
        //delete current_version;
        //delete user_info;
        //delete user_language;
        //https://www.youtube.com/watch?v=pC9eULhE77k
        #region User&Token
        public ResultData Login(UserInfo user)
        {
            ResultData result = new ResultData();
            try
            {
                ResultData checkResult = CheckAccount(user.Email);
                if (checkResult.Result)
                {
                    result.Exception = "RegisterNewAccountFirst";
                }
                else
                {
                    var param = new List<DbParameter>() { new SqlParameter("@Email", user.Email), new SqlParameter("@Password", user.Password) };
                    System.Data.DataTable data = _dao.GetDataTable("SELECT ID,USER_GUID,NAME FROM USER_INFO WHERE EMAIL=@Email AND PASSWORD=@Password", param);
                    int id;
                    if (data != null && data.Rows.Count > 0 && !string.IsNullOrEmpty(data.Rows[0]["ID"].ToString()) && int.TryParse(data.Rows[0]["ID"].ToString(), out id))
                    {
                        _dao.ExecuteSQL("UPDATE USER_INFO SET LAST_DATE=getutcdate() WHERE  EMAIL=@Email AND PASSWORD=@Password", param);
                        ResultData tokenResult = GetToken(user);
                        if (tokenResult.Result)
                        {
                            result.Result = true;
                            result.Values.Add("USER_GUID", data.Rows[0]["USER_GUID"].ToString());
                            result.Values.Add("Token", tokenResult.Values["Token"].ToString());
                            result.Values.Add("Name", data.Rows[0]["NAME"].ToString());
                            result.Values.Add("ID", id);
                        }
                        else
                        {
                            result.Exception = tokenResult.Exception;
                        }
                    }
                    else
                    {
                        result.Exception = "PasswordError";
                    }
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public List<LanguageLevel> GetLanguage(long id)
        {
            List<LanguageLevel> result = new List<LanguageLevel>();
            try
            {
                System.Data.DataTable langData = _dao.GetDataTable("SELECT LANGUAGE_NAME,LANGUAGE_LEVEL FROM USER_LANGUAGE WHERE UID=" + id);
                langData.TableName = "language";
                if (langData != null)
                {
                    foreach (System.Data.DataRow dr in langData.Rows)
                    {
                        result.Add(new LanguageLevel()
                        {
                            Name = dr["LANGUAGE_NAME"].ToString(),
                            Level = int.Parse(dr["LANGUAGE_LEVEL"].ToString())
                        });
                    }
                }
            }
            catch
            {
                result = null;
            }
            return result;
        }

        public ResultData NewAccount(UserInfo user)
        {
            ResultData result = new ResultData();
            try
            {
                ResultData checkResult = CheckAccount(user.Email);
                if (checkResult.Result)
                {
                    List<DbParameter> param = new List<DbParameter>();
                    string userGuid = Guid.NewGuid().ToString();
                    param.Add(new SqlParameter("@Email", user.Email));
                    param.Add(new SqlParameter("@Password", user.Password));
                    param.Add(new SqlParameter("@Name", user.Name));
                    param.Add(new SqlParameter("@UserGuid", userGuid));
                    object value = _dao.ExecuteScalar("INSERT INTO USER_INFO VALUES (@Email,@Password,@Name,@UserGuid,getutcdate(),getutcdate());Select @@IDENTITY", param);
                    int id;
                    if (value != null && int.TryParse(value.ToString(), out id))
                    {
                        _dao.ExecuteSQL("INSERT INTO CURRENT_VERSION VALUES (@Id,1,getutcdate())", new List<DbParameter>() { new SqlParameter("@Id", id) });
                        string token = Guid.NewGuid().ToString();
                        _dao.ExecuteSQL("INSERT INTO CURRENT_TOKEN VALUES (@Token,@Id,getutcdate(),getutcdate())", new List<DbParameter>() { new SqlParameter("@Token", token), new SqlParameter("@Id", id) });

                        if (user.LanguageLevel != null)
                        {
                            string strSql = string.Empty;
                            int i = 0;
                            param = new List<DbParameter>();
                            param.Add(new SqlParameter("@Id", id));
                            foreach (LanguageLevel lang in user.LanguageLevel)
                            {
                                param.Add(new SqlParameter(string.Format("@LanguageName_{0}", i), lang.Name));
                                param.Add(new SqlParameter(string.Format("@LanguageLevel_{0}", i), lang.Level));
                                strSql += string.Format("INSERT INTO USER_LANGUAGE VALUES (@Id,@LanguageName_{0},@LanguageLevel_{0});", i);
                            }
                            _dao.ExecuteSQL(strSql, param);
                        }

                        result.Result = true;
                        result.Values.Add("USER_GUID", userGuid);
                        result.Values.Add("Token", token);
                        result.Values.Add("ID", id);
                    }
                }
                else
                {
                    result.Exception = checkResult.Exception;
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public ResultData EditAccount(UserInfo user)
        {
            ResultData result = new ResultData();
            try
            {
                List<DbParameter> param = new List<DbParameter>();
                param.Add(new SqlParameter("@Password", user.Password));
                param.Add(new SqlParameter("@Name", user.Name));
                param.Add(new SqlParameter("@Id", user.ID));
                _dao.ExecuteSQL("UPDATE USER_INFO SET PASSWORD=@Password,NAME=@Name WHERE ID=@Id", param);
                if (user.LanguageLevel != null)
                {
                    string strSql = string.Empty;
                    int i = 0;
                    param = new List<DbParameter>();
                    param.Add(new SqlParameter("@Id", user.ID));
                    _dao.ExecuteSQL("DELETE FROM USER_LANGUAGE WHERE UID=@ID", param);

                    foreach (LanguageLevel lang in user.LanguageLevel)
                    {
                        param.Add(new SqlParameter(string.Format("@LanguageName_{0}", i), lang.Name));
                        param.Add(new SqlParameter(string.Format("@LanguageLevel_{0}", i), lang.Level));
                        strSql += string.Format("INSERT INTO USER_LANGUAGE VALUES (@Id,@LanguageName_{0},@LanguageLevel_{0});", i);
                    }
                    _dao.ExecuteSQL(strSql, param);
                }
                result.Result = true;
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public ResultData ChangePassword(string email, string password)
        {
            ResultData result = new ResultData();
            try
            {
                List<DbParameter> param = new List<DbParameter>();
                param.Add(new SqlParameter("@Email", email));
                param.Add(new SqlParameter("@Password", password));
                _dao.ExecuteSQL("UPDATE USER_INFO SET PASSWORD=@Password WHERE EMAIL=@Email", param);
                result.Result = true;
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public ResultData CheckAccount(string email)
        {
            ResultData result = new ResultData();
            try
            {
                List<DbParameter> param = new List<DbParameter>();
                param.Add(new SqlParameter("@Email", email));
                var data = _dao.GetValue("SELECT COUNT(*) FROM USER_INFO WHERE EMAIL = @EMAIL;", param);
                int count;
                if (int.TryParse(data, out count) && count == 0)
                {
                    result.Result = true;
                }
                else
                {
                    result.Exception = "DuplicateEmail";
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public ResultData GetToken(UserInfo user)
        {
            ResultData result = new ResultData();
            try
            {
                var token = _dao.GetValue(@"DELETE FROM CURRENT_TOKEN WHERE DATEDIFF(MINUTE,LAST_DATE,getutcdate()) > 60;
                                            SELECT TOKEN FROM CURRENT_TOKEN A LEFT JOIN USER_INFO B ON A.UID = B.ID WHERE B.EMAIL=@Email AND B.PASSWORD=@Password;
                                            UPDATE CURRENT_TOKEN SET LAST_DATE=getutcdate() WHERE TOKEN=(SELECT TOKEN FROM CURRENT_TOKEN A LEFT JOIN USER_INFO B ON A.UID = B.ID WHERE B.EMAIL=@Email AND B.PASSWORD=@Password)"
                    , new List<DbParameter>() { new SqlParameter("@Email", user.Email), new SqlParameter("@Password", user.Password) });
                if (string.IsNullOrEmpty(token))
                {
                    token = Guid.NewGuid().ToString();
                    _dao.ExecuteSQL("INSERT INTO CURRENT_TOKEN VALUES (@Token,(SELECT ID FROM USER_INFO WHERE EMAIL=@Email AND PASSWORD=@Password),getutcdate(),getutcdate())"
                        , new List<DbParameter>() { new SqlParameter("@Token", token), new SqlParameter("@Email", user.Email), new SqlParameter("@Password", user.Password) });
                }
                result.Result = true;
                result.Values.Add("Token", token);
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public ResultData SendValidationCode(string email, string code)
        {
            ResultData result = new ResultData();
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("BaBaoTech@gmail.com", "Ba Bao Tech");
                msg.To.Add(email);
                msg.Subject = "[Ba Bao Word]Verification Code Check";
                msg.Body = string.Format("Your verification code is 「{0}」", code);
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                string password = _dao.GetValue("SELECT VALUE FROM SYSTEM_CONFIG WHERE CATEGORY='System' AND [KEY]='BaBaoTechGMailPW'");
                client.Credentials = new System.Net.NetworkCredential("BaBaoTech@gmail.com", password);
                client.Timeout = 20000;
                client.Send(msg);
                result.Result = true;
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        #endregion

        #region Version

        public ResultData GetVersion(string token, long id)
        {
            ResultData result = new ResultData();
            try
            {
                if (CommonUtil.CheckToken(token))
                {
                    var version = _dao.GetValue(@"SELECT VERSION FROM CURRENT_VERSION WHERE UID=@Id"
                        , new List<DbParameter>() { new SqlParameter("@Id", id) });
                    if (string.IsNullOrEmpty(version))
                    {
                        _dao.ExecuteSQL("INSERT INTO CURRENT_VERSION VALUES (@Id,1,getutcdate())", new List<DbParameter>() { new SqlParameter("@Id", id) });
                        result.Values.Add("Version", "1");
                    }
                    else
                    {
                        result.Values.Add("Version", version);
                    }
                    result.Result = true;
                }
                else
                {
                    result.Exception = "LostToken";
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public ResultData SetVersion(string token, long id, int version)
        {
            ResultData result = new ResultData();
            try
            {
                if (CommonUtil.CheckToken(token))
                {
                    _dao.ExecuteSQL(@"UPDATE CURRENT_VERSION SET VERSION=@Version,UPDATE_TIME=getutcdate() WHERE UID=@Id"
                        , new List<DbParameter>() { new SqlParameter("@Id", id), new SqlParameter("@Version", version) });
                    result.Result = true;
                }
                else
                {
                    result.Exception = "LostToken";
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        #endregion


        public ResultData GetBaBaoWordVersion()
        {
            ResultData result = new ResultData();
            try
            {
                string version = _dao.GetValue("SELECT VALUE FROM SYSTEM_CONFIG WHERE CATEGORY='System' AND [KEY]='Version'");
                result.Values.Add("Version", version);
                result.Result = true;
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }
    }
}
