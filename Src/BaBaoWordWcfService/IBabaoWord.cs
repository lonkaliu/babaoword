﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;

namespace BaBaoWordWcfService
{
    //TODO X.509 certificate validation
    [ServiceContract]
    public interface IBaBaoWord
    {

        [OperationContract]
        ResultData Login(UserInfo user);

        [OperationContract]
        List<LanguageLevel> GetLanguage(long id);
        [OperationContract]
        ResultData NewAccount(UserInfo user);

        [OperationContract]
        ResultData EditAccount(UserInfo user);

        [OperationContract]
        ResultData ChangePassword(string email, string password);

        [OperationContract]
        ResultData CheckAccount(string email);

        [OperationContract]
        ResultData GetToken(UserInfo user);

        [OperationContract]
        ResultData SendValidationCode(string email,string code);

        [OperationContract]
        ResultData GetVersion(string token, long id);

        [OperationContract]
        ResultData SetVersion(string token, long id, int version);

        [OperationContract]
        ResultData GetBaBaoWordVersion();
    }



    //使用下列範例中所示的資料合約，新增複合型別至服務作業。
    [DataContract]
    public class UserInfo
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<LanguageLevel> LanguageLevel { get; set; }
    }

    [DataContract]
    public class LanguageLevel
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Level { get; set; }
    }

    [DataContract]
    public class ResultData
    {
        public ResultData()
        {
            Values = new Dictionary<string, object>();
        }
        [DataMember]
        public bool Result { get; set; }

        [DataMember]
        public Dictionary<string, object> Values { get; set; }

        [DataMember]
        public string Exception { get; set; }
    }
}
