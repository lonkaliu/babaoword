﻿using BaBaoWordWcfService.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Hosting;

namespace BaBaoWordWcfService
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼、svc 和組態檔中的類別名稱 "BaBaoWordFileService"。
    // 注意: 若要啟動 WCF 測試用戶端以便測試此服務，請在 [方案總管] 中選取 BaBaoWordFileService.svc 或 BaBaoWordFileService.svc.cs，然後開始偵錯。
    public class BaBaoWordFileService : IBaBaoWordFile
    {
        public ResultMessage UploadUserData(UploadFileData fileMessage)
        {
            ResultMessage result = new ResultMessage();
            try
            {
                if (CommonUtil.CheckToken(fileMessage.Token))
                {
                    FileStream targetStream = null;
                    Stream sourceStream = fileMessage.FileByte;
                    Directory.CreateDirectory(HostingEnvironment.MapPath("~/data"));
                    string filePath = Path.Combine(HostingEnvironment.MapPath("~/data"), fileMessage.UserGuid);
                    using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        const int bufferLen = 65000;
                        byte[] buffer = new byte[bufferLen];
                        int count = 0;
                        while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                        {
                            targetStream.Write(buffer, 0, count);
                        }
                        targetStream.Close();
                        targetStream.Close();
                        result.Result = true;
                    }
                }
                else
                {
                    result.Exception = "LostToken";
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public DownloadResultMessage DownloadUserData(DownloadFileData fileMessage)
        {
            DownloadResultMessage result = new DownloadResultMessage();
            try
            {
                if (CommonUtil.CheckToken(fileMessage.Token))
                {
                    string filePath = System.IO.Path.Combine(HostingEnvironment.MapPath("~/data"), fileMessage.UserGuid);
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
                    if (!fileInfo.Exists)
                    {
                        result.Exception = "FileNotFound";
                        return result;
                    }
                    System.IO.FileStream stream = new System.IO.FileStream(filePath,
                          System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    result.Length = fileInfo.Length;
                    result.FileByte = stream;
                    result.Result = true;
                }
                else
                {
                    result.Exception = "LostToken";
                }
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

        public DownloadResultMessage DownloadFile(DownloadPath path)
        {
            DownloadResultMessage result = new DownloadResultMessage();
            try
            {
                string filePath = System.IO.Path.Combine(HostingEnvironment.MapPath("~"), path.Path);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
                if (!fileInfo.Exists)
                {
                    result.Exception = "FileNotFound";
                    return result;
                }
                System.IO.FileStream stream = new System.IO.FileStream(filePath,
                      System.IO.FileMode.Open, System.IO.FileAccess.Read);
                result.Length = fileInfo.Length;
                result.FileByte = stream;
                result.Result = true;
            }
            catch (Exception e)
            {
                result.Exception = e.Message;
            }
            return result;
        }

    }
}
