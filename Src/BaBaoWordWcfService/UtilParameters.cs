﻿using System.Collections;

namespace BaBaoWordWcfService.Util
{
    public class UtilParameters
    {
        private static readonly object ms_lockObject = new object();
        private volatile static UtilParameters ms_Instance;


        private CommonDbType _commonDbType = CommonDbType.SqlServer;
        //private string _connectionString = "workstation id=BaBaoWord.mssql.somee.com;packet size=4096;user id=lonka_SQLLogin_1;pwd=lq6kwpwj59;data source=BaBaoWord.mssql.somee.com;persist security info=False;initial catalog=BaBaoWord";
        //private string _connectionString = "Data Source=SQL5023.myASP.NET;Initial Catalog=DB_A2A8C1_babaotech;User Id=DB_A2A8C1_babaotech_admin;Password=babao0915;";
        private string _connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["babaotech"].ConnectionString;
        private string _logFile = System.Web.Hosting.HostingEnvironment.MapPath("~/LogFiles");

        private LogLevel _logLevel = LogLevel.Verbose;
        private LogType _logType = LogType.File;
        private int _maxLogSize = -1;

        private UtilParameters()
        {
        }

        /// <summary>
        /// Common DB的取資料方式
        /// <para>預設：SqlServer</para>
        /// <para>對應：（Setting.ini）Setting -> CommonDbType</para>
        /// </summary>
        public CommonDbType CommonDbType
        {
            get
            {
                return _commonDbType;
            }
            set
            {
                _commonDbType = value;
            }
        }


        /// <summary>
        /// 連線字串
        /// <para>預設：Empty</para>
        /// <para>對應：（Setting.ini）Setting -> ConnectionString</para>
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return _connectionString;
            }
        }

        /// <summary>
        /// 記錄Log的資料夾路徑
        /// <para>預設：BaseDirectory\\LogFiles</para>
        /// <para>對應：（Setting.ini）Path Setting -> LogFile</para>
        /// </summary>
        public string LogFile
        {
            get
            {
                return _logFile;
            }
            set
            {
                _logFile = value;
            }
        }

        /// <summary>
        /// 寫Log的等級，越嚴重數字越小，所以只會寫小於設定等級的Log
        /// <para>預設：Error</para>
        /// <para>對應：（Setting.ini）Setting -> LogLevel</para>
        /// </summary>
        public LogLevel LogLevel
        {
            get
            {
                return _logLevel;
            }
            set
            {
                _logLevel = value;
            }
        }

        /// <summary>
        /// 記錄寫Log的種類
        /// <para>預設：File</para>
        /// <para>對應：（Setting.ini）Setting -> LogType</para>
        /// </summary>
        public LogType LogType
        {
            get
            {
                return _logType;
            }
            set
            {
                _logType = value;
            }
        }

        /// <summary>
        /// 寫Log的最大字數限學
        /// <para>預設：-1</para>
        /// <para>對應：（Setting.ini）Setting -> MaxLogSize</para>
        /// </summary>
        public int MaxLogSize
        {
            get
            {
                return _maxLogSize;
            }
            set
            {
                _maxLogSize = value;
            }
        }


        public static UtilParameters GetInstance()
        {
            if (ms_Instance == null)
            {
                lock (ms_lockObject)
                {
                    if (ms_Instance == null)
                    {
                        ms_Instance = new UtilParameters();
                    }
                }
            }
            return ms_Instance;
        }
    }
}