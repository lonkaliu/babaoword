﻿namespace BaBaoWordWcfService.Util
{
    internal class FileLogFactory : ILogFactory
    {
        public LogProvide CreatorProvide()
        {
            //這邊可以依照傳入值Switch決定File Log的提供者（Thread File Log）
            LogProvide logProvide = new FileLog();
            //LogProvide logProvide = new ThreadFileLog();
            return logProvide;
        }
    }
}