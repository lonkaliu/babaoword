﻿using System;
using System.IO;

namespace BaBaoWordWcfService.Util
{
    internal class FileLog : LogProvide
    {
        /// <summary>
        /// 存Log的資料夾路徑
        /// </summary>
        private string m_filePath
        {
            get
            {
                return UtilParameters.GetInstance().LogFile;
            }
        }

        private void DoWriteLog(LogLevel logLevel, string function, string msg)
        {
            if (CheckLevel(logLevel))
            {
                string fileName = string.Empty;
                if ((int)logLevel > 2)
                {
                    fileName = Path.Combine(m_filePath, "info_" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
                }
                else
                {
                    fileName = Path.Combine(m_filePath, "faillog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
                }
                DoWriteLog(fileName, GetLogMsg(logLevel, function, msg));
            }
        }

        private readonly object m_writeLogLockObj = new object();

        protected virtual void DoWriteLog(string fileName, string msg)
        {
            bool retry = false;
            lock (m_writeLogLockObj)
            {
                StreamWriter writer = null;
                try
                {
                    writer = File.AppendText(fileName);
                    writer.WriteLine(msg);
                }
                catch (Exception e)
                {
                    if (e is DirectoryNotFoundException)
                    {
                        retry = true;
                    }
                }
                if (writer != null)
                {
                    try
                    {
                        writer.Close();
                    }
                    catch { }
                    writer = null;
                }
                if (retry) // Create Directory
                {
                    writer = null;
                    try
                    {
                        Directory.CreateDirectory(fileName.Replace(Path.GetFileName(fileName), string.Empty));
                        writer = File.AppendText(fileName);
                        writer.WriteLine(msg);
                    }
                    catch { }
                    if (writer != null)
                    {
                        try
                        {
                            writer.Close();
                        }
                        catch { }
                        writer = null;
                    }
                }
            }
        }

        #region LogProvide

        public override void WriteLog(LogLevel logLevel, string function, string description)
        {
            DoWriteLog(logLevel, function, description);
        }

        public override void WriteLog(LogLevel logLevel, string function, Exception e)
        {
            DoWriteLog(logLevel, function, e.Message + e.Source + e.StackTrace);
        }

        public override void WriteLog(LogLevel logLevel, string function, string description, Exception e)
        {
            DoWriteLog(logLevel, function, description + "," + e.Message + e.Source + e.StackTrace);
        }

        #endregion LogProvide
    }
}