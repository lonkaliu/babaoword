﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BaBaoWordWcfService.Util
{
    public class CommonUtil
    {
        public static bool CheckToken(string token)
        {
            bool result = false;
            try
            {
                CommonDb _dao = new CommonDb();
                string tokenCount = _dao.GetValue("SELECT COUNT(*) FROM CURRENT_TOKEN WHERE TOKEN = @Token;UPDATE CURRENT_TOKEN SET LAST_DATE = GETUTCDATE() WHERE TOKEN = @Token", new List<DbParameter>() { new SqlParameter("@Token", token) });
                int count;
                if (int.TryParse(tokenCount, out count) && count == 1)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Log.WriteLog(LogLevel.Error, e);
            }
            return result;
        }


    }
}