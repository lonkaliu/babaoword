﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWordWcfService.Util
{
    internal class CommonDbInfo
    {

        private static List<CommonDbClass> ms_dbInfo;

        /// <summary>
        /// 所有DbProviderFactory支援的種類的List，
        /// dataBaseType:type(Oracle)，
        /// dbProvider:Dll(System.Data.OracleClient)
        /// </summary>
        public static List<CommonDbClass> DbInfo
        {
            get
            {
                if (ms_dbInfo == null)
                    ms_dbInfo = new List<CommonDbClass>();
                ms_dbInfo.Add(new CommonDbClass() { DataBaseType = CommonDbType.MySql, DbProvider = "MySql.Data.MySqlClient" });
                ms_dbInfo.Add(new CommonDbClass() { DataBaseType = CommonDbType.SqlServer, DbProvider = "System.Data.SqlClient" });
                ms_dbInfo.Add(new CommonDbClass() { DataBaseType = CommonDbType.Oracle, DbProvider = "System.Data.OracleClient" });
                ms_dbInfo.Add(new CommonDbClass() { DataBaseType = CommonDbType.Teradata, DbProvider = "Teradata.Client.Provider" });
                ms_dbInfo.Add(new CommonDbClass() { DataBaseType = CommonDbType.Sqlite, DbProvider = "System.Data.SQLite" });
                ms_dbInfo.Add(new CommonDbClass() { DataBaseType = CommonDbType.OleDb, DbProvider = "System.Data.OleDb" });
                return ms_dbInfo;
            }
        }
    }
}
