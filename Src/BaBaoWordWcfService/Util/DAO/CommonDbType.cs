﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaBaoWordWcfService.Util
{
    public enum CommonDbType
    {
        /// <summary>
        /// MySql native provider
        /// </summary>
        MySql,

        /// <summary>
        /// SqlServer native provider
        /// </summary>
        SqlServer,

        /// <summary>
        /// Oracle native provider
        /// </summary>
        Oracle,

        /// <summary>
        /// Teradata native provider
        /// </summary>
        Teradata,

        /// <summary>
        /// Sqlite native provider
        /// </summary>
        Sqlite,

        /// <summary>
        /// OleDb provider
        /// </summary>
        OleDb,

        /// <summary>
        /// None
        /// </summary>
        None
    }
}
