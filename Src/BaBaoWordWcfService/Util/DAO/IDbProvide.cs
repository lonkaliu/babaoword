﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BaBaoWordWcfService.Util
{
    public interface IDbProvide
    {
        string GetValue(string strSQL);
        string GetValue(string strSQL, List<DbParameter> parms);
        DataTable GetDataTable(string strSQL);
        DataTable GetDataTable(string strSQL, List<DbParameter> parms);
        DataTable GetStoredProcedure(string spName, List<DbParameter> parms);
        void ExecuteSQL(string strSQL);
        void ExecuteSQL(string strSQL, List<DbParameter> parms);
        object ExecuteScalar(string strSQL);
        object ExecuteScalar(string strSQL, List<DbParameter> parms);
    }
}
