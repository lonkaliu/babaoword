﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace BaBaoWordWcfService.Util
{
    public class CommonDb : IDbProvide
    {
        private DbProviderFactory m_dbProviderFactory;

        private DbProviderFactory m_DbProviderFactory
        {
            get
            {
                if (m_dbProviderFactory == null)
                {
                    for (int i = 0; i < CommonDbInfo.DbInfo.Count; i++)
                    {
                        if (GetCommonDbType() == CommonDbInfo.DbInfo[i].DataBaseType)
                        {
                            m_dbProviderFactory = DbProviderFactories.GetFactory(CommonDbInfo.DbInfo[i].DbProvider);
                            break;
                        }
                    }
                    if (m_dbProviderFactory == null)
                    {
                        Log.WriteLog(LogLevel.Critical, "specify DatabaseMode can not be supported.");
                    }
                }
                return m_dbProviderFactory;
            }
        }

        private CommonDbType GetCommonDbType()
        {
            return UtilParameters.GetInstance().CommonDbType;
        }

        private string GetConnectionString()
        {
            return UtilParameters.GetInstance().ConnectionString;
        }

        #region IDbProvide

        public void ExecuteSQL(string strSQL)
        {
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                DbTransaction dbTransaction = dbConnection.BeginTransaction();
                try
                {
                    DbCommand dbCommand = m_DbProviderFactory.CreateCommand();
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    dbCommand.Transaction = dbTransaction;
                    dbCommand.ExecuteNonQuery();
                    dbTransaction.Commit();
                    dbCommand.Cancel();
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    throw ex;
                }
            }
        }

        public void ExecuteSQL(string strSQL, List<DbParameter> parms)
        {
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                DbTransaction dbTransaction = dbConnection.BeginTransaction();
                try
                {
                    DbCommand dbCommand = m_DbProviderFactory.CreateCommand();
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    dbCommand.Transaction = dbTransaction;
                    for (int i = 0; i < parms.Count; i++)
                    {
                        DbParameter dbParms = dbCommand.CreateParameter();
                        dbParms.Direction = parms[i].Direction;
                        dbParms.DbType = parms[i].DbType;
                        dbParms.ParameterName = parms[i].ParameterName;
                        dbParms.Value = parms[i].Value;
                        dbCommand.Parameters.Add(dbParms);
                    }
                    dbCommand.ExecuteNonQuery();
                    dbTransaction.Commit();
                    dbCommand.Cancel();
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    throw ex;
                }
            }
        }

        public DataTable GetDataTable(string strSQL)
        {
            DataTable result = new DataTable();
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                using (DbCommand dbCommand = m_DbProviderFactory.CreateCommand())
                {
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    DbDataAdapter dbDataAdapter = m_DbProviderFactory.CreateDataAdapter();
                    dbDataAdapter.SelectCommand = dbCommand;
                    dbDataAdapter.Fill(result);
                    dbCommand.Cancel();
                }
            }
            return result;
        }

        public DataTable GetDataTable(string strSQL, List<DbParameter> parms)
        {
            DataTable result = new DataTable();
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                using (DbCommand dbCommand = m_DbProviderFactory.CreateCommand())
                {
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    for (int i = 0; i < parms.Count; i++)
                    {
                        DbParameter dbParms = dbCommand.CreateParameter();
                        dbParms.Direction = parms[i].Direction;
                        dbParms.DbType = parms[i].DbType;
                        dbParms.ParameterName = parms[i].ParameterName;
                        dbParms.Value = parms[i].Value;
                        dbCommand.Parameters.Add(dbParms);
                    }
                    DbDataAdapter dbDataAdapter = m_DbProviderFactory.CreateDataAdapter();
                    dbDataAdapter.SelectCommand = dbCommand;
                    dbDataAdapter.Fill(result);
                    dbCommand.Cancel();
                }
            }
            return result;
        }

        public DataTable GetStoredProcedure(string spName, List<DbParameter> parms)
        {
            DataTable result = new DataTable();
            bool hasOutput = false;
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                using (DbCommand dbCommand = m_DbProviderFactory.CreateCommand())
                {
                    dbCommand.CommandType = CommandType.StoredProcedure;
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = spName;
                    for (int i = 0; i < parms.Count; i++)
                    {
                        DbParameter dbParms = dbCommand.CreateParameter();
                        dbParms.Direction = parms[i].Direction;
                        dbParms.DbType = parms[i].DbType;
                        dbParms.ParameterName = parms[i].ParameterName;
                        dbParms.Value = parms[i].Value;
                        dbParms.Size = parms[i].Size;
                        if (parms[i].Direction == ParameterDirection.Output)
                        {
                            dbParms.Size = 4000;
                            result.Columns.Add(parms[i].ParameterName);
                            hasOutput = true;
                        }
                        dbCommand.Parameters.Add(dbParms);
                    }
                    DbDataReader dbDataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                    dbCommand.Cancel();
                    dbDataReader.Close();
                    if (hasOutput)
                    {
                        DataRow dr = result.NewRow();
                        foreach (DbParameter dbParms in dbCommand.Parameters)
                        {
                            if (dbParms.Direction == ParameterDirection.Output)
                            {
                                dr[dbParms.ParameterName] = dbParms.Value;
                            }
                        }
                        result.Rows.Add(dr);
                    }
                }
            }
            return result;
        }

        public string GetValue(string strSQL)
        {
            string result = string.Empty;
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                using (DbCommand dbCommand = m_DbProviderFactory.CreateCommand())
                {
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    DbDataReader dbDataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dbDataReader.HasRows)
                    {
                        while (dbDataReader.Read())
                        {
                            result = dbDataReader[0].ToString();
                            break;
                        }
                    }
                    dbCommand.Cancel();
                    dbDataReader.Close();
                }
            }
            return result;
        }

        public string GetValue(string strSQL, List<DbParameter> parms)
        {
            string result = string.Empty;
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                using (DbCommand dbCommand = m_DbProviderFactory.CreateCommand())
                {
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    for (int i = 0; i < parms.Count; i++)
                    {
                        DbParameter dbParms = dbCommand.CreateParameter();
                        dbParms.Direction = parms[i].Direction;
                        dbParms.DbType = parms[i].DbType;
                        dbParms.ParameterName = parms[i].ParameterName;
                        dbParms.Value = parms[i].Value;
                        dbCommand.Parameters.Add(dbParms);
                    }
                    DbDataReader dbDataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dbDataReader.HasRows)
                    {
                        while (dbDataReader.Read())
                        {
                            result = dbDataReader[0].ToString();
                            break;
                        }
                    }
                    dbCommand.Cancel();
                    dbDataReader.Close();
                }
            }
            return result;
        }

        #endregion IDbProvide


        public object ExecuteScalar(string strSQL)
        {
            object value = null;
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                DbTransaction dbTransaction = dbConnection.BeginTransaction();
                try
                {
                    DbCommand dbCommand = m_DbProviderFactory.CreateCommand();
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    dbCommand.Transaction = dbTransaction;
                    value = dbCommand.ExecuteScalar();
                    dbTransaction.Commit();
                    dbCommand.Cancel();
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    throw ex;
                }
            }
            return value;
        }

        public object ExecuteScalar(string strSQL, List<DbParameter> parms)
        {
            object value = null;
            using (DbConnection dbConnection = m_DbProviderFactory.CreateConnection())
            {
                dbConnection.ConnectionString = GetConnectionString();
                dbConnection.Open();
                DbTransaction dbTransaction = dbConnection.BeginTransaction();
                try
                {
                    DbCommand dbCommand = m_DbProviderFactory.CreateCommand();
                    dbCommand.Connection = dbConnection;
                    dbCommand.CommandText = strSQL;
                    dbCommand.Transaction = dbTransaction;
                    for (int i = 0; i < parms.Count; i++)
                    {
                        DbParameter dbParms = dbCommand.CreateParameter();
                        dbParms.Direction = parms[i].Direction;
                        dbParms.DbType = parms[i].DbType;
                        dbParms.ParameterName = parms[i].ParameterName;
                        dbParms.Value = parms[i].Value;
                        dbCommand.Parameters.Add(dbParms);
                    }
                    value = dbCommand.ExecuteScalar();
                    dbTransaction.Commit();
                    dbCommand.Cancel();
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    throw ex;
                }
            }
            return value;
        }
    }
}